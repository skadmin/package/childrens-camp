<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220113165636 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE childrens_camp_registration_supplement (id INT AUTO_INCREMENT NOT NULL, childrens_camp_id INT DEFAULT NULL, supplement_id INT DEFAULT NULL, price DOUBLE PRECISION NOT NULL, INDEX IDX_8B250F1D767399D2 (childrens_camp_id), INDEX IDX_8B250F1D7793FA21 (supplement_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE childrens_camp_supplement (id INT AUTO_INCREMENT NOT NULL, price DOUBLE PRECISION NOT NULL, type INT NOT NULL, name VARCHAR(255) NOT NULL, content LONGTEXT NOT NULL, is_active TINYINT(1) DEFAULT \'1\' NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE childrens_camp_registration_supplement ADD CONSTRAINT FK_8B250F1D767399D2 FOREIGN KEY (childrens_camp_id) REFERENCES childrens_camp (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE childrens_camp_registration_supplement ADD CONSTRAINT FK_8B250F1D7793FA21 FOREIGN KEY (supplement_id) REFERENCES childrens_camp_supplement (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE childrens_camp_registration_supplement DROP FOREIGN KEY FK_8B250F1D7793FA21');
        $this->addSql('DROP TABLE childrens_camp_registration_supplement');
        $this->addSql('DROP TABLE childrens_camp_supplement');
    }
}
