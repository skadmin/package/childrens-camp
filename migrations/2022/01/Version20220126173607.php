<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220126173607 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE childrens_camp_mail_template_rel_file (childrens_camp_mail_template_id INT NOT NULL, file_id INT NOT NULL, INDEX IDX_8F4A767033D0416D (childrens_camp_mail_template_id), INDEX IDX_8F4A767093CB796C (file_id), PRIMARY KEY(childrens_camp_mail_template_id, file_id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE childrens_camp_mail_template_rel_file ADD CONSTRAINT FK_8F4A767033D0416D FOREIGN KEY (childrens_camp_mail_template_id) REFERENCES childrens_camp_mail_template (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE childrens_camp_mail_template_rel_file ADD CONSTRAINT FK_8F4A767093CB796C FOREIGN KEY (file_id) REFERENCES file (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE childrens_camp_mail_template_rel_file');
    }
}
