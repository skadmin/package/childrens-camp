<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220114205419 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE childrens_camp_registration_supplement DROP FOREIGN KEY FK_8B250F1D767399D2');
        $this->addSql('DROP INDEX IDX_8B250F1D767399D2 ON childrens_camp_registration_supplement');
        $this->addSql('ALTER TABLE childrens_camp_registration_supplement CHANGE childrens_camp_id registration_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE childrens_camp_registration_supplement ADD CONSTRAINT FK_8B250F1D833D8F43 FOREIGN KEY (registration_id) REFERENCES childrens_camp_registration (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_8B250F1D833D8F43 ON childrens_camp_registration_supplement (registration_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE childrens_camp_registration_supplement DROP FOREIGN KEY FK_8B250F1D833D8F43');
        $this->addSql('DROP INDEX IDX_8B250F1D833D8F43 ON childrens_camp_registration_supplement');
        $this->addSql('ALTER TABLE childrens_camp_registration_supplement CHANGE registration_id childrens_camp_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE childrens_camp_registration_supplement ADD CONSTRAINT FK_8B250F1D767399D2 FOREIGN KEY (childrens_camp_id) REFERENCES childrens_camp (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_8B250F1D767399D2 ON childrens_camp_registration_supplement (childrens_camp_id)');
    }
}
