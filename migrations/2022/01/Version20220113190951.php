<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220113190951 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE childrens_camp_rel_childrens_camp_supplement (childrens_camp_id INT NOT NULL, childrens_camp_supplement_id INT NOT NULL, INDEX IDX_CBCBA856767399D2 (childrens_camp_id), INDEX IDX_CBCBA85678CE1FD0 (childrens_camp_supplement_id), PRIMARY KEY(childrens_camp_id, childrens_camp_supplement_id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE childrens_camp_rel_childrens_camp_supplement ADD CONSTRAINT FK_CBCBA856767399D2 FOREIGN KEY (childrens_camp_id) REFERENCES childrens_camp (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE childrens_camp_rel_childrens_camp_supplement ADD CONSTRAINT FK_CBCBA85678CE1FD0 FOREIGN KEY (childrens_camp_supplement_id) REFERENCES childrens_camp_supplement (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE childrens_camp_rel_childrens_camp_supplement');
    }
}
