<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220620164302 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE childrens_camp ADD registration_external_link VARCHAR(255) NOT NULL');

        $translations = [
            ['original' => 'form.childrens-camp.edit.registration-external-link', 'hash' => 'a620efa36ea5a6adb7c5f71cde0e6828', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Odkaz na registraci', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.childrens-camp.edit.registration-external-link.hint', 'hash' => 'c25b8444fec82089e41c64abdc4abad9', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Pokud registrace probíhá mimo stránky, zadejte celou URL. Následně nebude možné nastavit registraci zde.', 'plural1' => '', 'plural2' => ''],
        ];

        foreach ($translations as $translation) {
            $this->addSql('DELETE FROM translation WHERE hash = :hash', $translation);
            $this->addSql('SELECT create_translation(:original, :hash, :module, :language_id, :singular, :plural1, :plural2)', $translation);
        }
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE childrens_camp DROP registration_external_link');
    }
}
