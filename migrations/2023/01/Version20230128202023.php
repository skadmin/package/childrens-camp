<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230128202023 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE childrens_camp_registration ADD invoice_price DOUBLE PRECISION NOT NULL, ADD invoice_date_of_issue DATETIME DEFAULT NULL, ADD invoice_date_of_taxable_supply DATETIME DEFAULT NULL, ADD invoice_date_due DATETIME DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE childrens_camp_registration DROP invoice_price, DROP invoice_date_of_issue, DROP invoice_date_of_taxable_supply, DROP invoice_date_due');
    }
}
