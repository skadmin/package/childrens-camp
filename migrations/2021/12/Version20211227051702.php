<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211227051702 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE childrens_camp_rel_transport (childrens_camp_id INT NOT NULL, transport_id INT NOT NULL, INDEX IDX_F8039D1E767399D2 (childrens_camp_id), INDEX IDX_F8039D1E9909C13F (transport_id), PRIMARY KEY(childrens_camp_id, transport_id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE childrens_camp_rel_payment (childrens_camp_id INT NOT NULL, payment_id INT NOT NULL, INDEX IDX_48CC16B9767399D2 (childrens_camp_id), INDEX IDX_48CC16B94C3A3BB (payment_id), PRIMARY KEY(childrens_camp_id, payment_id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE childrens_camp_rel_transport ADD CONSTRAINT FK_F8039D1E767399D2 FOREIGN KEY (childrens_camp_id) REFERENCES childrens_camp (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE childrens_camp_rel_transport ADD CONSTRAINT FK_F8039D1E9909C13F FOREIGN KEY (transport_id) REFERENCES transport (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE childrens_camp_rel_payment ADD CONSTRAINT FK_48CC16B9767399D2 FOREIGN KEY (childrens_camp_id) REFERENCES childrens_camp (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE childrens_camp_rel_payment ADD CONSTRAINT FK_48CC16B94C3A3BB FOREIGN KEY (payment_id) REFERENCES payment (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE childrens_camp ADD text_registration LONGTEXT NOT NULL');
        $this->addSql('ALTER TABLE childrens_camp_registration DROP street_number, DROP invoice_street_number');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE childrens_camp_rel_transport');
        $this->addSql('DROP TABLE childrens_camp_rel_payment');
        $this->addSql('ALTER TABLE childrens_camp DROP text_registration');
        $this->addSql('ALTER TABLE childrens_camp_registration ADD street_number VARCHAR(255) CHARACTER SET utf8mb3 NOT NULL COLLATE `utf8mb3_unicode_ci`, ADD invoice_street_number VARCHAR(255) CHARACTER SET utf8mb3 NOT NULL COLLATE `utf8mb3_unicode_ci`');
    }
}
