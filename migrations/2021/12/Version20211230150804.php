<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211230150804 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE childrens_camp_mail_template (id INT AUTO_INCREMENT NOT NULL, camp_id INT DEFAULT NULL, subject VARCHAR(255) NOT NULL, preheader VARCHAR(255) NOT NULL, content LONGTEXT NOT NULL, type VARCHAR(255) NOT NULL, class VARCHAR(255) NOT NULL, recipients LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', parameters LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', name VARCHAR(255) NOT NULL, last_update_author VARCHAR(255) NOT NULL, last_update_at DATETIME DEFAULT NULL, INDEX IDX_45EA3B3A77075ABB (camp_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE childrens_camp_mail_template ADD CONSTRAINT FK_45EA3B3A77075ABB FOREIGN KEY (camp_id) REFERENCES childrens_camp (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE childrens_camp_mail_template');
    }
}
