<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211223151047 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE childrens_camp_registration (id INT AUTO_INCREMENT NOT NULL, camp_id INT DEFAULT NULL, transport_id INT DEFAULT NULL, payment_id INT DEFAULT NULL, insurance_company_id INT DEFAULT NULL, child_first_name VARCHAR(255) NOT NULL, child_last_name VARCHAR(255) NOT NULL, personal_identification_number VARCHAR(255) NOT NULL, birthdate DATETIME NOT NULL, street VARCHAR(255) NOT NULL, street_number VARCHAR(255) NOT NULL, city VARCHAR(255) NOT NULL, zip_code VARCHAR(255) NOT NULL, contact_emails VARCHAR(255) NOT NULL, mother_name VARCHAR(255) NOT NULL, mother_phone VARCHAR(255) NOT NULL, father_name VARCHAR(255) NOT NULL, father_phone VARCHAR(255) NOT NULL, swimming_skills INT NOT NULL, health_condition VARCHAR(2048) NOT NULL, washing_first VARCHAR(2048) NOT NULL, washing_second VARCHAR(2048) NOT NULL, washing_third VARCHAR(2048) NOT NULL, invoice_firm VARCHAR(255) NOT NULL, invoice_org VARCHAR(255) NOT NULL, invoice_in VARCHAR(255) NOT NULL, invoice_tin VARCHAR(255) NOT NULL, invoice_street VARCHAR(255) NOT NULL, invoice_street_number VARCHAR(255) NOT NULL, invoice_city VARCHAR(255) NOT NULL, invoice_zip_code VARCHAR(255) NOT NULL, invoice_note VARCHAR(2048) NOT NULL, consent_to_the_camp_rules TINYINT(1) DEFAULT \'0\' NOT NULL, consent_to_personal_data_protection TINYINT(1) DEFAULT \'0\' NOT NULL, consent_to_the_processing_of_sensitive_data TINYINT(1) DEFAULT \'0\' NOT NULL, permission_to_separate_achild_on_trip TINYINT(1) DEFAULT \'0\' NOT NULL, tshirt_color VARCHAR(255) NOT NULL, tshirt_size VARCHAR(255) NOT NULL, tshirt_count INT NOT NULL, tshirt_note VARCHAR(2048) NOT NULL, note VARCHAR(2048) NOT NULL, created_at DATETIME NOT NULL, last_update_at DATETIME DEFAULT NULL, INDEX IDX_3FB462EE77075ABB (camp_id), INDEX IDX_3FB462EE9909C13F (transport_id), INDEX IDX_3FB462EE4C3A3BB (payment_id), INDEX IDX_3FB462EEECB24509 (insurance_company_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE childrens_camp_registration ADD CONSTRAINT FK_3FB462EE77075ABB FOREIGN KEY (camp_id) REFERENCES childrens_camp (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE childrens_camp_registration ADD CONSTRAINT FK_3FB462EE9909C13F FOREIGN KEY (transport_id) REFERENCES transport (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE childrens_camp_registration ADD CONSTRAINT FK_3FB462EE4C3A3BB FOREIGN KEY (payment_id) REFERENCES payment (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE childrens_camp_registration ADD CONSTRAINT FK_3FB462EEECB24509 FOREIGN KEY (insurance_company_id) REFERENCES insurance_company (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE childrens_camp ADD tshirt_color_list LONGTEXT NOT NULL COMMENT \'(DC2Type:simple_array)\', ADD tshirt_size_list LONGTEXT NOT NULL COMMENT \'(DC2Type:simple_array)\'');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE childrens_camp_registration DROP FOREIGN KEY FK_3FB462EEECB24509');
        $this->addSql('ALTER TABLE childrens_camp_registration DROP FOREIGN KEY FK_3FB462EE4C3A3BB');
        $this->addSql('ALTER TABLE childrens_camp_registration DROP FOREIGN KEY FK_3FB462EE9909C13F');
        $this->addSql('DROP TABLE childrens_camp_registration');
        $this->addSql('ALTER TABLE childrens_camp DROP tshirt_color_list, DROP tshirt_size_list');
    }
}
