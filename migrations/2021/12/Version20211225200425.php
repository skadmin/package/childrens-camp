<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211225200425 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE childrens_camp ADD tshirt_death_line DATETIME NOT NULL');
        $this->addSql('ALTER TABLE childrens_camp_registration DROP tshirt_death_line');
        $this->addSql('UPDATE childrens_camp SET tshirt_death_line = "2022-04-12 23:59:59"');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE childrens_camp DROP tshirt_death_line');
        $this->addSql('ALTER TABLE childrens_camp_registration ADD tshirt_death_line DATETIME NOT NULL');
    }
}
