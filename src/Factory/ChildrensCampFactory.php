<?php

declare(strict_types=1);

namespace SkadminUtils\ChildrensCamp\Factory;

class ChildrensCampFactory
{
    private bool $prependChildrensCamp;

    public function __construct(bool $prependChildrensCamp)
    {
        $this->prependChildrensCamp = $prependChildrensCamp;
    }

    public function isPrependChildrensCamp(): bool
    {
        return $this->prependChildrensCamp;
    }
}
