<?php

declare(strict_types=1);

namespace Skadmin\ChildrensCamp\DI;

use Nette\DI\CompilerExtension;
use Nette\Schema\Expect;
use Nette\Schema\Schema;
use SkadminUtils\ChildrensCamp\Factory\ChildrensCampFactory;
use SkadminUtils\Gallery\Factory\GallerySettingsFactory;

class ChildrensCampExtension extends CompilerExtension
{
    public function getConfigSchema(): Schema
    {
        return Expect::structure([
            'prependChildrensCamp' => Expect::bool(true),
        ]);
    }

    public function loadConfiguration(): void
    {
        parent::loadConfiguration();

        $builder = $this->getContainerBuilder();
        $builder->addDefinition($this->prefix('childrensCampFactory'))
            ->setClass(ChildrensCampFactory::class)
            ->setArguments([
                $this->config->prependChildrensCamp,
            ]);
    }
}
