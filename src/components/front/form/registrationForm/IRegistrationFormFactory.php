<?php

declare(strict_types=1);

namespace Skadmin\ChildrensCamp\Components\Front;

use Skadmin\ChildrensCamp\Doctrine\ChildrensCamp\ChildrensCamp;

interface IRegistrationFormFactory
{
    public function create(ChildrensCamp $childrensCamp) : RegistrationForm;
}
