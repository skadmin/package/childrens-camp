<?php

declare(strict_types=1);

namespace Skadmin\ChildrensCamp\Components\Front;

use App\Model\System\APackageControl;
use App\Model\System\Constant;
use App\Model\System\Flash;
use App\Model\System\FormRuleValidator;
use SkadminUtils\ImageStorage\ImageStorage;
use Defr\Ares;
use Nette\ComponentModel\IContainer;
use Nette\Utils\ArrayHash;
use Nette\Utils\Arrays;
use Nette\Utils\Html;
use Skadmin\ChildrensCamp\Doctrine\ChildrensCamp\ChildrensCamp;
use Skadmin\ChildrensCamp\Doctrine\ChildrensCamp\ChildrensCampSupplement\ChildrensCampSupplement;
use Skadmin\ChildrensCamp\Doctrine\ChildrensCampRegistration\ChildrensCampRegistration;
use Skadmin\ChildrensCamp\Doctrine\ChildrensCampRegistration\ChildrensCampRegistrationFacade;
use Skadmin\ChildrensCamp\Service\ChildrensCampRegistrationService;
use Skadmin\InsuranceCompany\Doctrine\InsuranceCompany\InsuranceCompanyFacade;
use Skadmin\Mailing\Doctrine\Mail\MailQueue;
use Skadmin\Payment\Doctrine\Payment\PaymentFacade;
use Skadmin\Translator\Translator;
use Skadmin\Transport\Doctrine\Transport\TransportFacade;
use SkadminUtils\FormControls\UI\Form;
use SkadminUtils\FormControls\UI\FormControl;
use DateTime;
use Tracy\Debugger;
use WebLoader\Nette\CssLoader;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;

class RegistrationForm extends FormControl
{
    use APackageControl;

    private ChildrensCampRegistrationFacade  $facade;
    private ChildrensCamp                    $childrensCamp;
    private TransportFacade                  $facadeTransport;
    private PaymentFacade                    $facadePayment;
    private InsuranceCompanyFacade           $facadeInsuranceCompany;
    private ImageStorage                     $imageStorage;
    private LoaderFactory                    $webLoader;
    private ChildrensCampRegistrationService $serviceChildrensCampRegistration;

    public function __construct(ChildrensCamp $childrensCamp, ChildrensCampRegistrationFacade $facade, Translator $translator, TransportFacade $facadeTransport, PaymentFacade $facadePayment, InsuranceCompanyFacade $facadeInsuranceCompany, ImageStorage $imageStorage, LoaderFactory $webLoader, ChildrensCampRegistrationService $serviceChildrensCampRegistration)
    {
        parent::__construct($translator);
        $this->facade        = $facade;
        $this->childrensCamp = $childrensCamp;

        $this->facadeTransport        = $facadeTransport;
        $this->facadePayment          = $facadePayment;
        $this->facadeInsuranceCompany = $facadeInsuranceCompany;

        $this->imageStorage = $imageStorage;
        $this->webLoader    = $webLoader;

        $this->serviceChildrensCampRegistration = $serviceChildrensCampRegistration;
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null)
    {
        parent::setParent($parent, $name);
        return $this;
    }

    public function getTitle() : string
    {
        return 'childrens-camp.front.registration.title';
    }

    /**
     * @return CssLoader[]
     */
    public function getCss() : array
    {
        return [
            $this->webLoader->createCssLoader('daterangePicker'),
        ];
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs() : array
    {
        return [
            $this->webLoader->createJavaScriptLoader('reCaptchaInvisible'),
            $this->webLoader->createJavaScriptLoader('moment'),
            $this->webLoader->createJavaScriptLoader('daterangePicker'),
        ];
    }

    public function render() : void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/registrationForm.latte');

        $template->childrensCamp = $this->childrensCamp;
        $template->__form        = $this['form'];
        $template->render();
    }

    protected function createComponentForm() : Form
    {
        // DATA
        $dataInsuranceCompanies = [];
        foreach ($this->facadeInsuranceCompany->getAll(true) as $insuranceCompany) {
            $dataInsuranceCompanies[$insuranceCompany->getId()] = sprintf('%s - %s', $insuranceCompany->getCode(), $insuranceCompany->getName());
        }
        $dataTransport = [];
        foreach ($this->childrensCamp->getTransports(true) as $transport) {
            $img = Html::el('img', [
                'src'   => '/' . $this->imageStorage->fromIdentifier([$transport->getImagePreview(), '50x50', 'exact'])->createLink(),
                'alt'   => $transport->getName(),
                'class' => 'me-1',
            ]);

            $dataTransport[$transport->getId()] = Html::el('div', [
                'class' => 'd-inline-block'
            ])->addHtml($img)
                ->addText($transport->getName());
        }
        $dataPayment = [];
        foreach ($this->childrensCamp->getPayments(true) as $payment) {
            $img = Html::el('img', [
                'src'   => '/' . $this->imageStorage->fromIdentifier([$payment->getImagePreview(), '50x50', 'exact'])->createLink(),
                'alt'   => $payment->getName(),
                'class' => 'me-1',
            ]);

            $dataPayment[$payment->getId()] = Html::el('div', [
                'class' => 'd-inline-block'
            ])->addHtml($img)
                ->addText($payment->getName());
        }
        $dataTshirtColor = array_combine($this->childrensCamp->getTshirtColorList(), $this->childrensCamp->getTshirtColorList());
        $dataTshirtSize  = array_combine($this->childrensCamp->getTshirtSizeList(), $this->childrensCamp->getTshirtSizeList());

        $dataSupplements = [];
        foreach ($this->childrensCamp->getSupplements() as $supplement) {
            $dataSupplements[$supplement->getId()] = sprintf('%s (%s Kč)', $supplement->getName(), number_format($supplement->getPrice(true), 2, ',', ' '));
        }

        // FORM
        $form = new Form();
        $form->setTranslator($this->translator);

        // 1. Část = Základní údaje
        $form->addText('childFirstName', 'form.childrens-camp.front.registration.child-first-name')
            ->setRequired('form.childrens-camp.front.registration.child-first-name.req');
        $form->addText('childLastName', 'form.childrens-camp.front.registration.child-last-name')
            ->setRequired('form.childrens-camp.front.registration.child-last-name.req');
        $form->addText('personalIdentificationNumber', 'form.childrens-camp.front.registration.personal-identification-number')
            ->setRequired('form.childrens-camp.front.registration.personal-identification-number.req');
        $form->addText('birthdate', 'form.childrens-camp.front.registration.birthdate')
            ->setRequired('form.childrens-camp.front.registration.birthdate.req')
            ->setHtmlAttribute('data-date')
            ->setHtmlAttribute('data-date-clear')
            ->setHtmlAttribute('data-date-noicon');

        // 2. Část = Bydliště
        $form->addText('street', 'form.childrens-camp.front.registration.street')
            ->setRequired('form.childrens-camp.front.registration.street.req');
        $form->addText('city', 'form.childrens-camp.front.registration.city')
            ->setRequired('form.childrens-camp.front.registration.city.req');
        $form->addText('zipCode', 'form.childrens-camp.front.registration.zip-code')
            ->setRequired('form.childrens-camp.front.registration.zip-code.req');

        // 4. Část = Kontaktní údaje (Rodiče / Zákonný zástupce)
        $form->addEmail('contactEmail', 'form.childrens-camp.front.registration.contact-email')
            ->setRequired('form.childrens-camp.front.registration.contact-email.req');
        $form->addText('motherName', 'form.childrens-camp.front.registration.mother-name');
        $form->addPhone('motherPhone', 'form.childrens-camp.front.registration.mother-phone');
        $form->addText('fatherName', 'form.childrens-camp.front.registration.father-name');
        $form->addPhone('fatherPhone', 'form.childrens-camp.front.registration.father-phone');
        $form->addText('legalRepresentativeName', 'form.childrens-camp.front.registration.legal-representative-name');
        $form->addPhone('legalRepresentativePhone', 'form.childrens-camp.front.registration.legal-representative-phone');

        // 5. Část = Doprava na tábor
        $inputTransport = $form->addRadioList('transport', 'form.childrens-camp.front.registration.transport', $dataTransport)
            ->setTranslator(null)
            ->setRequired('form.childrens-camp.front.registration.transport.req')
            ->setDefaultValue(array_keys($dataTransport)[0]);

        foreach (array_keys($dataTransport) as $transportId) {
            $inputTransport->addCondition(Form::EQUAL, $transportId)
                ->toggle(sprintf('toggle-transport-%d', $transportId));
        }

        // 6. Část = Způsob platby
        $inputPayment = $form->addRadioList('payment', 'form.childrens-camp.front.registration.payment', $dataPayment)
            ->setTranslator(null)
            ->setRequired('form.childrens-camp.front.registration.payment.req')
            ->setDefaultValue(array_keys($dataPayment)[0]);

        foreach (array_keys($dataPayment) as $paymentId) {
            $inputPayment->addCondition(Form::EQUAL, $paymentId)
                ->toggle(sprintf('toggle-payment-%d', $paymentId));
        }

        // 7. Část = Informace o dítěti
        $form->addSelect('swimmingSkills', 'form.childrens-camp.front.registration.swimming-skills', ChildrensCampRegistration::SWIMMINGS_SKILLS)
            ->setPrompt(Constant::PROMTP)
            ->setRequired('form.childrens-camp.front.registration.swimming-skills.req');
        $form->addSelect('insuranceCompany', 'form.childrens-camp.front.registration.insurance-company', $dataInsuranceCompanies)
            ->setTranslator(null)
            ->setPrompt(Constant::PROMTP)
            ->setRequired('form.childrens-camp.front.registration.insurance-company.req');
        $form->addCheckbox('confirmationForTheInsuranceCompany', 'form.childrens-camp.front.registration.confirmation-for-the-insurance-company');
        $form->addTextArea('healthCondition', 'form.childrens-camp.front.registration.health-condition');
        $form->addTextArea('washingFirst', 'form.childrens-camp.front.registration.washing-first');
        $form->addTextArea('washingSecond', 'form.childrens-camp.front.registration.washing-second');
        $form->addTextArea('washingThird', 'form.childrens-camp.front.registration.washing-third');

        // 8. Část = Fakturace
        $form->addCheckbox('isInvoice', 'form.childrens-camp.front.registration.is-invoice')
            ->addCondition(Form::EQUAL, true)
            ->toggle('toggle-invoice');
        $form->addText('invoiceFirm', 'form.childrens-camp.front.registration.invoice-firm');
        $form->addText('invoiceIN', 'form.childrens-camp.front.registration.invoice-in');
        $form->addText('invoiceTIN', 'form.childrens-camp.front.registration.invoice-tin');
        $form->addText('invoiceStreet', 'form.childrens-camp.front.registration.invoice-street');
        $form->addText('invoiceCity', 'form.childrens-camp.front.registration.invoice-city');
        $form->addText('invoiceZipCode', 'form.childrens-camp.front.registration.invoice-zip-code');
        $form->addTextArea('invoiceNote', 'form.childrens-camp.front.registration.invoice-note');

        // 10. Část = Souhlasy
        $form->addCheckbox('consentToTheCampRules', 'form.childrens-camp.front.registration.consent-to-the-camp-rules')
            ->setRequired('form.childrens-camp.front.registration.consent-to-the-camp-rules.req');
        $form->addCheckbox('consentToPersonalDataProtection', 'form.childrens-camp.front.registration.consent-to-personal-data-protection')
            ->setRequired('form.childrens-camp.front.registration.consent-to-personal-data-protection.req');
        $form->addCheckbox('consentToTheProcessingOfSensitiveData', 'form.childrens-camp.front.registration.consent-to-the-processing-of-sensitive-data')
            ->setRequired('form.childrens-camp.front.registration.consent-to-the-processing-of-sensitive-data.req');
        $form->addCheckbox('permissionToSeparateAChildOnTrip', 'form.childrens-camp.front.registration.permission-to-separate-a-child-on-trip');

        // 11. Část = Táborové tričko
        if (new DateTime() < $this->childrensCamp->getTshirtDeathLine()) {
            $form->addCheckbox('wantTshirt', 'form.childrens-camp.front.registration.want-tshirt')
                ->addCondition(Form::EQUAL, true)
                ->toggle('toggle-tshirt');
            $form->addSelect('tshirtColor', 'form.childrens-camp.front.registration.tshirt-color', $dataTshirtColor)
                ->setPrompt(Constant::PROMTP)
                ->setTranslator(null)
                ->addConditionOn($form['wantTshirt'], Form::EQUAL, true)
                ->setRequired('form.childrens-camp.front.registration.tshirt-color.req');
            $form->addSelect('tshirtSize', 'form.childrens-camp.front.registration.tshirt-size', $dataTshirtSize)
                ->setPrompt(Constant::PROMTP)
                ->setTranslator(null)
                ->addConditionOn($form['wantTshirt'], Form::EQUAL, true)
                ->setRequired('form.childrens-camp.front.registration.tshirt-size.req');
            $form->addSelect('tshirtCount', 'form.childrens-camp.front.registration.tshirt-count', [1 => 1, 2, 3, 4, 5])
                ->setTranslator(null)
                ->addConditionOn($form['wantTshirt'], Form::EQUAL, true)
                ->setRequired('form.childrens-camp.front.registration.tshirt-count.req');
            $form->addTextArea('tshirtNote', 'form.childrens-camp.front.registration.tshirt-note');
        }

        // 12. Část = Závěr
        $form->addTextArea('note', 'form.childrens-camp.front.registration.note');

        // 14. Část = příplatky/slevy
        $formPrice             = $form->addContainer('price');
        $inputPriceSupplements = $formPrice->addCheckboxList('supplements', 'form.childrens-camp.front.supplements', $dataSupplements)
            ->setTranslator(null);

        // CAPTCHA
        $form->addInvisibleReCaptchaInput();

        // BUTTON
        $form->addSubmit('send', 'form.childrens-camp.front.registration.send');

        // CALLBACK
        $form->onSuccess[] = [$this, 'processOnSuccess'];

        return $form;
    }

    public function processOnSuccess(Form $form, ArrayHash $values) : void
    {
        $birthdate = DateTime::createFromFormat('d.m.Y', $values->birthdate);

        $supplements = Arrays::map($values->price->supplements, fn(int $sId) : ChildrensCampSupplement => $this->childrensCamp->getSupplement($sId));

        $registration = $this->facade->create(
            $this->childrensCamp,
            $values->childFirstName,
            $values->childLastName,
            $values->personalIdentificationNumber,
            $birthdate,
            $values->street,
            $values->city,
            $values->zipCode,
            $values->contactEmail,
            $values->motherName,
            $values->motherPhone,
            $values->fatherName,
            $values->fatherPhone,
            $values->legalRepresentativeName,
            $values->legalRepresentativePhone,
            $this->facadeTransport->get($values->transport),
            $this->facadePayment->get($values->payment),
            $values->swimmingSkills,
            $this->facadeInsuranceCompany->get($values->insuranceCompany),
            $values->confirmationForTheInsuranceCompany,
            $values->healthCondition,
            $values->washingFirst,
            $values->washingSecond,
            $values->washingThird,
            $values->consentToTheCampRules,
            $values->consentToPersonalDataProtection,
            $values->consentToTheProcessingOfSensitiveData,
            $values->permissionToSeparateAChildOnTrip,
            $values->note
        );

        if ($values->isInvoice) {
            $this->facade->updateInvoice(
                $registration,
                $values->invoiceFirm,
                $values->invoiceIN,
                $values->invoiceTIN,
                $values->invoiceStreet,
                $values->invoiceCity,
                $values->invoiceZipCode,
                $values->invoiceNote
            );
        }

        if (isset($values->wantTshirt)) {
            $this->facade->updateTshirt(
                $registration,
                $values->tshirtColor,
                $values->tshirtSize,
                $values->tshirtCount,
                $values->tshirtNote,
            );
        }

        $registration = $this->facade->updateSupplements($registration, $supplements);
        $mailQueue    = $this->serviceChildrensCampRegistration->sendByState($registration);

        if ($mailQueue !== null && $mailQueue->getStatus() === MailQueue::STATUS_SENT) {
            $this->onFlashmessage('form.childrens-camp.front.registration.flash.flash.success-mail', Flash::SUCCESS);
        } else {
            $this->onFlashmessage('form.childrens-camp.front.registration.flash.flash.danger-mail', Flash::DANGER);
        }

        $this->onFlashmessage('form.childrens-camp.front.registration.flash.success.create', Flash::SUCCESS);

        $form->reset();
        $this->onRedraw();
    }

    public function handleLoadFromAres(?string $val = null) : void
    {
        try {
            $ares = (new Ares())->findByIdentificationNumber($this->getPresenter()->getParameter('val'));

            $this['form']['invoiceIN']->setValue($ares->getCompanyId());
            $this['form']['invoiceTIN']->setValue($ares->getTaxId());
            $this['form']['invoiceFirm']->setValue($ares->getCompanyName());
            $this['form']['invoiceStreet']->setValue($ares->getStreetWithNumbers());
            $this['form']['invoiceCity']->setValue($ares->getTown());
            $this['form']['invoiceZipCode']->setValue($ares->getZip());

            $this->redrawControl('snipForm');
            $this->redrawControl('snipFormInvoice');
        } catch (Ares\AresException $e) {
            exit;
        }
    }

}
