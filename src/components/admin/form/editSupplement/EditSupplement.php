<?php

declare(strict_types=1);

namespace Skadmin\ChildrensCamp\Components\Admin;

use App\Components\Form\FormWithUserControl;
use Skadmin\Role\Doctrine\Role\Privilege;
use App\Model\System\APackageControl;
use App\Model\System\Constant;
use App\Model\System\Flash;
use Nette\ComponentModel\IContainer;
use Nette\Security\User as LoggedUser;
use Nette\Utils\ArrayHash;
use Skadmin\ChildrensCamp\BaseControl;
use Skadmin\ChildrensCamp\Doctrine\ChildrensCamp\ChildrensCampSupplement\ChildrensCampSupplement;
use Skadmin\ChildrensCamp\Doctrine\ChildrensCamp\ChildrensCampSupplement\ChildrensCampSupplementFacade;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use SkadminUtils\FormControls\UI\Form;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;
use function intval;

class EditSupplement extends FormWithUserControl
{
    use APackageControl;

    private LoaderFactory                 $webLoader;
    private ChildrensCampSupplementFacade $facade;
    private ChildrensCampSupplement       $supplement;

    public function __construct(?int $id, ChildrensCampSupplementFacade $facade, Translator $translator, LoaderFactory $webLoader, LoggedUser $user)
    {
        parent::__construct($translator, $user);
        $this->facade    = $facade;
        $this->webLoader = $webLoader;

        $this->supplement = $this->facade->get($id);
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null)
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    /**
     * @return SimpleTranslation|string
     */
    public function getTitle()
    {
        if ($this->supplement->isLoaded()) {
            return new SimpleTranslation('childrens-camp.edit-supplement.title - %s', $this->supplement->getName());
        }

        return 'childrens-camp.edit-supplement.title';
    }


    /**
     * @return JavaScriptLoader[]
     */
    public function getJs() : array
    {
        return [$this->webLoader->createJavaScriptLoader('adminTinyMce')];
    }

    public function render() : void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/editSupplement.latte');

        $template->render();
    }

    protected function createComponentForm() : Form
    {
        $form = new Form();
        $form->setTranslator($this->translator);

        // INPUT
        $form->addText('name', 'form.childrens-camp.edit-supplement.name')
            ->setRequired('form.childrens-camp.edit-supplement.name.req');
        $form->addTextArea('content', 'form.childrens-camp.edit-supplement.content');

        $form->addCheckbox('isActive', 'form.childrens-camp.edit-supplement.is-active')
            ->setDefaultValue(true);

        $form->addText('price', 'form.childrens-camp.edit-supplement.price')
            ->setRequired('form.childrens-camp.edit-supplement.price.req')
            ->setHtmlType('number');

        $form->addSelect('type', 'form.childrens-camp.edit-supplement.type', ChildrensCampSupplement::TYPES)
            ->setRequired('form.childrens-camp.edit-supplement.type.req')
            ->setPrompt(Constant::PROMTP);

        // BUTTON
        $form->addSubmit('send', 'form.childrens-camp.edit-supplement.send');
        $form->addSubmit('sendBack', 'form.childrens-camp.edit-supplement.send-back');
        $form->addSubmit('back', 'form.childrens-camp.edit-supplement.back')
            ->setValidationScope([])
            ->onClick[] = [$this, 'processOnBack'];

        // DEFAULT
        $form->setDefaults($this->getDefaults());

        // CALLBACK
        $form->onSuccess[] = [$this, 'processOnSuccess'];

        return $form;
    }

    /**
     * @return mixed[]
     */
    private function getDefaults() : array
    {
        if (! $this->supplement->isLoaded()) {
            return [];
        }

        return [
            'name'     => $this->supplement->getName(),
            'content'  => $this->supplement->getContent(),
            'price'    => $this->supplement->getPrice(),
            'isActive' => $this->supplement->isActive(),
            'type'     => $this->supplement->getType(),
        ];
    }

    public function processOnSuccess(Form $form, ArrayHash $values) : void
    {
        if ($this->supplement->isLoaded()) {
            $supplement = $this->facade->update(
                $this->supplement->getId(),
                $values->name,
                $values->isActive,
                $values->content,
                floatval($values->price),
                $values->type
            );
            $this->onFlashmessage('form.childrens-camp.edit-supplement.flash.success.update', Flash::SUCCESS);
        } else {
            $supplement = $this->facade->create(
                $values->name,
                $values->isActive,
                $values->content,
                floatval($values->price),
                $values->type
            );
            $this->onFlashmessage('form.childrens-camp.edit-supplement.flash.success.create', Flash::SUCCESS);
        }

        if ($form->isSubmitted()->name === 'sendBack') {
            $this->processOnBack();
        }

        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'edit-supplement',
            'id'      => $supplement->getId(),
        ]);
    }

    public function processOnBack() : void
    {
        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'overview-supplement',
        ]);
    }
}
