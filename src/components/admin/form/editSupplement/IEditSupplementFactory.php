<?php

declare(strict_types=1);

namespace Skadmin\ChildrensCamp\Components\Admin;

interface IEditSupplementFactory
{
    public function create(?int $id = null) : EditSupplement;
}
