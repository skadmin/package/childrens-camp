<?php

declare(strict_types=1);

namespace Skadmin\ChildrensCamp\Components\Admin;

interface IEditRegistrationFactory
{
    public function create(int $id) : EditRegistration;
}
