<?php

declare(strict_types=1);

namespace Skadmin\ChildrensCamp\Components\Admin;

use App\Components\Form\FormWithUserControl;
use Skadmin\Role\Doctrine\Role\Privilege;
use App\Model\System\APackageControl;
use App\Model\System\Constant;
use App\Model\System\Flash;
use App\Model\System\FormRuleValidator;
use SkadminUtils\ImageStorage\ImageStorage;
use Defr\Ares;
use Nette\ComponentModel\IContainer;
use Nette\Security\User as LoggedUser;
use Nette\Utils\ArrayHash;
use Nette\Utils\Arrays;
use Nette\Utils\Html;
use Skadmin\ChildrensCamp\BaseControl;
use Skadmin\ChildrensCamp\Doctrine\ChildrensCamp\ChildrensCamp;
use Skadmin\ChildrensCamp\Doctrine\ChildrensCamp\ChildrensCampRegistrationSupplement\ChildrensCampRegistrationSupplement;
use Skadmin\ChildrensCamp\Doctrine\ChildrensCamp\ChildrensCampSupplement\ChildrensCampSupplement;
use Skadmin\ChildrensCamp\Doctrine\ChildrensCampRegistration\ChildrensCampRegistration;
use Skadmin\ChildrensCamp\Doctrine\ChildrensCampRegistration\ChildrensCampRegistrationFacade;
use Skadmin\ChildrensCamp\Service\ChildrensCampRegistrationService;
use Skadmin\InsuranceCompany\Doctrine\InsuranceCompany\InsuranceCompanyFacade;
use Skadmin\Mailing\Doctrine\Mail\MailQueue;
use Skadmin\Payment\Doctrine\Payment\PaymentFacade;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use Skadmin\Transport\Doctrine\Transport\TransportFacade;
use SkadminUtils\FormControls\UI\Form;
use DateTime;
use Tracy\Debugger;
use WebLoader\Nette\CssLoader;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;

class EditRegistration extends FormWithUserControl
{
    use APackageControl;

    private ChildrensCampRegistrationFacade  $facade;
    private TransportFacade                  $facadeTransport;
    private PaymentFacade                    $facadePayment;
    private InsuranceCompanyFacade           $facadeInsuranceCompany;
    private LoaderFactory                    $webLoader;
    private ChildrensCampRegistrationService $serviceChildrensCampRegistration;
    private ChildrensCampRegistration        $registration;

    public function __construct(int $id, ChildrensCampRegistrationFacade $facade, Translator $translator, LoggedUser $user, TransportFacade $facadeTransport, PaymentFacade $facadePayment, InsuranceCompanyFacade $facadeInsuranceCompany, LoaderFactory $webLoader, ChildrensCampRegistrationService $serviceChildrensCampRegistration)
    {
        parent::__construct($translator, $user);
        $this->facade = $facade;

        $this->facadeTransport        = $facadeTransport;
        $this->facadePayment          = $facadePayment;
        $this->facadeInsuranceCompany = $facadeInsuranceCompany;

        $this->webLoader = $webLoader;

        $this->serviceChildrensCampRegistration = $serviceChildrensCampRegistration;

        $this->registration = $this->facade->get($id);
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null)
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    /**
     * @return SimpleTranslation|string
     */
    public function getTitle()
    {
        if ($this->registration->isLoaded()) {
            return new SimpleTranslation('childrens-camp.edit-registration.title - %s', $this->registration->getFullNameWithPID());
        }

        return 'childrens-camp.edit-registration.title';
    }

    /**
     * @return CssLoader[]
     */
    public function getCss(): array
    {
        return [
            $this->webLoader->createCssLoader('daterangePicker'),
        ];
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs(): array
    {
        return [
            $this->webLoader->createJavaScriptLoader('moment'),
            $this->webLoader->createJavaScriptLoader('daterangePicker'),
        ];
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/editRegistration.latte');

        $template->registration = $this->registration;
        $template->render();
    }

    protected function createComponentForm(): Form
    {
        // DATA
        $dataInsuranceCompanies = [];
        foreach ($this->facadeInsuranceCompany->getAll(true) as $insuranceCompany) {
            $dataInsuranceCompanies[$insuranceCompany->getId()] = sprintf('%s - %s', $insuranceCompany->getCode(), $insuranceCompany->getName());
        }

        $dataTransport = [];
        foreach ($this->registration->getCamp()->getTransports(true) as $transport) {
            $dataTransport[$transport->getId()] = $transport->getName();
        }

        $dataPayment = [];
        foreach ($this->registration->getCamp()->getPayments(true) as $payment) {
            $dataPayment[$payment->getId()] = $payment->getName();
        }

        $dataTshirtColor = array_combine($this->registration->getCamp()->getTshirtColorList(), $this->registration->getCamp()->getTshirtColorList());
        $dataTshirtSize  = array_combine($this->registration->getCamp()->getTshirtSizeList(), $this->registration->getCamp()->getTshirtSizeList());

        $dataSupplements = [];
        foreach ($this->registration->getCamp()->getSupplements() as $supplement) {
            $dataSupplements[$supplement->getId()] = sprintf('%s (%s Kč)', $supplement->getName(), number_format($supplement->getPrice(true), 2, ',', ' '));
        }

        // FORM
        $form = new Form();
        $form->setTranslator($this->translator);

        // 1. Část = Základní údaje
        $form->addText('childFirstName', 'form.childrens-camp.edit-registration.child-first-name')
            ->setRequired('form.childrens-camp.edit-registration.child-first-name.req');
        $form->addText('childLastName', 'form.childrens-camp.edit-registration.child-last-name')
            ->setRequired('form.childrens-camp.edit-registration.child-last-name.req');
        $form->addText('personalIdentificationNumber', 'form.childrens-camp.edit-registration.personal-identification-number')
            ->setRequired('form.childrens-camp.edit-registration.personal-identification-number.req');
        $form->addText('birthdate', 'form.childrens-camp.edit-registration.birthdate')
            ->setRequired('form.childrens-camp.edit-registration.birthdate.req')
            ->setHtmlAttribute('data-date');

        // 2. Část = Bydliště
        $form->addText('street', 'form.childrens-camp.edit-registration.street')
            ->setRequired('form.childrens-camp.edit-registration.street.req');
        $form->addText('city', 'form.childrens-camp.edit-registration.city')
            ->setRequired('form.childrens-camp.edit-registration.city.req');
        $form->addText('zipCode', 'form.childrens-camp.edit-registration.zip-code')
            ->setRequired('form.childrens-camp.edit-registration.zip-code.req');

        // 4. Část = Kontaktní údaje (Rodiče / Zákonný zástupce)
        $form->addEmail('contactEmail', 'form.childrens-camp.edit-registration.contact-email')
            ->setRequired('form.childrens-camp.edit-registration.contact-email.req');
        $form->addText('motherName', 'form.childrens-camp.edit-registration.mother-name');
        $form->addPhone('motherPhone', 'form.childrens-camp.edit-registration.mother-phone');
        $form->addText('fatherName', 'form.childrens-camp.edit-registration.father-name');
        $form->addPhone('fatherPhone', 'form.childrens-camp.edit-registration.father-phone');
        $form->addText('legalRepresentativeName', 'form.childrens-camp.edit-registration.legal-representative-name');
        $form->addPhone('legalRepresentativePhone', 'form.childrens-camp.edit-registration.legal-representative-phone');

        // 5. Část = Doprava na tábor
        $form->addSelect('transport', 'form.childrens-camp.edit-registration.transport', $dataTransport)
            ->setTranslator(null)
            ->setRequired('form.childrens-camp.edit-registration.transport.req')
            ->setDefaultValue(count($dataTransport) === 1 ? array_keys($dataTransport)[0] : null);

        // 6. Část = Způsob platby
        $form->addSelect('payment', 'form.childrens-camp.edit-registration.payment', $dataPayment)
            ->setTranslator(null)
            ->setRequired('form.childrens-camp.edit-registration.payment.req')
            ->setDefaultValue(count($dataPayment) === 1 ? array_keys($dataPayment)[0] : null);

        // 7. Část = Informace o dítěti
        $form->addSelect('swimmingSkills', 'form.childrens-camp.edit-registration.swimming-skills', ChildrensCampRegistration::SWIMMINGS_SKILLS)
            ->setPrompt(Constant::PROMTP)
            ->setRequired('form.childrens-camp.edit-registration.swimming-skills.req');
        $form->addSelect('insuranceCompany', 'form.childrens-camp.edit-registration.insurance-company', $dataInsuranceCompanies)
            ->setTranslator(null)
            ->setPrompt(Constant::PROMTP)
            ->setRequired('form.childrens-camp.edit-registration.insurance-company.req');
        $form->addCheckbox('confirmationForTheInsuranceCompany', 'form.childrens-camp.edit-registration.confirmation-for-the-insurance-company');
        $form->addTextArea('healthCondition', 'form.childrens-camp.edit-registration.health-condition');
        $form->addTextArea('washingFirst', 'form.childrens-camp.edit-registration.washing-first');
        $form->addTextArea('washingSecond', 'form.childrens-camp.edit-registration.washing-second');
        $form->addTextArea('washingThird', 'form.childrens-camp.edit-registration.washing-third');

        // 8. Část = Fakturace
        $formInvoice = $form->addContainer('invoice');
        $formInvoice->addCheckbox('isInvoice', 'form.childrens-camp.edit-registration.is-invoice')
            ->addCondition(Form::EQUAL, true)
            ->toggle('toggle-invoice');
        $formInvoice->addText('invoiceFirm', 'form.childrens-camp.edit-registration.invoice-firm');
        $formInvoice->addText('invoiceIN', 'form.childrens-camp.edit-registration.invoice-in');
        $formInvoice->addText('invoiceTIN', 'form.childrens-camp.edit-registration.invoice-tin');
        $formInvoice->addText('invoiceStreet', 'form.childrens-camp.edit-registration.invoice-street');
        $formInvoice->addText('invoiceCity', 'form.childrens-camp.edit-registration.invoice-city');
        $formInvoice->addText('invoiceZipCode', 'form.childrens-camp.edit-registration.invoice-zip-code');
        $formInvoice->addTextArea('invoiceNote', 'form.childrens-camp.edit-registration.invoice-note');

        $formInvoice = $form->addContainer('invoicePDF');
        $formInvoice->addText('invoicePrice', 'form.childrens-camp.edit-registration.invoice-price')
            ->setHtmlType('number')
            ->setHtmlAttribute('step', '0.1');
        $formInvoice->addText('invoiceDateOfIssue', 'form.childrens-camp.edit-registration.invoice-date-of-issue')
            ->setHtmlAttribute('data-date')
            ->setHtmlAttribute('data-date-clear');
        $formInvoice->addText('invoiceDateOfTaxableSupply', 'form.childrens-camp.edit-registration.invoice-date-of-taxable-supply')
            ->setHtmlAttribute('data-date')
            ->setHtmlAttribute('data-date-clear');
        $formInvoice->addText('invoiceDateDue', 'form.childrens-camp.edit-registration.invoice-date-due')
            ->setHtmlAttribute('data-date')
            ->setHtmlAttribute('data-date-clear');

        // 10. Část = Souhlasy
        $form->addCheckbox('consentToTheCampRules', 'form.childrens-camp.edit-registration.consent-to-the-camp-rules')
            ->setRequired('form.childrens-camp.edit-registration.consent-to-the-camp-rules.req');
        $form->addCheckbox('consentToPersonalDataProtection', 'form.childrens-camp.edit-registration.consent-to-personal-data-protection')
            ->setRequired('form.childrens-camp.edit-registration.consent-to-personal-data-protection.req');
        $form->addCheckbox('consentToTheProcessingOfSensitiveData', 'form.childrens-camp.edit-registration.consent-to-the-processing-of-sensitive-data')
            ->setRequired('form.childrens-camp.edit-registration.consent-to-the-processing-of-sensitive-data.req');
        $form->addCheckbox('permissionToSeparateAChildOnTrip', 'form.childrens-camp.edit-registration.permission-to-separate-a-child-on-trip');

        // 11. Část = Táborové tričko
        $formTshirt = $form->addContainer('tshirt');
        $formTshirt->addCheckbox('wantTshirt', 'form.childrens-camp.edit-registration.want-tshirt')
            ->addCondition(Form::EQUAL, true)
            ->toggle('toggle-tshirt');
        $formTshirt->addSelect('tshirtColor', 'form.childrens-camp.edit-registration.tshirt-color', $dataTshirtColor)
            ->setPrompt(Constant::PROMTP)
            ->setTranslator(null)
            ->addConditionOn($form['tshirt-wantTshirt'], Form::EQUAL, true)
            ->setRequired('form.childrens-camp.edit-registration.tshirt-color.req');
        $formTshirt->addSelect('tshirtSize', 'form.childrens-camp.edit-registration.tshirt-size', $dataTshirtSize)
            ->setPrompt(Constant::PROMTP)
            ->setTranslator(null)
            ->addConditionOn($form['tshirt-wantTshirt'], Form::EQUAL, true)
            ->setRequired('form.childrens-camp.edit-registration.tshirt-size.req');
        $formTshirt->addSelect('tshirtCount', 'form.childrens-camp.edit-registration.tshirt-count', [1 => 1, 2, 3, 4, 5])
            ->setTranslator(null)
            ->addConditionOn($form['tshirt-wantTshirt'], Form::EQUAL, true)
            ->setRequired('form.childrens-camp.edit-registration.tshirt-count.req');
        $formTshirt->addTextArea('tshirtNote', 'form.childrens-camp.edit-registration.tshirt-note');

        // 12. Část = Závěr
        $form->addTextArea('note', 'form.childrens-camp.edit-registration.note');
        $form->addSelect('status', 'form.childrens-camp.edit-registration.status', ChildrensCampRegistration::STATES);
        $form->addText('alreadyPaid', 'form.childrens-camp.edit-registration.already-paid');
        $form->addCheckbox('sendEmailByState', 'form.childrens-camp.edit-registration.send-email-by-state');

        // 14. Část = příplatky/slevy
        $formPrice             = $form->addContainer('price');
        $inputPriceSupplements = $formPrice->addMultiSelect('supplements', 'form.childrens-camp.edit-registration.supplements', $dataSupplements)
            ->setTranslator(null);

        foreach (array_keys($dataSupplements) as $supplementId) {
            $inputPriceSupplements->addCondition(FormRuleValidator::IS_SELECTED, $supplementId)
                ->toggle(sprintf('toogle-camp-supplement-price-%d', $supplementId));
        }

        // BUTTON
        $form->addSubmit('send', 'form.childrens-camp.edit-registration.send');
        $form->addSubmit('sendBack', 'form.childrens-camp.edit-registration.send-back');
        $form->addSubmit('back', 'form.childrens-camp.edit-registration.back')
            ->setValidationScope([])
            ->onClick[] = [$this, 'processOnBack'];

        // DEFAULT
        $form->setDefaults($this->getDefaults());

        // CALLBACK
        $form->onSuccess[] = [$this, 'processOnSuccess'];

        return $form;
    }

    /**
     * @return mixed[]
     */
    private function getDefaults(): array
    {
        if (! $this->registration->isLoaded()) {
            return [];
        }

        return [
            'childFirstName'               => $this->registration->getChildFirstName(),
            'childLastName'                => $this->registration->getChildLastName(),
            'personalIdentificationNumber' => $this->registration->getPersonalIdentificationNumber(),
            'birthdate'                    => $this->registration->getBirthdate()->format('d.m.Y'),

            'street'  => $this->registration->getStreet(),
            'city'    => $this->registration->getCity(),
            'zipCode' => $this->registration->getZipCode(),

            'contactEmail'             => $this->registration->getContactEmail(),
            'motherName'               => $this->registration->getMotherName(),
            'motherPhone'              => $this->registration->getMotherPhone(),
            'fatherName'               => $this->registration->getFatherName(),
            'fatherPhone'              => $this->registration->getFatherPhone(),
            'legalRepresentativeName'  => $this->registration->getLegalRepresentativeName(),
            'legalRepresentativePhone' => $this->registration->getLegalRepresentativePhone(),

            'transport' => $this->registration->getTransport()->getId(),
            'payment'   => $this->registration->getPayment()->getId(),

            'swimmingSkills'  => $this->registration->getSwimmingSkills(),
            'healthCondition' => $this->registration->getHealthCondition(),
            'washingFirst'    => $this->registration->getWashingFirst(),
            'washingSecond'   => $this->registration->getWashingSecond(),
            'washingThird'    => $this->registration->getWashingThird(),

            'insuranceCompany'                   => $this->registration->getInsuranceCompany()->getId(),
            'confirmationForTheInsuranceCompany' => $this->registration->isConfirmationForTheInsuranceCompany(),

            'consentToTheCampRules'                 => $this->registration->isConsentToTheCampRules(),
            'consentToPersonalDataProtection'       => $this->registration->isConsentToPersonalDataProtection(),
            'consentToTheProcessingOfSensitiveData' => $this->registration->isConsentToTheProcessingOfSensitiveData(),
            'permissionToSeparateAChildOnTrip'      => $this->registration->isPermissionToSeparateAChildOnTrip(),

            'note'        => $this->registration->getNote(),
            'alreadyPaid' => $this->registration->getAlreadyPaid(),
            'status'      => $this->registration->getStatus(),

            'invoice' => [
                'isInvoice'      => $this->registration->requireInvoice(),
                'invoiceFirm'    => $this->registration->getInvoiceFirm(),
                'invoiceIN'      => $this->registration->getInvoiceIN(),
                'invoiceTIN'     => $this->registration->getInvoiceTIN(),
                'invoiceStreet'  => $this->registration->getInvoiceStreet(),
                'invoiceCity'    => $this->registration->getInvoiceCity(),
                'invoiceZipCode' => $this->registration->getInvoiceZipCode(),
                'invoiceNote'    => $this->registration->getInvoiceNote(),
            ],

            'invoicePDF' => [
                'invoicePrice'               => $this->registration->getInvoicePrice(),
                'invoiceDateOfIssue'         => $this->registration->getInvoiceDateOfIssue()->format('d.m.Y'),
                'invoiceDateOfTaxableSupply' => $this->registration->getInvoiceDateOfTaxableSupply()->format('d.m.Y'),
                'invoiceDateDue'             => $this->registration->getInvoiceDateDue()->format('d.m.Y'),
            ],

            'tshirt' => [
                'wantTshirt'  => $this->registration->getTshirtColor() !== '',
                'tshirtColor' => $this->registration->getTshirtColor() ? $this->registration->getTshirtColor() : null,
                'tshirtSize'  => $this->registration->getTshirtSize() ? $this->registration->getTshirtSize() : null,
                'tshirtCount' => max($this->registration->getTshirtCount(), 1),
                'tshirtNote'  => $this->registration->getTshirtNote(),
            ],

            'price' => [
                'supplements' => $this->registration->getSupplements()->map(fn(ChildrensCampRegistrationSupplement $rs): ?int => $rs->getSupplement()->getId())->toArray(),
            ],
        ];
    }

    public function processOnSuccess(Form $form, ArrayHash $values): void
    {
        $birthdate                  = DateTime::createFromFormat('d.m.Y', $values->birthdate);
        $invoiceDateOfIssue         = DateTime::createFromFormat('d.m.Y', $values->invoicePDF->invoiceDateOfIssue);
        $invoiceDateOfTaxableSupply = DateTime::createFromFormat('d.m.Y', $values->invoicePDF->invoiceDateOfTaxableSupply);
        $invoiceDateDue             = DateTime::createFromFormat('d.m.Y', $values->invoicePDF->invoiceDateDue);

        $supplements = Arrays::map($values->price->supplements, fn(int $sId): ChildrensCampSupplement => $this->registration->getCamp()->getSupplement($sId));

        $registration = $this->facade->update(
            $this->registration->getId(),
            $values->childFirstName,
            $values->childLastName,
            $values->personalIdentificationNumber,
            $birthdate,
            $values->street,
            $values->city,
            $values->zipCode,
            $values->contactEmail,
            $values->motherName,
            $values->motherPhone,
            $values->fatherName,
            $values->fatherPhone,
            $values->legalRepresentativeName,
            $values->legalRepresentativePhone,
            $this->facadeTransport->get($values->transport),
            $this->facadePayment->get($values->payment),
            $values->swimmingSkills,
            $this->facadeInsuranceCompany->get($values->insuranceCompany),
            $values->confirmationForTheInsuranceCompany,
            $values->healthCondition,
            $values->washingFirst,
            $values->washingSecond,
            $values->washingThird,
            $values->consentToTheCampRules,
            $values->consentToPersonalDataProtection,
            $values->consentToTheProcessingOfSensitiveData,
            $values->permissionToSeparateAChildOnTrip,
            $values->note,
            floatval($values->alreadyPaid)
        );

        $this->facade->updateStatus($registration->getId(), $values->status);

        if ($values->invoice->isInvoice) {
            $this->facade->updateInvoice(
                $registration,
                $values->invoice->invoiceFirm,
                $values->invoice->invoiceIN,
                $values->invoice->invoiceTIN,
                $values->invoice->invoiceStreet,
                $values->invoice->invoiceCity,
                $values->invoice->invoiceZipCode,
                $values->invoice->invoiceNote
            );

            $this->facade->updateInvoicePDF(
                $registration,
                floatval($values->invoicePDF->invoicePrice),
                $invoiceDateOfIssue,
                $invoiceDateOfTaxableSupply,
                $invoiceDateDue
            );
        } else {
            $this->facade->updateInvoice($registration, '', '', '', '', '', '', '');
        }

        if ($values->tshirt->wantTshirt) {
            $this->facade->updateTshirt($registration, $values->tshirt->tshirtColor, $values->tshirt->tshirtSize, $values->tshirt->tshirtCount, $values->tshirt->tshirtNote);
        } else {
            $this->facade->updateTshirt($registration, '', '', 0, '');
        }

        $registration = $this->facade->updateSupplements($registration, $supplements);

        if ($values->sendEmailByState) {
            $mailQueue = $this->serviceChildrensCampRegistration->sendByState($registration);
            if ($mailQueue !== null && $mailQueue->getStatus() === MailQueue::STATUS_SENT) {
                $this->onFlashmessage('form.childrens-camp.edit-registration.flash.flash.success-mail', Flash::SUCCESS);
            } else {
                $this->onFlashmessage('form.childrens-camp.edit-registration.flash.flash.danger-mail', Flash::DANGER);
            }
        }

        $this->onFlashmessage('form.childrens-camp.edit-registration.flash.success.update', Flash::SUCCESS);

        if ($form->isSubmitted()->name === 'sendBack') {
            $this->processOnBack();
        }

        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'edit-registration',
            'id'      => $registration->getId(),
        ]);
    }

    public function processOnBack(): void
    {
        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'overview-registration',
            'id'      => $this->registration->getCamp()->getId(),
        ]);
    }

    public function handleLoadFromAres(?string $val = null): void
    {
        try {
            $ares = (new Ares())->findByIdentificationNumber($this->getPresenter()->getParameter('val'));

            $this['form']['invoice-invoiceIN']->setValue($ares->getCompanyId());
            $this['form']['invoice-invoiceTIN']->setValue($ares->getTaxId());
            $this['form']['invoice-invoiceFirm']->setValue($ares->getCompanyName());
            $this['form']['invoice-invoiceStreet']->setValue($ares->getStreetWithNumbers());
            $this['form']['invoice-invoiceCity']->setValue($ares->getTown());
            $this['form']['invoice-invoiceZipCode']->setValue($ares->getZip());

            $this->redrawControl('snipForm');
            $this->redrawControl('snipFormInvoice');
        } catch (Ares\AresException $e) {
            exit;
        }
    }

}
