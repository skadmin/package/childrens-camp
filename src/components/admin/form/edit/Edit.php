<?php

declare(strict_types=1);

namespace Skadmin\ChildrensCamp\Components\Admin;

use App\Components\Form\FormWithUserControl;
use Skadmin\ChildrensCamp\Mail\CMailChildrensCampExtraordinaryEvent;
use Skadmin\ChildrensCamp\Mail\CMailChildrensCampSatisfactionQuestionnaire;
use Skadmin\Role\Doctrine\Role\Privilege;
use App\Model\Doctrine\User\User;
use App\Model\System\APackageControl;
use App\Model\System\Flash;
use App\Model\System\FormRuleValidator;
use SkadminUtils\ImageStorage\ImageStorage;
use Nette\ComponentModel\IContainer;
use Nette\Http\FileUpload;
use Nette\Security\User as LoggedUser;
use Nette\Utils\ArrayHash;
use Nette\Utils\Arrays;
use Nette\Utils\DateTime;
use Nette\Utils\Html;
use Skadmin\ChildrensCamp\BaseControl;
use Skadmin\ChildrensCamp\Doctrine\ChildrensCamp\ChildrensCamp;
use Skadmin\ChildrensCamp\Doctrine\ChildrensCamp\ChildrensCampFacade;
use Skadmin\ChildrensCamp\Doctrine\ChildrensCamp\ChildrensCampSupplement\ChildrensCampSupplement;
use Skadmin\ChildrensCamp\Doctrine\ChildrensCamp\ChildrensCampSupplement\ChildrensCampSupplementFacade;
use Skadmin\ChildrensCamp\Doctrine\ChildrensCampMailTemplate\ChildrensCampMailTemplateFacade;
use Skadmin\ChildrensCamp\Doctrine\ChildrensCampMailTemplate\ChildrensCampMailTemplateFake;
use Skadmin\ChildrensCamp\Doctrine\ChildrensCampRegistration\ChildrensCampRegistration;
use Skadmin\ChildrensCamp\Mail\CMailChildrensCampRegistration;
use Skadmin\ChildrensCamp\Mail\CMailChildrensCampRegistrationInvoice;
use Skadmin\File\Components\Admin\CreateComponentFormFile;
use Skadmin\File\Components\Admin\FileDownloadByFacade;
use Skadmin\File\Components\Admin\FileRemoveByFacade;
use Skadmin\File\Components\Admin\FormFile;
use Skadmin\File\Components\Admin\IFormFileFactory;
use Skadmin\File\Doctrine\File\File;
use Skadmin\File\Doctrine\File\FileFacade;
use Skadmin\FileStorage\FileStorage;
use Skadmin\Payment\Doctrine\Payment\Payment;
use Skadmin\Payment\Doctrine\Payment\PaymentFacade;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use Skadmin\Transport\Doctrine\Transport\Transport;
use Skadmin\Transport\Doctrine\Transport\TransportFacade;
use SkadminUtils\FormControls\UI\Form;
use SkadminUtils\FormControls\Utils\UtilsFormControl;
use SkadminUtils\Utils\Utils\Strings;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;
use DateTimeInterface;

/**
 * Class Edit
 */
class Edit extends FormWithUserControl
{
    use APackageControl;
    use CreateComponentFormFile;
    use FileDownloadByFacade {
        CreateComponentFormFile::checkFileObject insteadof FileDownloadByFacade;
    }
    use FileRemoveByFacade {
        CreateComponentFormFile::checkFileObject insteadof FileRemoveByFacade;
    }

    private LoaderFactory                   $webLoader;
    private ChildrensCampFacade             $facade;
    private User                            $user;
    private ChildrensCamp                   $childrensCamp;
    private ImageStorage                    $imageStorage;
    private TransportFacade                 $facadeTransport;
    private PaymentFacade                   $facadePayment;
    private ChildrensCampMailTemplateFacade $facadeChildrensCampMailTemplate;
    private ChildrensCampSupplementFacade   $facadeChildrensCampSupplement;
    private FileFacade                      $facadeFile;
    private FormFile                        $formFile;

    public function __construct(?int $id, ChildrensCampFacade $facade, Translator $translator, LoaderFactory $webLoader, LoggedUser $user, ImageStorage $imageStorage, TransportFacade $facadeTransport, PaymentFacade $facadePayment, ChildrensCampMailTemplateFacade $facadeChildrensCampMailTemplate, ChildrensCampSupplementFacade $facadeChildrensCampSupplement, IFormFileFactory $iFormFileFactory, FileStorage $fileStorage, FileFacade $facadeFile)
    {
        parent::__construct($translator, $user);
        $this->facade                          = $facade;
        $this->webLoader                       = $webLoader;
        $this->user                            = $this->loggedUser->getIdentity(); //@phpstan-ignore-line
        $this->imageStorage                    = $imageStorage;
        $this->facadeTransport                 = $facadeTransport;
        $this->facadePayment                   = $facadePayment;
        $this->facadeChildrensCampMailTemplate = $facadeChildrensCampMailTemplate;
        $this->facadeChildrensCampSupplement   = $facadeChildrensCampSupplement;
        $this->facadeFile                      = $facadeFile;

        $this->childrensCamp = $this->facade->get($id);
        $this->fileObject    = $this->childrensCamp;
        $this->fileStorage   = $fileStorage;

        if (! $this->childrensCamp->isLoaded()) {
            return;
        }

        $this->formFile = $iFormFileFactory->create();
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null)
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    /**
     * @return SimpleTranslation|string
     */
    public function getTitle()
    {
        if ($this->childrensCamp->isLoaded()) {
            return new SimpleTranslation('childrens-camp.edit.title - %s', $this->childrensCamp->getName());
        }

        return 'childrens-camp.edit.title';
    }

    /**
     * @return CssLoader[]
     */
    public function getCss(): array
    {
        $css = [
            $this->webLoader->createCssLoader('daterangePicker'),
            $this->webLoader->createCssLoader('customFileInput'),
            $this->webLoader->createCssLoader('fancyBox'), // responsive file manager
            $this->webLoader->createCssLoader('tagsInput'),
        ];

        if ($this->childrensCamp->isLoaded()) {
            foreach ($this->formFile->getCss() as $wl) {
                $css[] = $wl;
            }
        }

        return $css;
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs(): array
    {
        $js = [
            $this->webLoader->createJavaScriptLoader('adminTinyMce'),
            $this->webLoader->createJavaScriptLoader('moment'),
            $this->webLoader->createJavaScriptLoader('daterangePicker'),
            $this->webLoader->createJavaScriptLoader('customFileInput'),
            $this->webLoader->createJavaScriptLoader('fancyBox'), // responsive file manager
            $this->webLoader->createJavaScriptLoader('tagsInput'),
        ];

        if ($this->childrensCamp->isLoaded()) {
            foreach ($this->formFile->getJs() as $wl) {
                $js[] = $wl;
            }
        }

        return $js;
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/edit.latte');

        $template->preheaderTooltip = new SimpleTranslation('mailing.form.edit.preheader.title-content %s', [
            sprintf('<img src="%s" alt="preheader hint" class="my-2">',
                'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAgMAAABgCAYAAACAJHGKAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAAEnQAABJ0Ad5mH3gAACNXSURBVHhe7Z3PbxNJ2sf3TwGJXOCwq5GQZjkAByQkJGCkgZGQdhTJE4tkMD+SSazJ4J1EibzIAWFBnB0nCgnCm2CCIysZBbASxjCKMrPWKIvWiibRwoU5IM2cckGI9/C8T3VVdVdXV9ttx0DAz+FzcHV1dXX9er5d9VT5Tz/+D4AgCIIgiOaFxABBEARBNDkkBgiCIAiiySExQBAEQRBNDokBgiAIgmhySAwQBEEQRJNDYoAgCIIgmhwSAwRBEATR5JAYIAiCIIgmh8QAQRAEQTQ5JAYIgiAIoskhMUAQBEEQTQ6JAYIgCIJockgMEARBEESTQ2KAIAjibVEuQ/LrKETORaEnVYYlUxyCeAe8UTHwfaoNduxsg8GfzNcbx+8w+Nku2PHZDHxvvF4rmzAc/RwOhOcblN67Y+FmHEKRUciW8ffqI4iFOyGx8NIYtyYWRqH1i3ZMy3DtDZG93I7PxHcxXHtzrEIC37P18qrh2nbgBaS/wfyF45BeMV3/QNlYhysRfG/Ztk1xKoLlFsX7o/dgwXid42lzqyWIR6MQ+9cLESdYOpLCrTj03WX97yVMDmCdlczxCOJtU6cYeAXZsUtw4tBfoWUnGuGde+DPRzuh987vrni1i4FXkO7cg/d8CYlfTNf9aLwYuBraAy2fzMCs8XqDKd6GCBqcSPqZ+TpSmI5bxncg/9p43Z+XOKB1Qiieg+toNNqTZSga49UIiYFtgSX2vmiQwHtveA3ZZOcWBVC9YoAJ6nbomngu4pAYID4M6hADr2AiehgN9kdw5FwGEndWIH1nHnpDx1AY7IED/avwUMStWQz8pwCnLHGxC46n3MKiMo0WA2+b5zDShQPKmQzkjNc3YfwiXg/joLRhul6FtTIk8Csq9A0OWPXcb4LEwLtn7RmMXEQRflN+pSI/34MezG+PGvah8csyxLs6YWB2KwKoTjHgoTYxQMsExHaldjHwOAOH0FgfuvzUcy17+SQa8pPw7WP+u1Yx8GDsS4zfCx1fHYQdRzI1fJW/72KAfeH146DTCVcWDddXF6EXB/jwtbL32ruCxMD2pBnEQEN4R2KAILYptYuB/CXry70jb7j2eAZOHD4GbTf5V71JDDzE+/ft3APHr/3m3GfxG3x7ZBe0dK7Aw4UR2LvzIHQ/UK87PFycgbYjH1n5aNl7EtpSReh1iQFHHEzkRuDEfiXuGHvuK8im+uHQXrYksQt27/8SunPqTIQuLpzfWfbso2J55C/s2U/tmZAtIQ3+8Lrnmi0UCkr4k3UYvxyH9tNssMKv/kgcrsz94bqPTacu5DPQGzlrxWk9jV+Rlxchr6+xbmxC/mYKzndUSktDigGMN3XNyUf46wxM6Us8QdLX43TEIJYqQUGZyTAPzC8hPz3qfsdrJVj4VY3jR5B7pRhYdufvwhBcv79piId5nnPKPNTRbyjLas99DbcuYTibKdJmcnLDnXhPCm6t4W+X4RfPd9EP6Z+V+7HNpOMxCIfZtbPQftFQVy4cQ5cv3oOBrrMQYumejsLAzWeer9qlpUWIX+zkcQKlL9v2EIwXS3BF3hs+Cz1YHmrdW/yKbS015Gojej54ev0wMvcIBi6w8tXKwMZkxPmSGlt+GFnmy3GeNucRW8HSkfnS8/JuBC5BeKl7ZqCFOdetG64reMTA8gwc370L9kWdpQQbK909QmQ8hW8P74K9/WvuOAyRRsuhXvg2swLpzAx0HOVG3WO8d++B3Z54B+HQ0cPQcvQSDLrCVdHiIwY+Pgz79n4O3WNiaeRvbLnkIOb5lbhvK4ilAM9SgRhsLuRgXoaVceBn0/7nkjBytwRTc8swMsCMRCck7js+BYXskDW4diUXMQ7Gm74BXcwQRFQjIweuGMRvLlvxxpMx675YVjV2GkIMtEeiEBnMwSTeN3lzlKdvHBgrp8+NXCf0jfE4k2NJaMc4ob5HUBBxvAOnWDtGw9N1jb+jzEP1JZGg9wojy8K7RyHNyvvuPYh3s7yoMzkiXqQTIrJe7HiqEQj4XCxf5gvgnil6BtcvYHrxZe734TJMr6H4ZBMNcg66MKwrvQ5LT15CUaYn24x8h7lFSLC8hYdgclWmryPa3plOaGeiZprfl0SjbbW1BaetLWF+WX3JtjB1Nwd9loPfDcgy4eJK14EbScwXCgxe98uQTvD2EcL3tA39BuaFOUqelvlw2lHPLWcWxE4PBcX5vlFIJOcg+8R5noPXiJv8L+oRA6Z0SAwQ2526HAhn0cizL+OWvW3QkVqB7L/NxtAlBqQRPzUDswYRkRk8iMZ2BDLi2uzVk2jMsfO4BpJNuNqOhhvDh/+rhK8/hV4UD17jfQnSarz/FqGViQY9/IcMHMDw49/J2QEfMbAb32VZuW+tCG2sHGKNmUIuzqZwYNAMgBh8zo9JhyU08jMpiOCX6bg6sGyU4coZHFikofjfH3zw7J5TDDMO2neT1mCZuM9/F3EQD+Mz43Pq+utLuBXHe319GBApBi6vur7M5tNRDHcco4KlX4YEExGJkhIHIP9Pd1qegXM5B+fZwDzhGAOGNEy90xXETOB7pZHXvtLLJYiz8kaxwt9fxsP8qTMvj3OWc2jPLTE7EPS5G5gelolrpkjcazuRegyTTxgTIEyQnRmFKTVv5WUY0J/hQhg63VFvDe/DZ4SSctnqD5iMoyjsW3S1tR8LGax7zO+sIxp0uJHU24cUhzH8shZhi7fh/IUoXBHtliMEtCKUpRhwv78JtxHnBtx7X61iwC8dEgPEdqcuMcB48GAeOk7J3QRCGIy5p8xtMfDDKpzb7y8Eflxfg+6Pd8HeQWUm4CcUDzv3QNsdRWisr0AHe1ZUN74+xtv+LVm17t/x1YoS5oQ7TotB0/sNBj/BcE9+6mStxAdnxTeADyLKoOiL/oUip5pTMFnyH4ytwchg9JeyQ8bBy0aIAY/PwBwLd+4Llr5woIzehrzxK46jD5xceOCXrece8QVtG2ovwe8VRt7gM5BNqvnxibeBokExDrXkOXcNDaJu6MJiiYCFBRUDJmFhsQmTffhM5avWjferV4aPsFmFan4URmHixs9I/rgyZ81w2CLKB71N+KbnwXm3+fs3LCHWY+8QcPAY6wpioFI6JAaI7U7dYsBm7XfIZjLQ9ilfl9/XXrANJhcDx+D4KTadjsbW4yfAeXinH0WF43jI2YREKxraUAEeyDBLIJh2GtQoBjzGu14xIML9xIAYOJjR5FQbpPS1YmEkLy7aU+U2pRIk+/rttXobdeBmXtfneDhbpx9I3YPsiioMxECm3u+iQn79xIAVLu8Lnv5SMQe90u+gawjiY8uQX3WLGH3grDSQWtcqzGwEv9dfDLgHeL94PFwaj5rybH1ZR+G61S/4V3DoUsnZGhpUDHjaoUYdYsAKd70r80+5DbGo9BlwqEsMiHJzCRjhJxMRPgMOTnnWLAYuxKGHLWfgPSOGbYqe+vITA1XS8ctXpfZAEG+TrYsBhdmrzPjLdX8pBviMwMRYJzf4P+j3ybMF0KgaUc4ceN/EwNoLyLL1U5syLFRYP7WwDIBwFhTTwp7p7sc5a425fSAHU4//gCW2VowD5XX2taYP3BuvofC4BOlUynZYiww+EnHkQDYK4658BshvLWIgaPq/voT5xUW4npBOYmettW9p/PSBs9JAOpXAa3WKAfe9b08MePIsln6sJaInjyCG6bjOmahRDEQSwndEp/DC5+wJUX8BxMD8GFu/Zz4fJciXWHt0/BcaIgbKJRhAsRjqvgHji89Fm9+ESSaelfL0T09HvIOVvzKMsxmSrhzMq0tBiKe+/MRAlXRIDBDbnZrFwPBXx+DA4XGYMFzTjTUXA5+LdXZ+kM+O/VdhQjUAv8zDCbznUG8B0taZBSrj/NpVMaMglwk8a/TbVAzUgzAAbKmADbCmKWVretpz5oA2cKMIsAZMlzF/DfPjbFCSPgOvnbV7bfCqSiAxEDB9FAEsr0XVi39jE40jWzf29xl458sErvwEEwO15tlaKsA6nWf+JOoSASOoGBBr/LVvTQ0qBrjPR0h/d1NeNHyNt7ZMwA7rCWJMaxYD0qdGHCakH8oVWAxUSYfEALHdqVkMcAN/EDpyXqfBh9Nsun8XtE3za7bPgPTS/2nes5uAny2gevKrvILhc2hs7TMHxCyC7gAofA4+CDGAWM5TZ+LQ06VNCwvkADKlGljp0CYHbvEl6TEAwqFNGvGlmaQ1rRub1tZmy2WYcm2d0wgkBgKmv8gdzfQZEO5Q6aTlGTjfqgMhPtflfMfL26mfYGKg5jxbZROD86wtBDG2RgOMX9ADmLfwEIxrJ94tFUo+3vaMoGJAvLvmALo0x5xH9by44UbS60A4n2ZC2PGV4fHwtzoFv/ECRlg+tiIGlHdbQMHRkN0EhnS4L43eX4QDJIkBYhtQ+zLB+m+QaGX+AeoJhEUYjHXCAW23gEcMsLDv2E4EKSb42QJeI+vABAYTH92yExm3Fn4ELRj2oYgBaTDY4GE6friYT1kGNhTl28TGx1LQJX0H7EHJu4XN2u7FfAjUL0y5ZQsHr96UiDd9G3rZNrRv5mDeb79+QDEQLH0RJyy3l7E4Gf5ObMpVpO39inrLWwvtLYOiHNE48fV8JV41MVBzntf5LhFTWZsMv3BCDX1z29ril5fbBlcwLoa3RuKQlFvzUnHb4W1rywTC14W90+V7+NxFuB7vt9ooy3d1MYDx1K2n1/i9rp0qok+Ezg1Z+Z+8eQNiwh+mUWLAbofKrot6xIApHVkvrRFl22n0LIRYGIkBYhtQn8/A+iZkM+Nw6tPDsJsZV2T3/s/h1D9WXGcPmMQAM6CJU2x7ICrnPD+zwDHCBsT2PfXMAe+hQ09hOBrEeL8nYkA6DlY4fpgdbNMjD18Rh/hYa844sDgzBtrhNtbe69uGQ4G8h7mwA38qHtwTVAwwgqSvxTEdkOQVA4y3c+hQKKkfOpSCdFH9mg0qBhi15dmaKTK1BeMsABrELObTEodn3QdV/cKcTuWhQ+2Wo2YyX8lb32TolHD1XVn9oRF3HWhU4jsp9BkDFWm8RxaqHzq0xA4+sg4S4m2IHTiUnxAzCGLGYGtiAGFliu8Q6lu0nl+fGBDxlHRYmJp/WefZCZZfEgPEu6ehDoQEQRC1ENx4EwTxJiExQBDEO4PEAEFsD0gMEATxziAxQBDbAxIDBEG8M0gMEMT2gMQAQRAEQTQ5JAYIgiAIoskhMUAQBEEQTQ6JAYIgCIJockgMEARBEESTQ2KAIAiCIJocEgMEQRAE0eSQGCAIgiCIJofEAEEQBEE0OSQGCIIgCKLJITFAEARBEE0OiQGCIAiCaHJIDBAEQRBEk/On4vr/AUEQBEEQzcuf7v1nEwiCIAiCaF5omYAgCIIgmhwSAwRBEATR5JAYIAiCIIgmh8QAQRAEQTQ5JAYIgiAIoskhMUAQBEEQTQ6JAYIgCIJockgMEARBEESTQ2KAIAiCIJocEgMEQRAE0eSQGCAIgiCIJofEAEEQBEE0OSQGCIIgCKLJITFAEARBEE0OiQGCIAiCaHJIDBAEQRBEk0NigCAIgiCanDcqBr5PtcGOnW0w+JP5euP4HQY/2wU7PpuB743Xa2UThqOfw4HwfIPSe3cs3OyH1i/6If2z+XrNPM7B+S/aof1yCZY2DNcrsgoJvLf18qrh2ltgYRTLoh0SC4ZrNZK9jO/xxShkZdhqCeLRKMT+9cIT1+EFjPdFoWu4DEXj9e1FfjwOkWgGsmvm69VYKuQgduGsVeahwWVYMsT5YNhYhysRbBMRbBNlw/UAFO/fgHarb60GK6uNFzASbYeeCd7mFm7GIRS5UXd9ObyG7HA/RPoWYcF4nWgIK/egJ9yJ49FL/L0Jk3163b+ANNZva/SeUw+P56D3Qj9cuf9axGkcdYqBV5AduwQnDv0VWnaiEd65B/58tBN67/zuile7GHgF6c49eM+XkPjFdN2PxouBq6E90PLJDMwar78BfsaGwQylymks08v3IFuqv+IbKgY2nsH1LtY452ChZiHA+JDFwCOIhduha+K5J64Dlt+598cw5oY7sQ2OwlQ9xkWUR6h7FNJ3SzD1eNMc74MAjWcSyyoch/SK6XoAytg3UEyEvsGBv5a+JQ1KahTat/J8F5twa/AshM7lIG+8TjSKJRyT2sMpuJ42CTmDGFjMoGA8C/G5bSEGXsFE9DAa7I/gyLkMJO6sQPrOPPSGjqEw2AMH+lfhoYhbsxj4TwFOWeJiFxxPuYVFZRotBt4BQgxEEoswNYeDJzJ58wbE0Hi0hqNCPRruq0IjxUDh+xvQE9nKgPMBiwHCxVJ2CMsnCtcfm69/UPyyDPGuThiYra+PMgqzo3D+Qn2zCpZB+UJ+YZrjvDUa2Meag0pC0iAG3iC1i4HHGTiExvrQ5aeea9nLJ9GQn4RvxQBQqxh4MPYlxu+Fjq8Owo4jmRq+yj8cMdBzU5tm3vgDJuOisdRh0Bu+TLAlSAw0C9ur3RFvDRIDDWS7i4H8JevLvSNvuPZ4Bk4cPgZtN/lXvUkMPMT79+3cA8ev/ebcZ/EbfHtkF7R0rsDDhRHYu/MgdD9Qrzs8XJyBtiMfWflo2XsS2lJF6HWJAUccTORG4MR+Je4Ye+4ryKb64dBetiSxC3bv/xK6c+pMhC4unN9Z9uyjYnnkL+zZT+2ZkC3hJwYY5WXow2vha2V3+C8lSPbFIBxmhgmvd6UgveSejpWD8sj9Ely52AkhjNcaPgs9lx/B/K+GeHOPYMBa52UDuWiMZzKQU9KU61tWeIUpzcJcBno6eN5CkThcmVuEOHu+LgZK7rydj9+DvPqFJAeYuT9g6loc2k/zNMNfZ2CqluUkmc7dZ5COy3LD5/XlIPtExBH1EB5ed9/75BHEWLioA48YqFR/NqbO/RoW8hnojfC1db40tOh6f786PB9ftOpwqXgPBrrO8nB2/7USFPR6ebLueuf2i9XLzvOOgepBCD4XblGwVMR2IN9D5sW1FCbLaQ6msik4z54jyqwRZVH5+a/h1iUMN7Rta9nkixTcklO5v2I5pIbgvGzjHTEYuPnMtQRki6Ki1ua0Nl6zuBTtzV3OEi2dRtQ9w5UOG2+GIIltwbpmzI92f7V+riPamxFPH7oNMVnvvmNIJ45BleuB8xLy06PuPontaEEZL50Pm2XI38Q2qoxzyfs4BuNHnKuPsLG5qM3cbGx67r0iy9PCMF7IPlhNbFWqK582XvfMQAtzrls3XFfwiIHlGTi+exfsizpLCTZWunuEyHgK3x7eBXv719xxGCKNlkO98G1mBdKZGeg4yo26x3jv3gO7PfEOwqGjh6Hl6CUYdIWrosVHDHx8GPbt/Ry6x8TSyN/YcslBzPMrcd8WqGhMsPLiovJkGIuPFR06l4QRtiaLhjaJHY01eHW6kA9GGC+MDTrFlyDGU9hIWVjfIyh44mEH6RuFRHLOMpCF6biV5pVF8VzG2jIMsAamG0wFPnWJ6UUzMM6WPaZvY+fiDdMlBqw1z3YcoG7zeHdz0Gc5YikNVXSA9kgUIoM5mLSWUEahizX0WlSzSIe9Y1finjsd+3mbMH4Rf2sCqDibcpVDo8RAITtkDWBdSbE8NH1Dy49SN6ej0De2jPGWIZ2IWfeFu2PQHo5B/KY73JUPuR4t1++xrSS68RnhIZhcFXEM+ImByvXwGopPcJBLxzBuDK6jOF168hKK4l2kk5zdLu7eg7iVF3WaVJQTpsveOZa4AYnxsmVkt1oWgZ6P7xnS2zzz97iA8eLL3PlzA/P4Df4+3Q9XplmZYr9Kiufdcp7H89uJZYZt7pq7/4WxH0hH0prFwNoLyLL8q9xK8XdjfgcyXqPq3naQlOPNMowMsPGmHWJZZvxeYz1jXd9l/aQdBu7yerfTDNLPdVafud8PuYVlZ7XBpOOEazlOomGX5Sv7kKsc1L6v1UNrVw7m7TyIaXslPdnG3f4cQgxgeNgzxsWgC8vYflcZfgbL0/YJeIllzGZ8ZXt12o9VnlacOsVAtbpicQxtvC4Hwlk08uzLuGVvG3SkViD7b7MxdIkBacRPzcCsQURkBg+isR2BjLg2e/UkGnNU1C6Hik242o6GG8OH/6uErz+FXhQPXuN9CdJqvP8WoZWJBj38hwwcwPDj38nZAR8xsBvfZVm5b60IbawcYg2Y9q5iTNydUyq7UZhSVa0coBTFJwdPp4GJ8Ak+SMnGION5nr+6CL0YHkk/s8OK+ZRhsFR5BknWGF2dDBEzHI4YkI1dc0gUOxbUhsvypntZz6ejGI6DeMkJq4hMRxMxS3OjEFaexwWQut6tKGlxT2PEwB+8vrrx/ZV4S3eTVj4T9/lvaVDic+qXxUuYHGB50MOF0cKBiws9HNzYoKO3FayLATaQVRB0vmIgQD3wPOvLBM9hxOSAKgxW68VFkWdRToZ11K2VRcDnb+BvvWyWeZscyIsZhMXbcP5CFK6IOuIIIXkB270IM/crIe7DN+yyrVkMeBB937Wc2MC6RxHFDFV8Tom3gf3cVb6I0VgF7OfVWCvzenI52q3D9S4Up8PuWVNPHxb5kjsv7HhCjNv1KupZjyc/bnqnZV6FGNDfqXgbIoZw/jHh9Okiphc2tGP3R58+XiDG8tUIUleGNl6XGGA8eDAPHafkbgIhDMbcU+a2GPhhFc7t9xcCP66vQffHu2DvoDIT8BOKh517oO2OIjTWV6CDPSuqG18f423/lqxa9+/4akUJc8Idp8Wg6f0Gg59guCc/dVCLGKjwZa5/wZoHZaTkfp5vPLkkoAxwVl4UweFhZQ66jO8iOpAUA+KdvYMBKtszSjy/DoBG3JxnH3w7Eg4y2DHs54klAVsAyY6jLNN4Bsu6xIAUGSmYrLBjxK9uZB6mtHqwwuUzTIbNQtSrOtBoeN6xhnow5hkHfzZQmgZ/Lijwa9VarjEMgoItlUXg5wPkrqER1Y16WFki8EEvM7/8LkywmRMnnqesa4R/HaNRU50YG1n30kj+c73ylmJTGwnazysivqTZbFMQp1Q9H35td6NkLV2Gkrxv6+3AQQjLvkdCCGtjmUS8a5cmJmS4fL5VvtrsI4M73sr2UqcYCFhXehuvWwzYrP0O2UwG2j7l6/L72gu2weRi4BgcP8Wm09HYevwEOA/v9KOocBwPOZuQaEVDGyrAAxlmCQTTToMaxYDHeNcrBkS4nxgQDYBVHsc7KOhxA4mBSnHFNdnx/AYjvTH7x5MNVKps3oE9/gsqvg1W60Aini9aPE96VniFMtXxzZfe6TQBtJixVPyVgnOPZ7CsUn8cQ+dmnuhsxwjey9YMB1L3ILviFgbVDKBuQKxw+QxPG9SoxSDUUA/GPPuWv7wmRWz9YqBiWQR+PlJgdS7bPP/iD10quc+HeLIO45fjEBFrvg5OPvzyy8OdeH75DwSKb1bH4UTJNWPT0LpHY5wf44LDWpPvG4Xrd59BwbWOjpjKWIT5EkAMyNk7/Yvdgq29szX+c8JnQMHOh2/di7YmjHylerCu2Qa8shjwjAMinD9fPJPdb0S2F0M/qNSGbQLWldbGty4GFGavMuMv1/2lGOAzAhNjndzg/6DfJ88WQKNqRDlz4H0TA551vTIs+H1ZVDQmisNetbjiq7yRYkDORJwfe240jB58G6xZDPT+Uy0jBbk33S89K9wnzyZ88+XtdHyGJQYjy8JxTJsJ8QwaFetP4mPkNl5D4XEJ0qmU7bQUGXxkx9mSART5Uresuii88D0AyZN+DfVgzLNv+SPW7MI7FAOu5yMbZUv0Wm1ezBTZU8mMcgkGTqNA6L4B44vP+Xo5MslmepR8+OWXhzvx/PJfFXn2h+mwo0bWvaT8HHLTOYhLx+XT/XB9USkXUxmLsKr93A+xrGE+g+El3LqEfSYcg8T0OsyLepC+C3Y+fOtetLUAYmAqgdcaKQYujHKfAg/SThj6QaU2rFOtrrQ2XrMYGP7qGBw4PA4Thmu6seZi4HOxzs4P8tmx/ypMqAbxl3k4gfcc6i1A2jqzQGWcX7sqZhTkMoFnjX6bioFaqGRMxLq9/TXeiGUC7Xm+8SzEGmdXDibZNJpmGD3IZYJbqmcsQ+tAYtrWWYfzwa8DWOF+eTbgl46YTnV1bFsAPbKmB/WZEM+gUan+bLTOLZ2uXALxNcyPs7rAfLp8Buo0gLKtVJrJ8cGTfg31YMzzW1gmqFgWNSwTMKxpVLx3nvUpbYmgcIutSVfPh19+ebgTzy//1eA7HDqxrRiWmRpZ92XNIZDxpARxNs1fzWcgaD83Ij6E/LZWa8udNno+fPv+m1kmqCwGxHhabRytVwwErStEbeM1iwFu4A9CR87rNPhwmk3374K2aX7N9hmQXvo/zXt2E/CzBVRPfpVXMHwOja195oCYRdAdAIXPwQcpBqRToOYYVKsDoZ4udyDkX75OPO+gJeFOg2chhF9D1QcXH0ctIWqcDiQcDZlHsTaFtYAK2d766NcBrHD/PHsQ6egOcHIK0vXlJ8v4NJt69M6EeAbLesSAtl3RRgye8n23ZADZYMqc68JDMK45Wi4VSs6WSgOe9GuoB3Oea3QgVAdBwdbKIujzBdYsWAzO4z0hbdDn+cD+ozo4iuOB1Xz45ZeHO/H88l8J6dSmO8Q6NK7uueiIw7hrB4Iwamo9GdtIwH5uQPpCmAScheh3+ho9H9+UfIh8eRwD0Qhavhb1OhDWJQYwvZkkf69p7YOpXIYptjXR+l2fGAhcVwyljde+TLD+GyRamX+AegJhEQZjnXBA2y3gEQMs7Du2E0GKCX62gNfIOjCBwcRHt3x549bCj6AFwz4EMeA+gXCU79M3nUDI4rOps8BbC5UtNdf6rYYYijvH4voNWjZrJWuqzpp1qLREIPDbWhjSvsDtePbWJ7kljHnaik7h1wGscCfP+gDrQaTDysLeGjeWhAjLk77zAeECCK8ZFLxn8PYbBFzondu7jcnacmWdOul8ifrVTTADiIhtXa1sD7TcBie2VfVMPH97ywRITVsL9YEL2WpZBHu+RDi4md5ZGI3QuSGrTO3TQjFMzYdffvW26s0/Nza+7Ul61ncMwfU8r1OVvDQEjap7z3jjjCPWNLOMJ8ql/dI9jOMsiwbq5zrima1dN6y+qr6fk7YQeDhGxtjWaazPK3+P8n6L2PUm2i4bv+QWa9n33csPNW4trFMM2B9uSn7sMfKbOSGQ6hMDgevKwmnj9fkMrG9CNjMOpz49DLuZcUV27/8cTv1jxXX2gEkMMAOaOMW2B6LRyvMzCxwjbEBs31PPHPAeOvQUhqNBjPf2FwO8wXJCHdHK/00Q+NAhHOgWtUOHtMNY/AYtFXvNTDOMfngPHSrzLyetA3kPgRmFcfU9AhohfYD1INOZDXLwCCKmEE0zIcbBEuPWJgYY2gEnlsPPbdehMH51E9QAWnjaChqxvL6M48aTfsB6YFRqT8EPHdLeAWlEWVR/voP1lRXGdA1t3jrgSP4RkzhwKG/tEnBmDPzyq7dVb/6riAHDeKHiqqNG1D1DS4f16fi0+5Al1p5zqX4eBwWWOiNRtZ/riPZmRinTMuvPQgCw/sP68xJfqrRnDERa8az7AKatHzpUpxhgsIOJtEOrYin1OXWKAUaguuLINt5QB0LiQ4Yr8HrWH98WbgefBmB95QSbCQmGv5EjCBeaI/Dbxk9cvbcENaJNDIkBIhiPG20YG4zqGWu6Xgfz7BS9GmZCqvMCRtiUtDzFjiB8sA7NCXCuwZvCEtbKHvT3HhIDVSExQFSkuFJW1rK27xdtcXkR+qL4JWOa7q+J17BQUNZVlaNltwLbOsjWKFmajRQsxIfIJmSv9UNvxSWnNwPv7+I4X/1chfcZEgNVITFAVKTwL+7NG/76dkXv4w+HP/iRssyvIlWufNpaDWSTbNoVy/Hvucp/zkIQ7xC+ZRKFwIVRuFXlz4zeK0gMVIXEAEEQBEE0OSQGCIIgCKLJITFAEARBEE0OiQGCIAiCaHJIDBAEQRBEk0NigCAIgiCaHBIDBEEQBNHkkBggCIIgiCaHxABBEARBNDkkBgiCIAiiySExQBAEQRBNDokBgiAIgmhySAwQBEEQRFMD8P+LoRnEeJJMqwAAAABJRU5ErkJggg=='),
        ]);

        $template->parameters = [
            CMailChildrensCampRegistration::class              => CMailChildrensCampRegistration::getModelForSerialize(),
            CMailChildrensCampRegistrationInvoice::class       => CMailChildrensCampRegistrationInvoice::getModelForSerialize(),
            CMailChildrensCampExtraordinaryEvent::class        => CMailChildrensCampExtraordinaryEvent::getModelForSerialize(),
            CMailChildrensCampSatisfactionQuestionnaire::class => CMailChildrensCampSatisfactionQuestionnaire::getModelForSerialize(),
        ];

        $template->childrensCamp = $this->childrensCamp;
        $template->supplements   = $this->facadeChildrensCampSupplement->getAll();
        $template->render();
    }

    protected function createComponentForm(): Form
    {
        // DATA
        $dataTransports = $this->facadeTransport->getPairs('id', 'name');
        $dataPayments   = $this->facadePayment->getPairs('id', 'name');

        $dataSupplements         = [];
        $dataSupplementsInactive = [];
        foreach ($this->facadeChildrensCampSupplement->getAll() as $supplement) {
            if ($supplement->isActive()) {
                $dataSupplements[$supplement->getId()] = $supplement->getName();
            } else {
                $dataSupplementsInactive[$supplement->getId()] = sprintf('[X] %s', $supplement->getName());
            }
        }
        $dataSupplements += $dataSupplementsInactive;

        // FORM
        $form = new Form();
        $form->setTranslator($this->translator);

        // INPUT
        $form->addText('name', 'form.childrens-camp.edit.name')
            ->setRequired('form.childrens-camp.edit.name.req');
        $form->addText('term', 'form.childrens-camp.edit.term')
            ->setRequired('form.childrens-camp.edit.term.req')
            ->setHtmlAttribute('data-daterange');
        $form->addText('address', 'form.childrens-camp.edit.address');

        $form->addInteger('capacity', 'form.childrens-camp.edit.capacity')
            ->setHtmlAttribute('min', 0);

        $form->addTextArea('description', 'form.childrens-camp.edit.description');
        $form->addTextArea('content', 'form.childrens-camp.edit.content');
        $form->addTextArea('textPrice', 'form.childrens-camp.edit.text-price');
        $form->addTextArea('textCampAreal', 'form.childrens-camp.edit.text-camp-areal');
        $form->addTextArea('textTransport', 'form.childrens-camp.edit.text-transport');
        $form->addTextArea('textMenu', 'form.childrens-camp.edit.text-menu');
        $form->addTextArea('textQuestBook', 'form.childrens-camp.edit.text-quest-book');

        $form->addImageWithRFM('imagePreview', 'form.childrens-camp.edit.image-preview');

        $form->addCheckbox('isActive', 'form.childrens-camp.edit.is-active')
            ->setDefaultValue(true);

        // EXTEND
        if ($this->childrensCamp->isLoaded()) {
            $form->addCheckbox('changeWebalize', Html::el('sup', [
                'title'          => $this->translator->translate('form.childrens-camp.edit.change-webalize'),
                'class'          => 'far fa-fw fa-question-circle',
                'data-toggle'    => 'tooltip',
                'data-placement' => 'left',
            ]))->setTranslator(null);
        }

        // REGISTRATION
        $formRegistration = $form->addContainer('registration');
        $formRegistration->addTextArea('textRegistration', 'form.childrens-camp.edit.text-registration');

        $formRegistration->addMultiSelect('transports', 'form.childrens-camp.edit.transports', $dataTransports)
            ->setTranslator(null);
        $formRegistration->addMultiSelect('payments', 'form.childrens-camp.edit.payments', $dataPayments)
            ->setTranslator(null);

        $formRegistration->addText('tshirtColorList', 'form.childrens-camp.edit.tshirt-color-list');
        $formRegistration->addText('tshirtSizeList', 'form.childrens-camp.edit.tshirt-size-list');
        $formRegistration->addText('tshirtDeathLine', 'form.childrens-camp.edit.tshirt-death-line')
            ->setHtmlAttribute('data-date')
            ->setHtmlAttribute('data-date-clear');

        $formRegistration->addText('variableSymbolPrefix', 'form.childrens-camp.edit.variable-symbol-prefix');
        $formRegistration->addCheckbox('isOpenRegistration', 'form.childrens-camp.edit.is-open-registration');
        $formRegistration->addText('registrationExternalLink', 'form.childrens-camp.edit.registration-external-link')
            ->addCondition(Form::BLANK)
            ->toggle('toggle-registration');

        // MAIL TEMPLATE
        $formMailTemplates = $form->addContainer('mailTemplates');
        foreach (ChildrensCampRegistration::STATES as $stateKey => $stateName) {
            $formMailTemplate = $formMailTemplates->addContainer($stateKey);

            $mailTemplatePrefix = sprintf('form.childrens-camp.edit.%s', $stateName);
            $formMailTemplate->addHidden('name', $mailTemplatePrefix);
            $formMailTemplate->addHidden('class', CMailChildrensCampRegistration::class);
            $inputMailTemplateSubject = $formMailTemplate->addText('subject', sprintf('%s.subject', $mailTemplatePrefix));
            $formMailTemplate->addText('recipients', sprintf('%s.recipients', $mailTemplatePrefix));
            $formMailTemplate->addTextArea('preheader', sprintf('%s.preheader', $mailTemplatePrefix));
            $inputMailTemplateContent = $formMailTemplate->addTextArea('content', sprintf('%s.content', $mailTemplatePrefix));
            $formMailTemplate->addMultiUpload('attachments', sprintf('%s.attachments', $mailTemplatePrefix));

            $inputMailTemplateSubject->addConditionOn($inputMailTemplateContent, Form::FILLED, true)
                ->setRequired(sprintf('%s.subject.req', $mailTemplatePrefix));
            $inputMailTemplateContent->addConditionOn($inputMailTemplateSubject, Form::FILLED, true)
                ->setRequired(sprintf('%s.content.req', $mailTemplatePrefix));
        }

        $mtLists = [
            CMailChildrensCampRegistrationInvoice::IDENTIFIER       => CMailChildrensCampRegistrationInvoice::class,
            CMailChildrensCampExtraordinaryEvent::IDENTIFIER        => CMailChildrensCampExtraordinaryEvent::class,
            CMailChildrensCampSatisfactionQuestionnaire::IDENTIFIER => CMailChildrensCampSatisfactionQuestionnaire::class,
        ];
        foreach ($mtLists as $mtIdentifier => $mtClass) {
            $formMailTemplate   = $formMailTemplates->addContainer($mtIdentifier);
            $mailTemplatePrefix = sprintf('form.childrens-camp.edit.mail-%s', Strings::camelizeToWebalize($mtIdentifier));
            $formMailTemplate->addHidden('name', $mailTemplatePrefix);
            $formMailTemplate->addHidden('class', $mtClass);
            $formMailTemplate->addText('subject', sprintf('%s.subject', $mailTemplatePrefix));
            $formMailTemplate->addText('recipients', sprintf('%s.recipients', $mailTemplatePrefix));
            $formMailTemplate->addTextArea('preheader', sprintf('%s.preheader', $mailTemplatePrefix));
            $formMailTemplate->addTextArea('content', sprintf('%s.content', $mailTemplatePrefix));
            $formMailTemplate->addMultiUpload('attachments', sprintf('%s.attachments', $mailTemplatePrefix));
        }

        // PRICE
        $formPrice = $form->addContainer('price');
        $formPrice->addInteger('price', 'form.childrens-camp.edit.price')
            ->setHtmlAttribute('min', 0);
        $formPrice->addInteger('tshirtPrice', 'form.childrens-camp.edit.tshirt-price')
            ->setHtmlAttribute('min', 0);
        $inputPriceSupplements = $formPrice->addMultiSelect('supplements', 'form.childrens-camp.edit.supplements', $dataSupplements)
            ->setTranslator(null);

        foreach (array_keys($dataSupplements) as $supplementId) {
            $inputPriceSupplements->addCondition(FormRuleValidator::IS_SELECTED, $supplementId)
                ->toggle(sprintf('toggle-supplement-%d', $supplementId));
        }

        // BUTTON
        $form->addSubmit('send', 'form.childrens-camp.edit.send');
        $form->addSubmit('sendBack', 'form.childrens-camp.edit.send-back');
        $form->addSubmit('back', 'form.childrens-camp.edit.back')
            ->setValidationScope([])
            ->onClick[] = [$this, 'processOnBack'];

        // DEFAULT
        $form->setDefaults($this->getDefaults());

        // CALLBACK
        $form->onSuccess[] = [$this, 'processOnSuccess'];

        return $form;
    }

    /**
     * @return mixed[]
     */
    private function getDefaults(): array
    {
        if (! $this->childrensCamp->isLoaded()) {
            return [];
        }

        $mailTemplates = [];
        foreach ($this->childrensCamp->getMailTemplates() as $mailTemplate) {
            $mailTemplates[$mailTemplate->getIdentifier()] = [
                'subject'    => $mailTemplate->getSubject(),
                'recipients' => implode(';', $mailTemplate->getRecipients()),
                'preheader'  => $mailTemplate->getPreheader(),
                'content'    => $mailTemplate->getContent(),
            ];
        }

        return [
            'name'          => $this->childrensCamp->getName(),
            'description'   => $this->childrensCamp->getDescription(),
            'content'       => $this->childrensCamp->getContent(),
            'textPrice'     => $this->childrensCamp->getTextPrice(),
            'textCampAreal' => $this->childrensCamp->getTextCampAreal(),
            'textTransport' => $this->childrensCamp->getTextTransport(),
            'textMenu'      => $this->childrensCamp->getTextMenu(),
            'textQuestBook' => $this->childrensCamp->getTextQuestBook(),
            'term'          => $this->childrensCamp->getTermFromTo(),
            'address'       => $this->childrensCamp->getAddress(),
            'capacity'      => $this->childrensCamp->getCapacity(),
            'isActive'      => $this->childrensCamp->isActive(),
            'price'         => [
                'price'       => $this->childrensCamp->getPrice(),
                'tshirtPrice' => $this->childrensCamp->getTshirtPrice(),
                'supplements' => $this->childrensCamp->getSupplements()->map(fn(ChildrensCampSupplement $s): ?int => $s->getId())->toArray(),
            ],
            'registration'  => [
                'textRegistration'         => $this->childrensCamp->getTextRegistration(),
                'transports'               => $this->childrensCamp->getTransports()->map(fn(Transport $t): int => $t->getId())->toArray(),
                'payments'                 => $this->childrensCamp->getPayments()->map(fn(Payment $p): int => $p->getId())->toArray(),
                'tshirtColorList'          => implode(';', $this->childrensCamp->getTshirtColorList()),
                'tshirtSizeList'           => implode(';', $this->childrensCamp->getTshirtSizeList()),
                'tshirtDeathLine'          => $this->childrensCamp->getTshirtDeathLine() instanceof DateTimeInterface ? $this->childrensCamp->getTshirtDeathLine()->format('d.m.Y') : '',
                'variableSymbolPrefix'     => $this->childrensCamp->getVariableSymbolPrefix(),
                'isOpenRegistration'       => $this->childrensCamp->isOpenRegistration(),
                'registrationExternalLink' => $this->childrensCamp->getRegistrationExternalLink(),
            ],
            'mailTemplates' => $mailTemplates,
        ];
    }

    public function processOnSuccess(Form $form, ArrayHash $values): void
    {
        /**
         * @var DateTime $termFrom
         * @var DateTime $termTo
         */
        [$termFrom, $termTo] = Arrays::map(explode(' - ', $values->term), static function (string $date): DateTime {
            $date = DateTime::createFromFormat('d.m.Y', $date);
            return is_bool($date) ? new DateTime() : $date;
        });

        // IDENTIFIER
        $identifier = UtilsFormControl::getImagePreview($values->imagePreview, BaseControl::DIR_IMAGE);

        if ($this->childrensCamp->isLoaded()) {
            $childrensCamp = $this->facade->update(
                $this->childrensCamp->getId(),
                $values->name,
                $values->description,
                $values->content,
                $values->textPrice,
                $values->textCampAreal,
                $values->textTransport,
                $values->textMenu,
                $values->textQuestBook,
                $termFrom,
                $termTo,
                $values->address,
                $values->capacity,
                $values->isActive,
                $identifier,
                $values->changeWebalize
            );
            $this->onFlashmessage('form.childrens-camp.edit.flash.success.update', Flash::SUCCESS);
        } else {
            $childrensCamp = $this->facade->create(
                $values->name,
                $values->description,
                $values->content,
                $values->textPrice,
                $values->textCampAreal,
                $values->textTransport,
                $values->textMenu,
                $values->textQuestBook,
                $termFrom,
                $termTo,
                $values->address,
                $values->capacity,
                $values->isActive,
                $identifier
            );
            $this->onFlashmessage('form.childrens-camp.edit.flash.success.create', Flash::SUCCESS);
        }

        // REGISTRATION
        $transports = [];
        foreach ($values->registration->transports as $tid) {
            $transport = $this->facadeTransport->get($tid);
            if ($transport->isLoaded()) {
                $transports[] = $transport;
            }
        }

        $payments = [];
        foreach ($values->registration->payments as $pid) {
            $payment = $this->facadePayment->get($pid);
            if ($payment->isLoaded()) {
                $payments[] = $payment;
            }
        }

        $tshirtDeathLine = DateTime::createFromFormat('d.m.Y', $values->registration->tshirtDeathLine);
        if (is_bool($tshirtDeathLine)) {
            $tshirtDeathLine = $termFrom;
        }

        $childrensCamp = $this->facade->updateRegistration(
            $childrensCamp,
            $values->registration->textRegistration,
            $transports,
            $payments,
            explode(';', $values->registration->tshirtColorList),
            explode(';', $values->registration->tshirtSizeList),
            $tshirtDeathLine,
            $values->registration->variableSymbolPrefix,
            $values->registration->isOpenRegistration,
            $values->registration->registrationExternalLink
        );

        // PRICE AND SUPPLEMENTS
        $supplements = [];
        foreach ($values->price->supplements as $supplement) {
            $supplements[] = $this->facadeChildrensCampSupplement->get($supplement);
        }

        $childrensCamp = $this->facade->updatePriceAndSupplement(
            $childrensCamp,
            $values->price->price,
            $values->price->tshirtPrice,
            $supplements
        );

        // MAIL TEMPLATE
        $mailTemplates = [];
        foreach ($values->mailTemplates as $stateId => $mailTemplate) {
            if (trim($mailTemplate->subject) !== '' && trim($mailTemplate->content) !== '') {
                $attachments = Arrays::map($mailTemplate->attachments, function (FileUpload $fileUpload): File {
                    $fileIdentifier = $this->fileStorage->save($fileUpload, BaseControl::DIR_FILE_ATTACHMENT);
                    return $this->facadeFile->create($fileUpload->getUntrustedName(), $fileIdentifier, $fileUpload->getSize(), $fileUpload->getContentType() ?? '');
                });

                $mailTemplates[] = new ChildrensCampMailTemplateFake(
                    $mailTemplate->class,
                    $stateId,
                    $mailTemplate->subject,
                    $mailTemplate->preheader,
                    $mailTemplate->content,
                    array_filter(explode(';', $mailTemplate->recipients)),
                    $attachments
                );
            }
        }

        $this->facadeChildrensCampMailTemplate->addMailTemplates($childrensCamp, $mailTemplates, $this->user->getFullName());

        if ($form->isSubmitted()->name === 'sendBack') {
            $this->processOnBack();
        }

        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'edit',
            'id'      => $childrensCamp->getId(),
        ]);
    }

    public function processOnBack(): void
    {
        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'overview',
        ]);
    }

    public function handleForceDownloadFile(string $hash): void
    {
        $file = $this->facadeFile->getByHash($hash);

        if ($file !== null) {
            /* @phpstan-ignore-next-line */
            $this->fileStorage->download($file->getIdentifier(), $file->getName(), $file->getMimeType(), $file->getSize());
        } else {
            $this->getParent()->redirect(':Admin:Homepage:fileNotFound', ['hash' => $hash]);
        }
    }
}
