<?php

declare(strict_types=1);

namespace Skadmin\ChildrensCamp\Components\Admin;

use SkadminUtils\GridControls\UI\GridControl;
use SkadminUtils\GridControls\UI\GridDoctrine;
use Skadmin\Role\Doctrine\Role\Privilege;
use App\Model\Grid\Traits\IsActive;
use App\Model\System\APackageControl;
use App\Model\System\Callback;
use App\Model\System\Constant;
use App\Model\System\Flash;
use App\Model\System\Utils;
use SkadminUtils\ImageStorage\ImageStorage;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\QueryBuilder;
use Nette\ComponentModel\IContainer;
use Nette\Security\User;
use Nette\Utils\Arrays;
use Nette\Utils\Html;
use Nette\Utils\Random;
use Nette\Utils\Strings;
use Skadmin\ChildrensCamp\BaseControl;
use Skadmin\ChildrensCamp\Doctrine\ChildrensCamp\ChildrensCamp;
use Skadmin\ChildrensCamp\Doctrine\ChildrensCamp\ChildrensCampFacade;
use Skadmin\ChildrensCamp\Doctrine\ChildrensCamp\ChildrensCampRegistrationSupplement\ChildrensCampRegistrationSupplement;
use Skadmin\ChildrensCamp\Doctrine\ChildrensCampRegistration\ChildrensCampRegistration;
use Skadmin\ChildrensCamp\Doctrine\ChildrensCampRegistration\ChildrensCampRegistrationFacade;
use Skadmin\ChildrensCamp\Service\ChildrensCampRegistrationService;
use Skadmin\Mailing\Doctrine\Mail\MailQueue;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use Tracy\Debugger;
use Ublaboo\DataGrid\Column\ColumnText;

class OverviewRegistration extends GridControl
{
    use APackageControl;

    private ChildrensCampRegistrationFacade  $facade;
    private ChildrensCampFacade              $facadeChildrensCamp;
    private ChildrensCamp                    $childrensCamp;
    private ChildrensCampRegistrationService $serviceChildrensCampRegistration;

    public function __construct(int $childrensCampId, ChildrensCampRegistrationFacade $facade, ChildrensCampFacade $facadeChildrensCamp, Translator $translator, User $user, ChildrensCampRegistrationService $serviceChildrensCampRegistration)
    {
        parent::__construct($translator, $user);

        $this->facade                           = $facade;
        $this->facadeChildrensCamp              = $facadeChildrensCamp;
        $this->serviceChildrensCampRegistration = $serviceChildrensCampRegistration;

        $this->childrensCamp = $this->facadeChildrensCamp->get($childrensCampId);
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null)
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::READ)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/overviewRegistration.latte');
        $template->render();
    }

    public function getTitle(): string
    {
        return 'childrens-camp.overview-registration.title';
    }

    protected function createComponentGrid(string $name): GridDoctrine
    {
        // DATA
        $dataStatusExtra = ['valid' => 'grid.childrens-camp.overview-registration.status.valid'];
        $dataStatus      = ChildrensCampRegistration::STATES;
        $dataStatusBtn   = [
            ChildrensCampRegistration::STATE_NEW                            => 'btn-light',
            ChildrensCampRegistration::STATE_PAID                           => 'btn-primary',
            ChildrensCampRegistration::STATE_INFORMATION_BEFORE_CAMP        => 'btn-primary',
            ChildrensCampRegistration::STATE_TRANSPORT_TO_CAMP              => 'btn-primary',
            ChildrensCampRegistration::STATE_DOCUMENTS_FOR_CAMP             => 'btn-primary',
            ChildrensCampRegistration::STATE_LATEST_INFORMATION_BEFORE_CAMP => 'btn-primary',
            ChildrensCampRegistration::STATE_CANCELED                       => 'btn-danger',
        ];
        $dataStatusIcon  = [
            ChildrensCampRegistration::STATE_NEW                            => 'circle',
            ChildrensCampRegistration::STATE_PAID                           => 'dollar-sign',
            ChildrensCampRegistration::STATE_INFORMATION_BEFORE_CAMP        => 'info',
            ChildrensCampRegistration::STATE_TRANSPORT_TO_CAMP              => 'bus',
            ChildrensCampRegistration::STATE_DOCUMENTS_FOR_CAMP             => 'file',
            ChildrensCampRegistration::STATE_LATEST_INFORMATION_BEFORE_CAMP => 'exclamation',
            ChildrensCampRegistration::STATE_CANCELED                       => 'times',
        ];

        $dataPayments = [];
        foreach ($this->childrensCamp->getPayments() as $payment) {
            $dataPayments[$payment->getId()] = $payment->getName();
        }

        $dataTags = [
            'tshirt'            => $this->translator->translate('grid.childrens-camp.overview-registration.tags.tshirt'),
            'invoice'           => $this->translator->translate('grid.childrens-camp.overview-registration.tags.invoice'),
            'insurance-company' => $this->translator->translate('grid.childrens-camp.overview-registration.tags.insurance-company'),
        ];

        // GRID
        $grid = new GridDoctrine($this->getPresenter());

        // DEFAULT
        $grid->setPrimaryKey('id');
        $grid->setDataSource($this->facade->getModelForChildrensCamp($this->childrensCamp));

        // COLUMNS
        $grid->addColumnText('name', 'grid.childrens-camp.overview-registration.name')
            ->setRenderer(function (ChildrensCampRegistration $registration): Html {
                $name = Html::el('span', ['class' => 'text-primary font-weight-bold text-nowrap'])->setText($registration->getFullName());

                $pid = Html::el('span', ['class' => 'text-muted small'])
                    ->setText($registration->getPersonalIdentificationNumber());

                $birthdate = Html::el('span', [
                    'class'          => 'text-muted small',
                    'title'          => $this->translator->translate('grid.childrens-camp.overview-registration.name.hint-age'),
                    'data-toggle'    => 'tooltip',
                    'data-placement' => 'right',
                ])->setHtml(sprintf(
                    '%s <span class="font-weight-bold">(%s)</span>',
                    $registration->getBirthdate()->format('d.m.Y'),
                    $registration->getChildAge()
                ));

                $variableSymbol = Html::el('span', ['class' => 'text-muted small'])
                    ->setText($registration->getVariableSymbol());

                $render = new Html();
                $render->addHtml($name)
                    ->addHtml('<br>')
                    ->addHtml($pid)
                    ->addHtml('<br>')
                    ->addHtml($birthdate)
                    ->addHtml('<br>')
                    ->addHtml($variableSymbol);

                return $render;
            });
        $griColumnStatus = $grid->addColumnStatus('status', 'grid.childrens-camp.overview-registration.status')
            ->setAlign('center');
        foreach ($dataStatus as $statusId => $status) {
            $griColumnStatus
                ->addOption($statusId, $status)
                ->setIcon($dataStatusIcon[$statusId])
                ->setClass($dataStatusBtn[$statusId])
                ->endOption();
        }
        $griColumnStatus->onChange[] = [$this, 'overviewRegistrationOnChangeStatus'];
        $grid->addColumnText('contact', 'grid.childrens-camp.overview-registration.contact')
            ->setRenderer(static function (ChildrensCampRegistration $registration): Html {
                $contacts = new Html();

                $contact = Html::el('div');
                $contact->addText('E-mail')
                    ->addText(' ')
                    ->addHtml(Utils::createHtmlContact($registration->getContactEmail()));
                $contacts->addHtml($contact);

                if ($registration->getMotherName() !== '') {
                    $contact = Html::el('div');
                    $contact->addText($registration->getMotherName())
                        ->addText(' ')
                        ->addHtml(Utils::createHtmlContact($registration->getMotherPhone()));
                    $contacts->addHtml($contact);
                }

                if ($registration->getFatherName() !== '') {
                    $contact = Html::el('div');
                    $contact->addText($registration->getFatherName())
                        ->addText(' ')
                        ->addHtml(Utils::createHtmlContact($registration->getFatherPhone()));
                    $contacts->addHtml($contact);
                }

                if ($registration->getLegalRepresentativeName() !== '') {
                    $contact = Html::el('div');
                    $contact->addText($registration->getLegalRepresentativeName())
                        ->addText(' ')
                        ->addHtml(Utils::createHtmlContact($registration->getLegalRepresentativePhone()));
                    $contacts->addHtml($contact);
                }

                return $contacts;
            });
        $grid->addColumnText('payment', 'grid.childrens-camp.overview-registration.payment')
            ->setRenderer(fn(ChildrensCampRegistration $r) => $r->getPayment()->getName());
        $grid->addColumnText('totalPrice', 'grid.childrens-camp.overview-registration.total-price')
            ->setRenderer(function (ChildrensCampRegistration $registration): Html {
                $price = Html::el('span', ['class' => 'text-nowrap'])->setText(sprintf('%s Kč', number_format($registration->getTotalPrice(), 2, ',', ' ')));

                $render = new Html();
                $render->addHtml($price);

                if ($registration->getAlreadyPaid() === $registration->getTotalPrice()) {
                    $currentPaidClass = 'font-weight-bold text-success';
                } elseif ($registration->getAlreadyPaid() > $registration->getTotalPrice()) {
                    $currentPaidClass = 'font-weight-bold text-warning';
                } elseif ($registration->getAlreadyPaid() > 0) {
                    $currentPaidClass = 'font-weight-bold text-danger';
                }

                if (isset($currentPaidClass)) {
                    $currentPaid = Html::el('span', [
                        'class'          => $currentPaidClass . ' small text-nowrap',
                        'title'          => $this->translator->translate('grid.childrens-camp.overview-registration.total-price.current-paid.title'),
                        'data-toggle'    => 'tooltip',
                        'data-placement' => 'left',
                    ])->setText($this->translator->translate(new SimpleTranslation(
                        'grid.childrens-camp.overview-registration.total-price.current-paid %s',
                        [number_format($registration->getAlreadyPaid(), 2, ',', ' ')]
                    )));

                    $render->addHtml('<br>')
                        ->addHtml($currentPaid);
                }

                return $render;
            })
            ->setAlign('right');
        $grid->addColumnText('tags', 'grid.childrens-camp.overview-registration.tags')
            ->setRenderer(function (ChildrensCampRegistration $registration) use ($dataTags): Html {
                $render = new Html();

                if ($registration->getTshirtColor() !== '') {
                    $render->addHtml(sprintf(
                        '<i class="fas fa-fw fa-tshirt text-warning ml-1" title="" data-toggle="tooltip" data-placement="left" data-original-title="%s"></i>',
                        $dataTags['tshirt']
                    ));
                }

                if ($registration->requireInvoice()) {
                    if ($registration->isInvoiceSended()) {
                        $color = 'text-success';
                        $title = sprintf('%s %s',
                            $dataTags['invoice'],
                            $this->translator->translate(
                                new SimpleTranslation('grid.childrens-camp.overview-registration.tags.invoice.sent-at %s', [$registration->getInvoiceSentAt()->format('d.m.Y H:i')])
                            )
                        );
                    } else {
                        $color = 'text-primary';
                        $title = $dataTags['invoice'];
                    }
                    $render->addHtml(sprintf(
                        '<i class="fas fa-fw fa-file-invoice-dollar %s ml-1" title="" data-toggle="tooltip" data-placement="left" data-original-title="%s"></i>',
                        $color,
                        $title
                    ));
                }

                if ($registration->isConfirmationForTheInsuranceCompany()) {
                    $render->addHtml(sprintf(
                        '<i class="fas fa-fw fa-house-damage text-danger ml-1" title="" data-toggle="tooltip" data-placement="left" data-original-title="%s"></i>',
                        $dataTags['insurance-company']
                    ));
                }

                return $render;
            })
            ->setAlign('right');

        // FILTER
        $grid->addFilterText('name', 'grid.childrens-camp.overview-registration.name', ['childLastName', 'childFirstName', 'personalIdentificationNumber', 'variableSymbol']);
        $grid->addFilterSelect('status', 'grid.childrens-camp.overview-registration.status', Constant::PROMTP_ARR + $dataStatusExtra + $dataStatus)
            ->setTranslateOptions()
            ->setCondition(function (QueryBuilder $qb, $value) {
                $criteria = Criteria::create();

                switch ($value) {
                    case 'valid':
                        $criteria->andWhere(Criteria::expr()->notIn('status', [ChildrensCampRegistration::STATE_CANCELED]));
                        break;
                    default:
                        $criteria->andWhere(Criteria::expr()->eq('status', $value));
                        break;
                }

                $qb->addCriteria($criteria);
            });
        $grid->addFilterText('contact', 'grid.childrens-camp.overview-registration.contact',
            ['contactEmail', 'motherName', 'motherPhone', 'fatherName', 'fatherPhone', 'legalRepresentativeName', 'legalRepresentativePhone']);
        $grid->addFilterSelect('payment', 'grid.childrens-camp.overview-registration.payment', Constant::PROMTP_ARR + $dataPayments);
        $grid->addFilterSelect('tags', 'grid.childrens-camp.overview-registration.tags', Constant::PROMTP_ARR + $dataTags)
            ->setCondition(function (QueryBuilder $qb, $value) {
                $criteria = Criteria::create();

                switch ($value) {
                    case 'invoice':
                        $criteria->andWhere(Criteria::expr()->neq('invoiceIN', ''));
                        break;
                    case 'insurance-company':
                        $criteria->andWhere(Criteria::expr()->eq('confirmationForTheInsuranceCompany', true));
                        break;
                    case 'tshirt':
                        $criteria->andWhere(Criteria::expr()->neq('tshirtColor', ''));
                        break;
                }

                $qb->addCriteria($criteria);
            });

        $grid->addToolbarButton('Component:default', 'grid.childrens-camp.overview-registration.overview', [
            'package' => new BaseControl(),
            'render'  => 'overview',
        ])->setIcon('list')
            ->setClass('btn btn-xs btn-outline-primary');

        $grid->addToolbarButton('Component:default#1', 'grid.childrens-camp.overview-registration.edit', [
            'package' => new BaseControl(),
            'render'  => 'edit',
            'id'      => $this->childrensCamp->getId(),
        ])->setIcon('campground')
            ->setClass('btn btn-xs btn-primary');

        if ($this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            $grid->addActionCallback('sendInvoice', 'grid.childrens-camp.overview-registration.action.send-invoice', [$this, 'gridOverviewRegistrationSendInvoice'])
                ->setClass('btn btn-xs btn-outline-primary ajax d-block mb-2')
                ->setIcon('envelope');

            $grid->addActionCallback('sendExtraordinaryEvent', 'grid.childrens-camp.overview-registration.action.send-extraordinary-event', [$this, 'gridOverviewRegistrationSendExtraordinaryEvent'])
                ->setClass('btn btn-xs btn-outline-primary ajax d-block mb-2')
                ->setIcon('envelope');

            $grid->addActionCallback('sendSatisfactionQuestionnaire', 'grid.childrens-camp.overview-registration.action.send-satisfaction-questionnaire', [$this, 'gridOverviewRegistrationSendSatisfactionQuestionnaire'])
                ->setClass('btn btn-xs btn-outline-primary ajax d-block mb-2')
                ->setIcon('envelope');

            $grid->addActionCallback('pdfInvoice', 'grid.childrens-camp.overview-registration.action.pdf-invoice', [$this, 'gridOverviewRegistrationPdfInvoice'])
                ->setClass('btn btn-xs btn-outline-primary')
                ->setIcon('download');

            $grid->addAction('edit', 'grid.childrens-camp.overview-registration.edit-registration', 'Component:default', ['id' => 'id'])
                ->addParameters([
                    'package' => new BaseControl(),
                    'render'  => 'edit-registration',
                ])->setIcon('pencil-alt')
                ->setClass('btn btn-xs btn-primary');
        }

        $grid->allowRowsAction('sendInvoice', static function (ChildrensCampRegistration $registration): bool {
            return $registration->requireInvoice();
        });

        $grid->allowRowsAction('pdfInvoice', static function (ChildrensCampRegistration $registration): bool {
            return $registration->requireInvoice();
        });

        // DETAIL
        $grid->setItemsDetail(__DIR__ . '/detail.latte');

        $this->prepareExport($grid);

        return $grid;
    }

    public function overviewRegistrationOnChangeStatus(string $id, string $newStatus): void
    {
        $id        = intval($id);
        $newStatus = intval($newStatus);

        $registration = $this->facade->updateStatus($id, $newStatus);

        $presenter = $this->getPresenterIfExists();
        if ($presenter !== null) {
            $newStatusText = $this->translator->translate(ChildrensCampRegistration::STATES[$registration->getStatus()]);
            $message       = new SimpleTranslation('grid.childrens-camp.overview-registration.flash.change-status %s %s', [$registration->getVariableSymbol(), $newStatusText]);
            $presenter->flashMessage($message, Flash::SUCCESS);
        }

        // zkusíme odeslat e-mail
        $mailQueue = $this->serviceChildrensCampRegistration->sendByState($registration);
        if ($presenter !== null && $mailQueue !== null) {
            if ($mailQueue->getStatus() === MailQueue::STATUS_SENT) {
                $message = new SimpleTranslation('grid.childrens-camp.overview-registration.flash.registration-update.success %s', $registration->getContactEmail());
                $presenter->flashMessage($message, Flash::SUCCESS);
            } else {
                $message = new SimpleTranslation('grid.childrens-camp.overview-registration.flash.registration-update.danger %s', $registration->getContactEmail());
                $presenter->flashMessage($message, Flash::DANGER);
            }
        }

        $this['grid']->redrawItem($id);
    }

    public function gridOverviewRegistrationPdfInvoice(string $id): void
    {
        $childrensCampRegistration = $this->facade->get(intval($id));
        $this->serviceChildrensCampRegistration->createPdf(
            $childrensCampRegistration,
            ChildrensCampRegistration::PDF_TYPE_INVOICE,
            true
        );
    }

    public function gridOverviewRegistrationSendInvoice(string $id): void
    {
        $registration = $this->facade->get(intval($id));
        $presenter    = $this->getPresenterIfExists();

        // zkusíme odeslat e-mail
        $mailQueue = $this->serviceChildrensCampRegistration->sendInvoice($registration);
        if ($presenter !== null && $mailQueue !== null) {
            if ($mailQueue->getStatus() === MailQueue::STATUS_SENT) {
                $message = new SimpleTranslation('grid.childrens-camp.overview-registration.flash.send-invoice.success %s', $registration->getContactEmail());
                $presenter->flashMessage($message, Flash::SUCCESS);
                $this->facade->sendInvoice($registration);
            } else {
                $message = new SimpleTranslation('grid.childrens-camp.overview-registration.flash.send-invoice.danger %s', $registration->getContactEmail());
                $presenter->flashMessage($message, Flash::DANGER);
            }
        }

        $this['grid']->redrawItem($id);
    }

    public function gridOverviewRegistrationSendExtraordinaryEvent(string $id): void
    {
        $registration = $this->facade->get(intval($id));
        $presenter    = $this->getPresenterIfExists();

        // zkusíme odeslat e-mail
        $mailQueue = $this->serviceChildrensCampRegistration->sendExtraordinaryEvent($registration);
        if ($presenter !== null && $mailQueue !== null) {
            if ($mailQueue->getStatus() === MailQueue::STATUS_SENT) {
                $message = new SimpleTranslation('grid.childrens-camp.overview-registration.flash.send-extraordinary-event.success %s', $registration->getContactEmail());
                $presenter->flashMessage($message, Flash::SUCCESS);
                $this->facade->sendInvoice($registration);
            } else {
                $message = new SimpleTranslation('grid.childrens-camp.overview-registration.flash.send-extraordinary-event.danger %s', $registration->getContactEmail());
                $presenter->flashMessage($message, Flash::DANGER);
            }
        }

        $this['grid']->redrawItem($id);
    }

    public function gridOverviewRegistrationSendSatisfactionQuestionnaire(string $id): void
    {
        $registration = $this->facade->get(intval($id));
        $presenter    = $this->getPresenterIfExists();

        // zkusíme odeslat e-mail
        $mailQueue = $this->serviceChildrensCampRegistration->sendSatisfactionQuestionnaire($registration);
        if ($presenter !== null && $mailQueue !== null) {
            if ($mailQueue->getStatus() === MailQueue::STATUS_SENT) {
                $message = new SimpleTranslation('grid.childrens-camp.overview-registration.flash.send-satisfaction-questionnaire.success %s', $registration->getContactEmail());
                $presenter->flashMessage($message, Flash::SUCCESS);
                $this->facade->sendInvoice($registration);
            } else {
                $message = new SimpleTranslation('grid.childrens-camp.overview-registration.flash.send-satisfaction-questionnaire.danger %s', $registration->getContactEmail());
                $presenter->flashMessage($message, Flash::DANGER);
            }
        }

        $this['grid']->redrawItem($id);
    }

    private function prepareExport(GridDoctrine &$grid): void
    {
        // EXPORT
        $exportColumns   = [];
        $exportColumns[] = new ColumnText($grid, 'variableSymbol', 'variableSymbol', 'grid.childrens-camp.overview-registration.export.variable-symbol');
        $exportColumns[] = new ColumnText($grid, 'alreadyPaid', 'alreadyPaid', 'grid.childrens-camp.overview-registration.export.already-paid');
        $exportColumns[] = new ColumnText($grid, 'totalPrice', 'totalPrice', 'grid.childrens-camp.overview-registration.export.total-price');
        $exportColumns[] = (new ColumnText($grid, 'createdAt', 'createdAt', 'grid.childrens-camp.overview-registration.export.created-at'))
            ->setRenderer(fn(ChildrensCampRegistration $reg): string => $reg->getCreatedAt()->format('d.m.Y'));
        $exportColumns[] = new ColumnText($grid, 'fullName', 'fullName', 'grid.childrens-camp.overview-registration.export.full-name');
        $exportColumns[] = new ColumnText($grid, 'personalIdentificationNumber', 'personalIdentificationNumber', 'grid.childrens-camp.overview-registration.export.pid');
        $exportColumns[] = (new ColumnText($grid, 'birthdate', 'birthdate', 'grid.childrens-camp.overview-registration.export.birthdate'))
            ->setRenderer(fn(ChildrensCampRegistration $reg): string => $reg->getBirthdate()->format('d.m.Y'));
        $exportColumns[] = new ColumnText($grid, 'childAge', 'childAge', 'grid.childrens-camp.overview-registration.export.child-age');
        $exportColumns[] = new ColumnText($grid, 'street', 'street', 'grid.childrens-camp.overview-registration.export.street');
        $exportColumns[] = new ColumnText($grid, 'city', 'city', 'grid.childrens-camp.overview-registration.export.city');
        $exportColumns[] = new ColumnText($grid, 'zipCode', 'zipCode', 'grid.childrens-camp.overview-registration.export.zip-code');
        $exportColumns[] = new ColumnText($grid, 'contactEmail', 'contactEmail', 'grid.childrens-camp.overview-registration.export.contact-email');
        $exportColumns[] = new ColumnText($grid, 'motherName', 'motherName', 'grid.childrens-camp.overview-registration.export.mother-name');
        $exportColumns[] = new ColumnText($grid, 'motherPhone', 'motherPhone', 'grid.childrens-camp.overview-registration.export.mother-phone');
        $exportColumns[] = new ColumnText($grid, 'fatherName', 'fatherName', 'grid.childrens-camp.overview-registration.export.father-name');
        $exportColumns[] = new ColumnText($grid, 'fatherPhone', 'fatherPhone', 'grid.childrens-camp.overview-registration.export.father-phone');
        $exportColumns[] = new ColumnText($grid, 'legalRepresentativeName', 'legalRepresentativeName', 'grid.childrens-camp.overview-registration.export.legal-representative-name');
        $exportColumns[] = new ColumnText($grid, 'legalRepresentativePhone', 'legalRepresentativePhone', 'grid.childrens-camp.overview-registration.export.legal-representative-phone');
        $exportColumns[] = (new ColumnText($grid, 'transport', 'transport', 'grid.childrens-camp.overview-registration.export.transport'))
            ->setRenderer(fn(ChildrensCampRegistration $reg): string => $reg->getTransport()->getName());
        $exportColumns[] = (new ColumnText($grid, 'payment', 'payment', 'grid.childrens-camp.overview-registration.export.payment'))
            ->setRenderer(fn(ChildrensCampRegistration $reg): string => $reg->getPayment()->getName());
        $exportColumns[] = (new ColumnText($grid, 'swimmingSkills', 'swimmingSkills', 'grid.childrens-camp.overview-registration.export.swimming-skills'))
            ->setRenderer(fn(ChildrensCampRegistration $reg): string => $this->translator->translate(ChildrensCampRegistration::SWIMMINGS_SKILLS[$reg->getSwimmingSkills()]));
        $exportColumns[] = (new ColumnText($grid, 'insuranceCompany', 'insuranceCompany', 'grid.childrens-camp.overview-registration.export.insurance-company'))
            ->setRenderer(fn(ChildrensCampRegistration $reg): string => sprintf('%s - %s', $reg->getInsuranceCompany()->getCode(), $reg->getInsuranceCompany()->getName()));
        $exportColumns[] = (new ColumnText($grid, 'confirmationForTheInsuranceCompany', 'confirmationForTheInsuranceCompany',
            'grid.childrens-camp.overview-registration.export.confirmation-for-the-insurance-company'))
            ->setRenderer(fn(ChildrensCampRegistration $reg): string => $this->translator->translate(Constant::DIAL_YES_NO[intval($reg->isConfirmationForTheInsuranceCompany())]));
        $exportColumns[] = new ColumnText($grid, 'healthCondition', 'healthCondition', 'grid.childrens-camp.overview-registration.export.health-condition');
        $exportColumns[] = new ColumnText($grid, 'washingFirst', 'washingFirst', 'grid.childrens-camp.overview-registration.export.washing-first');
        $exportColumns[] = new ColumnText($grid, 'washingSecond', 'washingSecond', 'grid.childrens-camp.overview-registration.export.washing-second');
        $exportColumns[] = new ColumnText($grid, 'washingThird', 'washingThird', 'grid.childrens-camp.overview-registration.export.washing-third');
        $exportColumns[] = (new ColumnText($grid, 'invoice', 'invoice', 'grid.childrens-camp.overview-registration.export.invoice'))
            ->setRenderer(fn(ChildrensCampRegistration $reg): string => $this->translator->translate(Constant::DIAL_YES_NO[intval($reg->requireInvoice())]));
        $exportColumns[] = new ColumnText($grid, 'invoiceFirm', 'invoiceFirm', 'grid.childrens-camp.overview-registration.export.invoice-firm');
        $exportColumns[] = new ColumnText($grid, 'invoiceIn', 'invoiceIn', 'grid.childrens-camp.overview-registration.export.invoice-in');
        $exportColumns[] = new ColumnText($grid, 'invoiceTin', 'invoiceTin', 'grid.childrens-camp.overview-registration.export.invoice-tin');
        $exportColumns[] = new ColumnText($grid, 'invoiceStreet', 'invoiceStreet', 'grid.childrens-camp.overview-registration.export.invoice-street');
        $exportColumns[] = new ColumnText($grid, 'invoiceCity', 'invoiceCity', 'grid.childrens-camp.overview-registration.export.invoice-city');
        $exportColumns[] = new ColumnText($grid, 'invoiceZipCode', 'invoiceZipCode', 'grid.childrens-camp.overview-registration.export.invoice-zip-code');
        $exportColumns[] = new ColumnText($grid, 'invoiceNote', 'invoiceNote', 'grid.childrens-camp.overview-registration.export.invoice-note');
        $exportColumns[] = (new ColumnText($grid, 'sumplements', 'sumplements', 'grid.childrens-camp.overview-registration.export.sumplements'))
            ->setRenderer(fn(ChildrensCampRegistration $reg): string => implode(', ',
                $reg->getSupplements()->map(fn(ChildrensCampRegistrationSupplement $relSup): string => sprintf('%s (%s Kč)',
                    $relSup->getSupplement()->getName(),
                    number_format($relSup->getPrice(true), 2, ',', ' '))
                )->toArray()
            ));
        $exportColumns[] = (new ColumnText($grid, 'consentToTheCampRules', 'consentToTheCampRules',
            'grid.childrens-camp.overview-registration.export.consent-to-the-camp-rules'))
            ->setRenderer(fn(ChildrensCampRegistration $reg): string => $this->translator->translate(Constant::DIAL_YES_NO[intval($reg->isConsentToTheCampRules())]));
        $exportColumns[] = (new ColumnText($grid, 'consentToPersonalDataProtection', 'consentToPersonalDataProtection',
            'grid.childrens-camp.overview-registration.export.consent-to-personal-data-protection'))
            ->setRenderer(fn(ChildrensCampRegistration $reg): string => $this->translator->translate(Constant::DIAL_YES_NO[intval($reg->isConsentToPersonalDataProtection())]));
        $exportColumns[] = (new ColumnText($grid, 'consentToTheProcessingOfSensitiveData', 'consentToTheProcessingOfSensitiveData',
            'grid.childrens-camp.overview-registration.export.consent-to-the-processing-of-sensitive-data'))
            ->setRenderer(fn(ChildrensCampRegistration $reg): string => $this->translator->translate(Constant::DIAL_YES_NO[intval($reg->isConsentToTheProcessingOfSensitiveData())]));
        $exportColumns[] = (new ColumnText($grid, 'permissionToSeparateChildOnTrip', 'permissionToSeparateChildOnTrip',
            'grid.childrens-camp.overview-registration.export.permission-to-separate-child-on-trip'))
            ->setRenderer(fn(ChildrensCampRegistration $reg): string => $this->translator->translate(Constant::DIAL_YES_NO[intval($reg->isPermissionToSeparateAChildOnTrip())]));
        $exportColumns[] = new ColumnText($grid, 'tshirtColor', 'tshirtColor', 'grid.childrens-camp.overview-registration.export.tshirt-color');
        $exportColumns[] = new ColumnText($grid, 'tshirtSize', 'tshirtSize', 'grid.childrens-camp.overview-registration.export.tshirt-size');
        $exportColumns[] = new ColumnText($grid, 'tshirtCount', 'tshirtCount', 'grid.childrens-camp.overview-registration.export.tshirt-count');
        $exportColumns[] = new ColumnText($grid, 'tshirtNote', 'tshirtNote', 'grid.childrens-camp.overview-registration.export.tshirt-note');
        $exportColumns[] = new ColumnText($grid, 'note', 'note', 'grid.childrens-camp.overview-registration.export.note');
        $exportColumns[] = (new ColumnText($grid, 'camp', 'camp', 'grid.childrens-camp.overview-registration.export.camp'))
            ->setRenderer(fn(ChildrensCampRegistration $reg): string => $reg->getCamp()->getName());

        $exportFile = sprintf('%s-registrations-%s.csv', Strings::webalize($this->childrensCamp->getName()), date('YmdHis'));
        $grid->addExportCsvFiltered('grid.childrens-camp.overview-registration.export.filtered', $exportFile, 'utf-8', ';', true)
            ->setClass('btn btn-xs btn-outline-primary')
            ->setIcon('download')
            ->setColumns($exportColumns);
    }
}
