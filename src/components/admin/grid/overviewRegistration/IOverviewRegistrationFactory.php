<?php

declare(strict_types=1);

namespace Skadmin\ChildrensCamp\Components\Admin;

interface IOverviewRegistrationFactory
{
    public function create(int $childrensCampId) : OverviewRegistration;
}
