<?php

declare(strict_types=1);

namespace Skadmin\ChildrensCamp\Components\Admin;

use SkadminUtils\GridControls\UI\GridControl;
use SkadminUtils\GridControls\UI\GridDoctrine;
use Skadmin\Role\Doctrine\Role\Privilege;
use App\Model\Grid\Traits\IsActive;
use App\Model\System\APackageControl;
use App\Model\System\Callback;
use App\Model\System\Constant;
use App\Model\System\Flash;
use SkadminUtils\ImageStorage\ImageStorage;
use Nette\ComponentModel\IContainer;
use Nette\Security\User;
use Nette\Utils\Arrays;
use Nette\Utils\Html;
use Skadmin\ChildrensCamp\BaseControl;
use Skadmin\ChildrensCamp\Doctrine\ChildrensCamp\ChildrensCamp;
use Skadmin\ChildrensCamp\Doctrine\ChildrensCamp\ChildrensCampFacade;
use Skadmin\Translator\Translator;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;
use function is_array;
use function method_exists;

/**
 * Class Overview
 */
class Overview extends GridControl
{
    use APackageControl;
    use IsActive;

    private ChildrensCampFacade $facade;
    private ImageStorage        $imageStorage;
    private LoaderFactory       $webLoader;

    public function __construct(ChildrensCampFacade $facade, Translator $translator, User $user, ImageStorage $imageStorage, LoaderFactory $webLoader)
    {
        parent::__construct($translator, $user);

        $this->facade       = $facade;
        $this->imageStorage = $imageStorage;
        $this->webLoader    = $webLoader;
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null)
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::READ)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs() : array
    {
        return [$this->webLoader->createJavaScriptLoader('jQueryUi')];
    }

    public function render() : void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/overview.latte');
        $template->render();
    }

    public function getTitle() : string
    {
        return 'childrens-camp.overview.title';
    }

    protected function createComponentGrid(string $name) : GridDoctrine
    {
        $grid = new GridDoctrine($this->getPresenter());

        // DEFAULT
        $grid->setPrimaryKey('id');
        $grid->setDataSource($this->facade->getModel()->orderBy('a.sequence', 'ASC'));

        // COLUMNS
        $grid->addColumnText('imagePreview', '')
            ->setRenderer(function (ChildrensCamp $childrensCamp) : ?Html {
                if ($childrensCamp->getImagePreview() !== null) {
                    $imageSrc = $this->imageStorage->fromIdentifier([$childrensCamp->getImagePreview(), '120x60', 'exact']);

                    return Html::el('img', [
                        'src'   => sprintf('/%s', $imageSrc->createLink()),
                        'style' => 'max-width: none;',
                    ]);
                }

                return null;
            })->setAlign('center');
        $grid->addColumnText('name', 'grid.childrens-camp.overview.name')
            ->setRenderer(function (ChildrensCamp $childrensCamp) : Html {
                if ($this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
                    $link = $this->getPresenter()->link('Component:default', [
                        'package' => new BaseControl(),
                        'render'  => 'edit',
                        'id'      => $childrensCamp->getId(),
                    ]);

                    $name = Html::el('a', [
                        'href'  => $link,
                        'class' => 'font-weight-bold',
                    ])->setText($childrensCamp->getName());
                } else {
                    $name = (new Html())
                        ->setText($childrensCamp->getName());
                }

                $frontLink = Html::el('code', ['class' => 'text-muted small'])->setText($childrensCamp->getWebalize());

                $result = new Html();
                $result->addHtml($name)
                    ->addHtml('<br>')
                    ->addHtml($frontLink);

                return $result;
            });
        $grid->addColumnText('termClever', 'grid.childrens-camp.overview.term');
        $grid->addcolumntext('capacity', 'grid.childrens-camp.overview.capacity')
            ->setAlign('center')
            ->setRenderer(function (ChildrensCamp $childrensCamp) : Html {
                return Html::el('span')
                    ->setText(sprintf('%d / %d', $childrensCamp->getValidRegistration()->count(), $childrensCamp->getCapacity()));
            });
        $grid->addcolumntext('address', 'grid.childrens-camp.overview.address')
            ->setRenderer(function (ChildrensCamp $childrensCamp) : Html {
                $result = new Html();
                $result->addHtml((new Html())->setText($childrensCamp->getAddress()));

                if (trim($childrensCamp->getAddress()) !== '') {
                    $mapyCzLink = Html::el('a', [
                        'class'  => 'text-muted small',
                        'target' => '_blank',
                        'href'   => $childrensCamp->getAddressLinkToMap(),
                    ])->addHtml('<i class="fas fa-fw fa-map-marked-alt mr-1"></i>')
                        ->addText($this->translator->translate('grid.childrens-camp.overview.address.show-on-map'));

                    $result->addHtml('<br>')
                        ->addHtml($mapyCzLink);
                }

                return $result;
            });
        $this->addColumnIsActive($grid, 'childrens-camp.overview');

        // STYLE
        $grid->getColumn('imagePreview')
            ->getElementPrototype('th')
            ->setAttribute('style', 'width: 1px');

        // FILTER
        $grid->addFilterText('name', 'grid.childrens-camp.overview.name');
        $this->addFilterIsActive($grid, 'childrens-camp.overview');

        // ACTION
        if ($this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {

            $grid->addAction('overview-registration', 'grid.childrens-camp.overview.action.overview-registration', 'Component:default', ['id' => 'id'])
                ->addParameters([
                    'package' => new BaseControl(),
                    'render'  => 'overview-registration',
                ])->setIcon('users')
                ->setClass('btn btn-xs btn-default btn-outline-primary');

            $grid->addAction('edit', 'grid.childrens-camp.overview.action.edit', 'Component:default', ['id' => 'id'])
                ->addParameters([
                    'package' => new BaseControl(),
                    'render'  => 'edit',
                ])->setIcon('pencil-alt')
                ->setClass('btn btn-xs btn-primary');
        }

        // TOOLBAR
        if ($this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            $grid->addToolbarButton('Component:default', 'grid.childrens-camp.overview.action.new', [
                'package' => new BaseControl(),
                'render'  => 'edit',
            ])->setIcon('plus')
                ->setClass('btn btn-xs btn-primary');
        }

        // SORTING
        $grid->setSortable();
        $grid->setSortableHandler($this->link('sort!'));

        return $grid;
    }

    public function handleSort(?string $item_id, ?string $prev_id, ?string $next_id) : void
    {
        $this->facade->sort($item_id, $prev_id, $next_id);
        $this->onFlashmessage('grid.childrens-camp.overview.action.flash.sort.success', Flash::SUCCESS);
        $this['grid']->reload();
    }
}
