<?php

declare(strict_types=1);

namespace Skadmin\ChildrensCamp\Components\Admin;

interface IOverviewSupplementFactory
{
    public function create() : OverviewSupplement;
}
