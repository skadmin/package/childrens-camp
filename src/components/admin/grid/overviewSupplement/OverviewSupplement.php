<?php

declare(strict_types=1);

namespace Skadmin\ChildrensCamp\Components\Admin;

use SkadminUtils\GridControls\UI\GridControl;
use SkadminUtils\GridControls\UI\GridDoctrine;
use Skadmin\Role\Doctrine\Role\Privilege;
use App\Model\Grid\Traits\IsActive;
use App\Model\System\APackageControl;
use App\Model\System\Constant;
use Nette\ComponentModel\IContainer;
use Nette\Security\User;
use Nette\Utils\Arrays;
use Nette\Utils\Html;
use Skadmin\ChildrensCamp\BaseControl;
use Skadmin\ChildrensCamp\Doctrine\ChildrensCamp\ChildrensCampSupplement\ChildrensCampSupplement;
use Skadmin\ChildrensCamp\Doctrine\ChildrensCamp\ChildrensCampSupplement\ChildrensCampSupplementFacade;
use Skadmin\Translator\Translator;

class OverviewSupplement extends GridControl
{
    use APackageControl;
    use IsActive;

    private ChildrensCampSupplementFacade $facade;

    public function __construct(ChildrensCampSupplementFacade $facade, Translator $translator, User $user)
    {
        parent::__construct($translator, $user);

        $this->facade = $facade;
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null)
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::READ)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function render() : void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/overviewSupplement.latte');
        $template->render();
    }

    public function getTitle() : string
    {
        return 'childrens-camp.overview-supplement.title';
    }

    protected function createComponentGrid(string $name) : GridDoctrine
    {
        // DATA
        $dataSupplements = Arrays::map(ChildrensCampSupplement::TYPES, fn(string $val) => $this->translator->translate($val));

        // GRID
        $grid = new GridDoctrine($this->getPresenter());

        // DEFAULT
        $grid->setPrimaryKey('id');
        $grid->setDataSource($this->facade->getModel());

        // COLUMNS
        $grid->addColumnText('name', 'grid.childrens-camp.overview-supplement.name')
            ->setRenderer(function (ChildrensCampSupplement $supplement) : Html {
                if ($this->isAllowed(BaseControl::RESOURCE, 'write')) {
                    $link = $this->getPresenter()->link('Component:default', [
                        'package' => new BaseControl(),
                        'render'  => 'edit-supplement',
                        'id'      => $supplement->getId(),
                    ]);

                    $name = Html::el('a', [
                        'href'  => $link,
                        'class' => 'font-weight-bold',
                    ]);
                } else {
                    $name = new Html();
                }

                $name->setText($supplement->getName());

                return $name;
            });
        $grid->addColumnText('type', 'grid.childrens-camp.overview-supplement.type')
            ->setReplacement($dataSupplements);
        $this->addColumnIsActive($grid, 'childrens-camp.overview-supplement');
        $grid->addColumnText('price', 'grid.childrens-camp.overview-supplement.price')
            ->setAlign('right');

        // FILTER
        $grid->addFilterText('name', 'grid.childrens-camp.overview-supplement.name');
        $grid->addFilterSelect('type', 'grid.childrens-camp.overview-supplement.type', Constant::PROMTP_ARR + $dataSupplements);
        $this->addFilterIsActive($grid, 'childrens-camp.overview-supplement');

        // ACTION
        if ($this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            $grid->addAction('edit', 'grid.childrens-camp.overview-supplement.action.edit', 'Component:default', ['id' => 'id'])->addParameters([
                'package' => new BaseControl(),
                'render'  => 'edit-supplement',
            ])->setIcon('pencil-alt')
                ->setClass('btn btn-xs btn-default btn-primary');
        }

        // TOOLBAR
        if ($this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            $grid->addToolbarButton('Component:default', 'grid.childrens-camp.overview-supplement.action.new', [
                'package' => new BaseControl(),
                'render'  => 'edit-supplement',
            ])->setIcon('plus')
                ->setClass('btn btn-xs btn-default btn-primary');
        }

        // OTHER
        $grid->setDefaultSort(['name' => 'ASC']);

        return $grid;
    }
}
