<?php

declare(strict_types=1);

namespace Skadmin\ChildrensCamp\Doctrine\ChildrensCampMailTemplate;

use SkadminUtils\DoctrineTraits\Facade;
use Nette\Utils\Arrays;
use Nettrine\ORM\EntityManagerDecorator;
use Skadmin\ChildrensCamp\Doctrine\ChildrensCamp\ChildrensCamp;
use Skadmin\File\Doctrine\File\File;

final class ChildrensCampMailTemplateFacade extends Facade
{

    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em);

        $this->table = ChildrensCampMailTemplate::class;
    }

    /**
     * @param ChildrensCamp $camp
     * @param array<ChildrensCampMailTemplateFake> $fakeMailTemplates
     * @return ChildrensCamp
     */
    public function addMailTemplates(ChildrensCamp $camp, array $fakeMailTemplates, string $author) : ChildrensCamp
    {
        // odebere nepoužité šablony
        $mailTemplateByClasses = [];
        foreach ($fakeMailTemplates as $fakeMailTemplate) {
            if (! $fakeMailTemplate instanceof ChildrensCampMailTemplateFake) {
                continue;
            }

            if (! isset($mailTemplateByClasses[$fakeMailTemplate->getClass()])) {
                $mailTemplateByClasses[$fakeMailTemplate->getClass()] = [
                    'type'        => $fakeMailTemplate->getType(),
                    'identifiers' => []
                ];
            }

            $mailTemplateByClasses[$fakeMailTemplate->getClass()]['identifiers'][] = $fakeMailTemplate->getIdentifier();
        }

        foreach ($mailTemplateByClasses as $class => $mailTemplateByClass) {
            $camp->removeMailTemplatesExclude($class, $mailTemplateByClass['type'], $mailTemplateByClass['identifiers']);
        }

        // přidáme nové šablony
        foreach ($fakeMailTemplates as $fakeMailTemplate) {
            $mailTemplate = $this->findMailTemplate($camp, $fakeMailTemplate->getClass(), $fakeMailTemplate->getType(), $fakeMailTemplate->getIdentifier());

            if (! $mailTemplate->isLoaded()) {
                $mailTemplate->create(
                    $camp,
                    $fakeMailTemplate->getName(),
                    $fakeMailTemplate->getType(),
                    $fakeMailTemplate->getClass(),
                    $fakeMailTemplate->getIdentifier(),
                    $fakeMailTemplate->getParameters()
                );
            }

            $mailTemplate->updateSpecific($fakeMailTemplate->getSubject(), $fakeMailTemplate->getRecipients(), $fakeMailTemplate->getPreheader(), $fakeMailTemplate->getContent(), $fakeMailTemplate->getAttachments(), $author);
            $this->em->persist($mailTemplate);
        }

        $this->em->persist($camp);
        $this->em->flush();

        return $camp;
    }

    // e.mail se vytvoří podle stavu, táboru a jedné předlohy... budou se zakládat podle potřeby a ne všechny automaticky

    public function findMailTemplate(ChildrensCamp $camp, string $class, string $type, string $identifier) : ChildrensCampMailTemplate
    {
        $mailTemplate = $this->em
            ->getRepository($this->table)
            ->findOneBy([
                'camp'       => $camp,
                'class'      => $class,
                'type'       => $type,
                'identifier' => $identifier,
            ]);

        return $mailTemplate instanceof ChildrensCampMailTemplate ? $mailTemplate : new ChildrensCampMailTemplate();
    }

    public function addFile(ChildrensCampMailTemplate $mailTemplate, File $file) : void
    {
        $mailTemplate->addFile($file);
        $this->em->flush();
    }

    public function getFileByHash(ChildrensCampMailTemplate $mailTemplate, string $hash) : ?File
    {
        return $mailTemplate->getFileByHash($hash);
    }

    public function removeFileByHash(ChildrensCampMailTemplate $mailTemplate, string $hash) : void
    {
        $mailTemplate->removeFileByHash($hash);
        $this->em->flush();
    }

}
