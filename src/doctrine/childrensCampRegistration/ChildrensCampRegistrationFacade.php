<?php

declare(strict_types=1);

namespace Skadmin\ChildrensCamp\Doctrine\ChildrensCampRegistration;

use SkadminUtils\DoctrineTraits\Facade;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\QueryBuilder;
use Nette\Utils\DateTime;
use Nettrine\ORM\EntityManagerDecorator;
use DateTimeInterface;
use Skadmin\ChildrensCamp\Doctrine\ChildrensCamp\ChildrensCamp;
use Skadmin\ChildrensCamp\Doctrine\ChildrensCamp\ChildrensCampRegistrationSupplement\ChildrensCampRegistrationSupplementFacade;
use Skadmin\ChildrensCamp\Doctrine\ChildrensCamp\ChildrensCampSupplement\ChildrensCampSupplement;
use Skadmin\InsuranceCompany\Doctrine\InsuranceCompany\InsuranceCompany;
use Skadmin\Payment\Doctrine\Payment\Payment;
use Skadmin\Transport\Doctrine\Transport\Transport;
use SkadminUtils\DoctrineTraits;

final class ChildrensCampRegistrationFacade extends Facade
{
    use DoctrineTraits\Facade\VariableSymbol;

    private ChildrensCampRegistrationSupplementFacade $facadeChildrensCampRegistrationSupplement;

    public function __construct(EntityManagerDecorator $em, ChildrensCampRegistrationSupplementFacade $facadeChildrensCampRegistrationSupplement)
    {
        parent::__construct($em);
        $this->table = ChildrensCampRegistration::class;

        $this->facadeChildrensCampRegistrationSupplement = $facadeChildrensCampRegistrationSupplement;
    }

    public function getModelForChildrensCamp(ChildrensCamp $childrensCamp): QueryBuilder
    {
        return $this->getModel()
            ->where('a.camp = ?1')
            ->setParameters([1 => $childrensCamp])
            ->orderBy('a.createdAt', 'DESC');
    }

    public function create(
        ChildrensCamp     $childrensCamp,
        string            $childFirstName,
        string            $childLastName,
        string            $personalIdentificationNumber,
        DateTimeInterface $birthdate,
        string            $street,
        string            $city,
        string            $zipCode,
        string            $contactEmail,
        string            $motherName,
        string            $motherPhone,
        string            $fatherName,
        string            $fatherPhone,
        string            $legalRepresentativeName,
        string            $legalRepresentativePhone,
        Transport         $transport,
        Payment           $payment,
        int               $swimmingSkills,
        InsuranceCompany  $insuranceCompany,
        bool              $confirmationForTheInsuranceCompany,
        string            $healthCondition,
        string            $washingFirst,
        string            $washingSecond,
        string            $washingThird,
        bool              $consentToTheCampRules,
        bool              $consentToPersonalDataProtection,
        bool              $consentToTheProcessingOfSensitiveData,
        bool              $permissionToSeparateAChildOnTrip,
        string            $note
    ): ChildrensCampRegistration
    {
        $childrensCampRegistration = $this->get();

        $childrensCampRegistration->create(
            $childrensCamp,
            $childFirstName,
            $childLastName,
            $personalIdentificationNumber,
            $birthdate,
            $street,
            $city,
            $zipCode,
            $contactEmail,
            $motherName,
            $motherPhone,
            $fatherName,
            $fatherPhone,
            $legalRepresentativeName,
            $legalRepresentativePhone,
            $transport,
            $payment,
            $swimmingSkills,
            $insuranceCompany,
            $confirmationForTheInsuranceCompany,
            $healthCondition,
            $washingFirst,
            $washingSecond,
            $washingThird,
            $consentToTheCampRules,
            $consentToPersonalDataProtection,
            $consentToTheProcessingOfSensitiveData,
            $permissionToSeparateAChildOnTrip,
            $note
        );

        $childrensCampRegistration->setVariableSymbol($this->getValidVariableSymbol(intval(sprintf('%d%s', date('y'), $childrensCamp->getVariableSymbolPrefix()))));

        $this->em->persist($childrensCampRegistration);
        $this->em->flush();

        return $childrensCampRegistration;
    }

    public function update(
        int               $childrensCampRegistrationId,
        string            $childFirstName,
        string            $childLastName,
        string            $personalIdentificationNumber,
        DateTimeInterface $birthdate,
        string            $street,
        string            $city,
        string            $zipCode,
        string            $contactEmail,
        string            $motherName,
        string            $motherPhone,
        string            $fatherName,
        string            $fatherPhone,
        string            $legalRepresentativeName,
        string            $legalRepresentativePhone,
        Transport         $transport,
        Payment           $payment,
        int               $swimmingSkills,
        InsuranceCompany  $insuranceCompany,
        bool              $confirmationForTheInsuranceCompany,
        string            $healthCondition,
        string            $washingFirst,
        string            $washingSecond,
        string            $washingThird,
        bool              $consentToTheCampRules,
        bool              $consentToPersonalDataProtection,
        bool              $consentToTheProcessingOfSensitiveData,
        bool              $permissionToSeparateAChildOnTrip,
        string            $note,
        float             $alreadyPaid
    ): ChildrensCampRegistration
    {
        $childrensCampRegistration = $this->get($childrensCampRegistrationId);

        $childrensCampRegistration->update(
            $childFirstName,
            $childLastName,
            $personalIdentificationNumber,
            $birthdate,
            $street,
            $city,
            $zipCode,
            $contactEmail,
            $motherName,
            $motherPhone,
            $fatherName,
            $fatherPhone,
            $legalRepresentativeName,
            $legalRepresentativePhone,
            $transport,
            $payment,
            $swimmingSkills,
            $insuranceCompany,
            $confirmationForTheInsuranceCompany,
            $healthCondition,
            $washingFirst,
            $washingSecond,
            $washingThird,
            $consentToTheCampRules,
            $consentToPersonalDataProtection,
            $consentToTheProcessingOfSensitiveData,
            $permissionToSeparateAChildOnTrip,
            $note,
            $alreadyPaid
        );

        $this->em->persist($childrensCampRegistration);
        $this->em->flush();

        return $childrensCampRegistration;
    }

    public function updateInvoice(ChildrensCampRegistration $childrensCampRegistration, string $invoiceFirm, string $invoiceIN, string $invoiceTIN, string $invoiceStreet, string $invoiceCity, string $invoiceZipCode, string $invoiceNote): ChildrensCampRegistration
    {
        $childrensCampRegistration->updateInvoice($invoiceFirm, $invoiceIN, $invoiceTIN, $invoiceStreet, $invoiceCity, $invoiceZipCode, $invoiceNote);

        $this->em->persist($childrensCampRegistration);
        $this->em->flush();

        return $childrensCampRegistration;
    }

    public function updateInvoicePDF(ChildrensCampRegistration $childrensCampRegistration, float $invoicePrice, ?DateTimeInterface $invoiceDateOfIssue, ?DateTimeInterface $invoiceDateOfTaxableSupply, ?DateTimeInterface $voiceDateDue): ChildrensCampRegistration
    {
        $childrensCampRegistration->updateInvoicePDF($invoicePrice, $invoiceDateOfIssue, $invoiceDateOfTaxableSupply, $voiceDateDue);

        $this->em->persist($childrensCampRegistration);
        $this->em->flush();

        return $childrensCampRegistration;
    }

    public function sendInvoice(ChildrensCampRegistration $childrensCampRegistration): ChildrensCampRegistration
    {
        $childrensCampRegistration->sendInvoice();

        $this->em->persist($childrensCampRegistration);
        $this->em->flush();

        return $childrensCampRegistration;
    }

    public function updateTshirt(ChildrensCampRegistration $childrensCampRegistration, ?string $tshirtColor, ?string $tshirtSize, int $tshirtCount, string $tshirtNote): ChildrensCampRegistration
    {
        $childrensCampRegistration->updateTshirt((string) $tshirtColor, (string) $tshirtSize, $tshirtCount, $tshirtNote);

        $this->em->persist($childrensCampRegistration);
        $this->em->flush();

        return $childrensCampRegistration;
    }

    /**
     * @param array<ChildrensCampSupplement> $supplements
     */
    public function updateSupplements(ChildrensCampRegistration $childrensCampRegistration, array $supplements): ChildrensCampRegistration
    {
        $registrationId = $childrensCampRegistration->getId();

        $registrationSupplements = [];
        $currentSupplements      = $childrensCampRegistration->getSupplements();

        // remove no selected supplements
        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->notIn('supplement', $supplements));
        foreach ($currentSupplements->matching($criteria)->toArray() as $supplementForRemove) {
            $this->em->remove($supplementForRemove);
        }

        // add new supplement
        foreach ($supplements as $supplement) {
            $criteria = Criteria::create();
            $criteria->where(Criteria::expr()->eq('supplement', $supplement));

            // when this supplement not yet in registration
            if ($currentSupplements->matching($criteria)->count() === 0) {
                $registrationSupplements[] = $this->facadeChildrensCampRegistrationSupplement->create($childrensCampRegistration, $supplement);
            }
        }

        $this->em->refresh($childrensCampRegistration);
        return $childrensCampRegistration;
    }

    public function updateStatus(int $childrensCampRegistrationId, int $status): ChildrensCampRegistration
    {
        $childrensCampRegistration = $this->get($childrensCampRegistrationId);

        $childrensCampRegistration->setStatus($status);

        $this->em->persist($childrensCampRegistration);
        $this->em->flush();

        return $childrensCampRegistration;
    }

    public function addPayment(int $childrensCampRegistrationId, float $payment): ChildrensCampRegistration
    {
        $childrensCampRegistration = $this->get($childrensCampRegistrationId);

        $childrensCampRegistration->addPayment($payment);

        $this->em->persist($childrensCampRegistration);
        $this->em->flush();

        return $childrensCampRegistration;
    }

    public function get(?int $id = null): ChildrensCampRegistration
    {
        if ($id === null) {
            return new ChildrensCampRegistration();
        }

        $childrensCampRegistration = parent::get($id);

        if ($childrensCampRegistration === null) {
            return new ChildrensCampRegistration();
        }

        return $childrensCampRegistration;
    }


    public function findByVariableSymbol(string $variableSymbol): ?ChildrensCampRegistration
    {
        $criteria = ['variableSymbol' => $variableSymbol];

        return $this->em
            ->getRepository($this->table)
            ->findOneBy($criteria);
    }

}
