<?php

declare(strict_types=1);

namespace Skadmin\ChildrensCamp\Doctrine\ChildrensCampRegistration;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\DBAL\Types\Types;
use Nette\Utils\DateTime;
use Skadmin\ChildrensCamp\Doctrine\ChildrensCamp\ChildrensCamp;
use Skadmin\ChildrensCamp\Doctrine\ChildrensCamp\ChildrensCampRegistrationSupplement\ChildrensCampRegistrationSupplement;
use Skadmin\ChildrensCamp\Doctrine\ChildrensCamp\ChildrensCampSupplement\ChildrensCampSupplement;
use Skadmin\InsuranceCompany\Doctrine\InsuranceCompany\InsuranceCompany;
use Skadmin\Payment\Doctrine\Payment\Payment;
use Skadmin\Transport\Doctrine\Transport\Transport;
use SkadminUtils\DoctrineTraits\Entity;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use function boolval;

#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class ChildrensCampRegistration
{
    public const STATE_NEW                            = 1;
    public const STATE_PAID                           = 2;
    public const STATE_INFORMATION_BEFORE_CAMP        = 3;
    public const STATE_TRANSPORT_TO_CAMP              = 4;
    public const STATE_DOCUMENTS_FOR_CAMP             = 5;
    public const STATE_LATEST_INFORMATION_BEFORE_CAMP = 6;
    public const STATE_CANCELED                       = 7;

    public const STATES = [
        self::STATE_NEW                            => 'childrens-camp-registration.state-new',
        self::STATE_PAID                           => 'childrens-camp-registration.state-paid',
        self::STATE_INFORMATION_BEFORE_CAMP        => 'childrens-camp-registration.state-information-before-camp',
        self::STATE_TRANSPORT_TO_CAMP              => 'childrens-camp-registration.state-transport-to-camp',
        self::STATE_DOCUMENTS_FOR_CAMP             => 'childrens-camp-registration.state-documents-for-camp',
        self::STATE_LATEST_INFORMATION_BEFORE_CAMP => 'childrens-camp-registration.state-latest-information-before-camp',
        self::STATE_CANCELED                       => 'childrens-camp-registration.state-canceled',
    ];

    public const SWIMMING_SKILLS_SWIMMER        = 1;
    public const SWIMMING_SKILLS_WEAKER_SWIMMER = 2;
    public const SWIMMING_SKILLS_NON_SWIMMER    = 3;

    public const SWIMMINGS_SKILLS = [
        self::SWIMMING_SKILLS_SWIMMER        => 'childrens-camp-registration.swimming-skills-swimmer',
        self::SWIMMING_SKILLS_WEAKER_SWIMMER => 'childrens-camp-registration.swimming-skills-weaker-swimmer',
        self::SWIMMING_SKILLS_NON_SWIMMER    => 'childrens-camp-registration.swimming-skills-non-swimmer',
    ];

    public const PDF_TYPE_INVOICE = 1;

    use Entity\BaseEntity;
    use Entity\Status;
    use Entity\VariableSymbol;

    // 1. Část = Základní údaje

    #[ORM\Column]
    private string $childFirstName;

    #[ORM\Column]
    private string $childLastName;

    #[ORM\Column]
    private string $personalIdentificationNumber;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private DateTimeInterface $birthdate;

    // 2. Část = Bydliště

    #[ORM\Column]
    private string $street;

    #[ORM\Column]
    private string $city;

    #[ORM\Column]
    private string $zipCode;

    // 3. Část = Volba tábora

    #[ORM\ManyToOne(targetEntity: ChildrensCamp::class, inversedBy: 'registrations')]
    #[ORM\JoinColumn(onDelete: 'cascade')]
    private ChildrensCamp $camp;

    #[ORM\Column]
    private float $campPrice;

    // 4. Část = Kontaktní údaje (Rodiče / Zákonný zástupce)

    #[ORM\Column]
    private string $contactEmail;

    #[ORM\Column]
    private string $motherName;

    #[ORM\Column]
    private string $motherPhone;

    #[ORM\Column]
    private string $fatherName;

    #[ORM\Column]
    private string $fatherPhone;

    #[ORM\Column]
    private string $legalRepresentativeName;

    #[ORM\Column]
    private string $legalRepresentativePhone;

    // 5. Část = Doprava na tábor

    #[ORM\ManyToOne(targetEntity: Transport::class)]
    #[ORM\JoinColumn(onDelete: 'cascade')]
    private Transport $transport;

    // 6. Část = Způsob platby

    #[ORM\ManyToOne(targetEntity: Payment::class)]
    #[ORM\JoinColumn(onDelete: 'cascade')]
    private Payment $payment;

    // 7. Část = Informace o dítěti

    #[ORM\Column]
    private ?int $swimmingSkills = null;

    #[ORM\ManyToOne(targetEntity: InsuranceCompany::class)]
    #[ORM\JoinColumn(onDelete: 'cascade')]
    private InsuranceCompany $insuranceCompany;

    #[ORM\Column(options: ['default' => false])]
    private bool $confirmationForTheInsuranceCompany;

    #[ORM\Column(length: 2048)]
    private string $healthCondition;

    #[ORM\Column(length: 2048)]
    private string $washingFirst;

    #[ORM\Column(length: 2048)]
    private string $washingSecond;

    #[ORM\Column(length: 2048)]
    private string $washingThird;

    // 8. Část = Fakturace

    #[ORM\Column]
    private string $invoiceFirm = '';

    #[ORM\Column]
    private string $invoiceIN = ''; // IČO

    #[ORM\Column]
    private string $invoiceTIN = ''; // DIČ

    #[ORM\Column]
    private string $invoiceStreet = '';

    #[ORM\Column]
    private string $invoiceCity = '';

    #[ORM\Column]
    private string $invoiceZipCode = '';

    #[ORM\Column(length: 2048)]
    private string $invoiceNote = '';

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?DateTimeInterface $invoiceSentAt = null;

    #[ORM\Column]
    private float $invoicePrice = 0.0;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?DateTimeInterface $invoiceDateOfIssue = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?DateTimeInterface $invoiceDateOfTaxableSupply = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?DateTimeInterface $invoiceDateDue = null;

    // 9. Část = Sleva / Příplatky

    /** @var ArrayCollection|array<ChildrensCampRegistrationSupplement> */
    #[ORM\OneToMany(targetEntity: ChildrensCampRegistrationSupplement::class, mappedBy: 'registration')]
    private $supplements;

    // 10. Část = Souhlasy

    #[ORM\Column(options: ['default' => false])]
    private bool $consentToTheCampRules;

    #[ORM\Column(options: ['default' => false])]
    private bool $consentToPersonalDataProtection;

    #[ORM\Column(options: ['default' => false])]
    private bool $consentToTheProcessingOfSensitiveData;

    #[ORM\Column(options: ['default' => false])]
    private bool $permissionToSeparateAChildOnTrip;

    // 11. Část = Táborové tričko

    #[ORM\Column]
    private string $tshirtColor = '';

    #[ORM\Column]
    private string $tshirtSize = '';

    #[ORM\Column]
    private int $tshirtCount = 0;

    #[ORM\Column(length: 2048)]
    private string $tshirtNote = '';

    // 12. Část = Závěr

    #[ORM\Column(length: 2048)]
    private string $note;

    #[ORM\Column(options: ['default' => 0.0])]
    private float $alreadyPaid = 0.0;

    public function __construct()
    {
        $this->status      = self::STATE_NEW;
        $this->supplements = new ArrayCollection();
    }

    public function create(
        ChildrensCamp     $childrensCamp,
        string            $childFirstName,
        string            $childLastName,
        string            $personalIdentificationNumber,
        DateTimeInterface $birthdate,
        string            $street,
        string            $city,
        string            $zipCode,
        string            $contactEmail,
        string            $motherName,
        string            $motherPhone,
        string            $fatherName,
        string            $fatherPhone,
        string            $legalRepresentativeName,
        string            $legalRepresentativePhone,
        Transport         $transport,
        Payment           $payment,
        int               $swimmingSkills,
        InsuranceCompany  $insuranceCompany,
        bool              $confirmationForTheInsuranceCompany,
        string            $healthCondition,
        string            $washingFirst,
        string            $washingSecond,
        string            $washingThird,
        bool              $consentToTheCampRules,
        bool              $consentToPersonalDataProtection,
        bool              $consentToTheProcessingOfSensitiveData,
        bool              $permissionToSeparateAChildOnTrip,
        string            $note
    ): void
    {
        $this->camp      = $childrensCamp;
        $this->campPrice = $childrensCamp->getPrice();

        $this->childFirstName               = $childFirstName;
        $this->childLastName                = $childLastName;
        $this->personalIdentificationNumber = $personalIdentificationNumber;
        $this->birthdate                    = $birthdate;

        $this->street  = $street;
        $this->city    = $city;
        $this->zipCode = $zipCode;

        $this->contactEmail             = $contactEmail;
        $this->motherName               = $motherName;
        $this->motherPhone              = $motherPhone;
        $this->fatherName               = $fatherName;
        $this->fatherPhone              = $fatherPhone;
        $this->legalRepresentativeName  = $legalRepresentativeName;
        $this->legalRepresentativePhone = $legalRepresentativePhone;

        $this->transport = $transport;
        $this->payment   = $payment;

        $this->swimmingSkills                     = $swimmingSkills;
        $this->insuranceCompany                   = $insuranceCompany;
        $this->confirmationForTheInsuranceCompany = $confirmationForTheInsuranceCompany;
        $this->healthCondition                    = $healthCondition;
        $this->washingFirst                       = $washingFirst;
        $this->washingSecond                      = $washingSecond;
        $this->washingThird                       = $washingThird;

        $this->consentToTheCampRules                 = $consentToTheCampRules;
        $this->consentToPersonalDataProtection       = $consentToPersonalDataProtection;
        $this->consentToTheProcessingOfSensitiveData = $consentToTheProcessingOfSensitiveData;
        $this->permissionToSeparateAChildOnTrip      = $permissionToSeparateAChildOnTrip;

        $this->note = $note;
    }

    public function update(
        string            $childFirstName,
        string            $childLastName,
        string            $personalIdentificationNumber,
        DateTimeInterface $birthdate,
        string            $street,
        string            $city,
        string            $zipCode,
        string            $contactEmail,
        string            $motherName,
        string            $motherPhone,
        string            $fatherName,
        string            $fatherPhone,
        string            $legalRepresentativeName,
        string            $legalRepresentativePhone,
        Transport         $transport,
        Payment           $payment,
        int               $swimmingSkills,
        InsuranceCompany  $insuranceCompany,
        bool              $confirmationForTheInsuranceCompany,
        string            $healthCondition,
        string            $washingFirst,
        string            $washingSecond,
        string            $washingThird,
        bool              $consentToTheCampRules,
        bool              $consentToPersonalDataProtection,
        bool              $consentToTheProcessingOfSensitiveData,
        bool              $permissionToSeparateAChildOnTrip,
        string            $note,
        float             $alreadyPaid
    ): void
    {
        $this->childFirstName               = $childFirstName;
        $this->childLastName                = $childLastName;
        $this->personalIdentificationNumber = $personalIdentificationNumber;
        $this->birthdate                    = $birthdate;

        $this->street  = $street;
        $this->city    = $city;
        $this->zipCode = $zipCode;

        $this->contactEmail             = $contactEmail;
        $this->motherName               = $motherName;
        $this->motherPhone              = $motherPhone;
        $this->fatherName               = $fatherName;
        $this->fatherPhone              = $fatherPhone;
        $this->legalRepresentativeName  = $legalRepresentativeName;
        $this->legalRepresentativePhone = $legalRepresentativePhone;

        $this->transport = $transport;
        $this->payment   = $payment;

        $this->swimmingSkills                     = $swimmingSkills;
        $this->insuranceCompany                   = $insuranceCompany;
        $this->confirmationForTheInsuranceCompany = $confirmationForTheInsuranceCompany;
        $this->healthCondition                    = $healthCondition;
        $this->washingFirst                       = $washingFirst;
        $this->washingSecond                      = $washingSecond;
        $this->washingThird                       = $washingThird;

        $this->consentToTheCampRules                 = $consentToTheCampRules;
        $this->consentToPersonalDataProtection       = $consentToPersonalDataProtection;
        $this->consentToTheProcessingOfSensitiveData = $consentToTheProcessingOfSensitiveData;
        $this->permissionToSeparateAChildOnTrip      = $permissionToSeparateAChildOnTrip;

        $this->note        = $note;
        $this->alreadyPaid = $alreadyPaid;
    }

    public function updateInvoice(string $invoiceFirm, string $invoiceIN, string $invoiceTIN, string $invoiceStreet, string $invoiceCity, string $invoiceZipCode, string $invoiceNote): void
    {
        $this->invoiceFirm    = $invoiceFirm;
        $this->invoiceIN      = $invoiceIN;
        $this->invoiceTIN     = $invoiceTIN;
        $this->invoiceStreet  = $invoiceStreet;
        $this->invoiceCity    = $invoiceCity;
        $this->invoiceZipCode = $invoiceZipCode;
        $this->invoiceNote    = $invoiceNote;
    }

    public function updateInvoicePDF(float $invoicePrice, ?DateTimeInterface $invoiceDateOfIssue, ?DateTimeInterface $invoiceDateOfTaxableSupply, ?DateTimeInterface $invoiceDateDue): void
    {
        $this->invoicePrice               = $invoicePrice;
        $this->invoiceDateOfIssue         = $invoiceDateOfIssue;
        $this->invoiceDateOfTaxableSupply = $invoiceDateOfTaxableSupply;
        $this->invoiceDateDue             = $invoiceDateDue;
    }

    public function sendInvoice(): void
    {
        $this->invoiceSentAt = new DateTime();
    }

    public function updateTshirt(string $tshirtColor, string $tshirtSize, int $tshirtCount, string $tshirtNote): void
    {
        $this->tshirtColor = $tshirtColor;
        $this->tshirtSize  = $tshirtSize;
        $this->tshirtCount = $tshirtCount;
        $this->tshirtNote  = $tshirtNote;
    }

    public function setStatus(int $status): void
    {
        if ($this->status === $status) {
            return;
        }

        $this->status = $status;
    }

    public function getChildFirstName(): string
    {
        return $this->childFirstName;
    }

    public function getChildLastName(): string
    {
        return $this->childLastName;
    }

    public function getPersonalIdentificationNumber(): string
    {
        return $this->personalIdentificationNumber;
    }

    public function getBirthdate(): DateTimeInterface
    {
        return $this->birthdate;
    }

    public function getStreet(): string
    {
        return $this->street;
    }

    public function getCity(): string
    {
        return $this->city;
    }

    public function getZipCode(): string
    {
        return $this->zipCode;
    }

    public function getCamp(): ChildrensCamp
    {
        return $this->camp;
    }

    public function getCampPrice(): float
    {
        return $this->campPrice;
    }

    public function getContactEmail(): string
    {
        return $this->contactEmail;
    }

    public function getMotherName(): string
    {
        return $this->motherName;
    }

    public function getMotherPhone(): string
    {
        return $this->motherPhone;
    }

    public function getFatherName(): string
    {
        return $this->fatherName;
    }

    public function getFatherPhone(): string
    {
        return $this->fatherPhone;
    }

    public function getLegalRepresentativeName(): string
    {
        return $this->legalRepresentativeName;
    }

    public function getLegalRepresentativePhone(): string
    {
        return $this->legalRepresentativePhone;
    }

    public function getTransport(): Transport
    {
        return $this->transport;
    }

    public function getPayment(): Payment
    {
        return $this->payment;
    }

    public function getSwimmingSkills(): ?int
    {
        return $this->swimmingSkills;
    }

    public function getInsuranceCompany(): InsuranceCompany
    {
        return $this->insuranceCompany;
    }

    public function isConfirmationForTheInsuranceCompany(): bool
    {
        return $this->confirmationForTheInsuranceCompany;
    }

    public function getHealthCondition(): string
    {
        return $this->healthCondition;
    }

    public function getWashingFirst(): string
    {
        return $this->washingFirst;
    }

    public function getWashingSecond(): string
    {
        return $this->washingSecond;
    }

    public function getWashingThird(): string
    {
        return $this->washingThird;
    }

    public function requireInvoice(): bool
    {
        return trim($this->getInvoiceIN() . $this->getInvoiceFirm()) !== '';
    }

    public function getInvoiceFirm(): string
    {
        return $this->invoiceFirm;
    }

    public function getInvoiceIN(): string
    {
        return $this->invoiceIN;
    }

    public function getInvoiceTIN(): string
    {
        return $this->invoiceTIN;
    }

    public function getInvoiceStreet(): string
    {
        return $this->invoiceStreet;
    }

    public function getInvoiceCity(): string
    {
        return $this->invoiceCity;
    }

    public function getInvoiceZipCode(): string
    {
        return $this->invoiceZipCode;
    }

    public function getInvoiceNote(): string
    {
        return $this->invoiceNote;
    }

    public function getInvoiceSentAt(): ?DateTimeInterface
    {
        return $this->invoiceSentAt;
    }

    public function isInvoiceSended(): bool
    {
        return $this->invoiceSentAt !== null;
    }

    public function getInvoicePrice(): float
    {
        if ($this->invoicePrice <= 0) {
            return $this->getTotalPrice();
        }

        return $this->invoicePrice;
    }

    public function getInvoiceDateOfIssue(): ?DateTimeInterface
    {
        if (! $this->invoiceDateOfIssue instanceof DateTimeInterface) {
            return clone $this->getCreatedAt();
        }

        return $this->invoiceDateOfIssue;
    }

    public function getInvoiceDateOfTaxableSupply(): ?DateTimeInterface
    {
        if (! $this->invoiceDateOfTaxableSupply instanceof DateTimeInterface) {
            return (clone $this->getCreatedAt())->modify('+31days');
        }

        return $this->invoiceDateOfTaxableSupply;
    }

    public function getInvoiceDateDue(): ?DateTimeInterface
    {
        if (! $this->invoiceDateDue instanceof DateTimeInterface) {
            return clone $this->getCreatedAt();
        }

        return $this->invoiceDateDue;
    }

    /**
     * @return ArrayCollection|ChildrensCampRegistrationSupplement[]
     */
    public function getSupplements()
    {
        return $this->supplements;
    }

    public function isConsentToTheCampRules(): bool
    {
        return $this->consentToTheCampRules;
    }

    public function isConsentToPersonalDataProtection(): bool
    {
        return $this->consentToPersonalDataProtection;
    }

    public function isConsentToTheProcessingOfSensitiveData(): bool
    {
        return $this->consentToTheProcessingOfSensitiveData;
    }

    public function isPermissionToSeparateAChildOnTrip(): bool
    {
        return $this->permissionToSeparateAChildOnTrip;
    }

    public function getTshirtColor(): string
    {
        return $this->tshirtColor;
    }

    public function getTshirtSize(): string
    {
        return $this->tshirtSize;
    }

    public function getTshirtCount(): int
    {
        return $this->tshirtCount;
    }

    public function getTshirtNote(): string
    {
        return $this->tshirtNote;
    }

    public function getNote(): string
    {
        return $this->note;
    }

    public function getFullName(bool $reversed = false): string
    {
        if ($reversed) {
            return sprintf('%s %s', $this->getChildLastName(), $this->getChildFirstName());
        }

        return sprintf('%s %s', $this->getChildFirstName(), $this->getChildLastName());
    }

    public function getFullNameWithPID(bool $reversed = false): string
    {
        return sprintf('%s - %s', $this->getPersonalIdentificationNumber(), $this->getFullName($reversed));
    }

    public function getTotalPrice(): float
    {
        $totalPrice = $this->getCampPrice();
        if ($this->getTshirtColor() !== '') {
            $totalPrice += $this->getTshirtCount() * $this->getCamp()->getTshirtPrice();
        }

        foreach ($this->getSupplements() as $relSupplement) {
            $totalPrice += $relSupplement->getSupplement()->getPrice(true);
        }

        return $totalPrice;
    }

    public function getChildAge(): int
    {
        return intval($this->getBirthdate()->diff($this->getCamp()->getTermFrom())->format('%y'));
    }

    public function getAlreadyPaid()
    {
        return $this->alreadyPaid;
    }

    public function addPayment(float $payment): float
    {
        $this->alreadyPaid += $payment;
        return $this->getAlreadyPaid();
    }

}
