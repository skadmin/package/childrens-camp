<?php

declare(strict_types=1);

namespace Skadmin\ChildrensCamp\Doctrine\ChildrensCamp\ChildrensCampRegistrationSupplement;

use SkadminUtils\DoctrineTraits\Entity;
use Skadmin\ChildrensCamp\Doctrine\ChildrensCamp\ChildrensCampSupplement\ChildrensCampSupplement;
use Skadmin\ChildrensCamp\Doctrine\ChildrensCampRegistration\ChildrensCampRegistration;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class ChildrensCampRegistrationSupplement
{
    use Entity\Id;

    #[ORM\Column]
    private float $price = 0;

    #[ORM\ManyToOne(targetEntity: ChildrensCampRegistration::class, inversedBy: 'supplements', cascade: ['persist'])]
    #[ORM\JoinColumn(onDelete: 'cascade')]
    private ChildrensCampRegistration $registration;

    #[ORM\ManyToOne(targetEntity: ChildrensCampSupplement::class)]
    #[ORM\JoinColumn(onDelete: 'cascade')]
    private ChildrensCampSupplement $supplement;

    public function create(ChildrensCampRegistration $registration, ChildrensCampSupplement $supplement) : void
    {
        $this->registration = $registration;
        $this->supplement   = $supplement;
        $this->price        = $supplement->getPrice();
    }

    public function update(float $price) : void
    {
        $this->price = $price;
    }


    public function getPrice(bool $byType = false) : float
    {
        if ($byType && $this->getSupplement()->isDiscount()) {
            return $this->price * -1;
        }

        return $this->price;
    }

    public function getRegistration() : ChildrensCampRegistration
    {
        return $this->registration;
    }

    public function getSupplement() : ChildrensCampSupplement
    {
        return $this->supplement;
    }

}
