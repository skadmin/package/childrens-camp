<?php

declare(strict_types=1);

namespace Skadmin\ChildrensCamp\Doctrine\ChildrensCamp\ChildrensCampRegistrationSupplement;

use SkadminUtils\DoctrineTraits\Facade;
use App\Model\Doctrine\Traits;
use Nettrine\ORM\EntityManagerDecorator;
use Skadmin\ChildrensCamp\Doctrine\ChildrensCamp\ChildrensCampSupplement\ChildrensCampSupplement;
use Skadmin\ChildrensCamp\Doctrine\ChildrensCampRegistration\ChildrensCampRegistration;

final class ChildrensCampRegistrationSupplementFacade extends Facade
{

    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em);
        $this->table = ChildrensCampRegistrationSupplement::class;
    }

    public function create(ChildrensCampRegistration $registration, ChildrensCampSupplement $supplement) : ChildrensCampRegistrationSupplement
    {
        $childrensCampRegistrationSupplement = $this->get();
        $childrensCampRegistrationSupplement->create($registration, $supplement);

        $this->em->persist($childrensCampRegistrationSupplement);
        $this->em->flush();

        return $childrensCampRegistrationSupplement;
    }

    public function get(?int $id = null) : ChildrensCampRegistrationSupplement
    {
        if ($id === null) {
            return new ChildrensCampRegistrationSupplement();
        }

        $childrensCampRegistrationSupplement = parent::get($id);

        if ($childrensCampRegistrationSupplement === null) {
            return new ChildrensCampRegistrationSupplement();
        }

        return $childrensCampRegistrationSupplement;
    }
}
