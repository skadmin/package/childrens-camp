<?php

declare(strict_types=1);

namespace Skadmin\ChildrensCamp\Doctrine\ChildrensCamp\ChildrensCampSupplement;

use SkadminUtils\DoctrineTraits\Entity;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class ChildrensCampSupplement
{

    public const TYPE_SUPPLEMENT = 1;
    public const TYPE_DISCOUNT   = 2;

    public const TYPES = [
        self::TYPE_SUPPLEMENT => 'childrens-camp-supplement.type-supplement',
        self::TYPE_DISCOUNT   => 'childrens-camp-supplement.type-discount',
    ];

    use Entity\Id;
    use Entity\Name;
    use Entity\Content;
    use Entity\IsActive;

    #[ORM\Column]
    private float $price = 0.0;

    #[ORM\Column]
    private int $type;

    public function update(string $name, bool $isActive, string $content, float $price, int $type) : void
    {
        $this->name     = $name;
        $this->isActive = $isActive;
        $this->content  = $content;
        $this->price    = $price;
        $this->type     = $type;
    }

    public function getPrice(bool $byType = false) : float
    {
        if ($byType && $this->isDiscount()) {
            return $this->price * -1;
        }

        return $this->price;
    }

    public function getType() : int
    {
        return $this->type;
    }

    public function isDiscount() : bool
    {
        return $this->type === self::TYPE_DISCOUNT;
    }
}
