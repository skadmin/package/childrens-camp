<?php

declare(strict_types=1);

namespace Skadmin\ChildrensCamp\Doctrine\ChildrensCamp\ChildrensCampSupplement;

use SkadminUtils\DoctrineTraits\Facade;
use Nettrine\ORM\EntityManagerDecorator;

final class ChildrensCampSupplementFacade extends Facade
{
    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em);
        $this->table = ChildrensCampSupplement::class;
    }

    public function create(string $name, bool $isActive, string $content, float $price, int $type) : ChildrensCampSupplement
    {
        return $this->update(null, $name, $isActive, $content, $price, $type);
    }

    public function update(?int $id, string $name, bool $isActive, string $content, float $price, int $type) : ChildrensCampSupplement
    {
        $supplement = $this->get($id);
        $supplement->update($name, $isActive, $content, $price, $type);

        $this->em->persist($supplement);
        $this->em->flush();

        return $supplement;
    }

    public function get(?int $id = null) : ChildrensCampSupplement
    {
        if ($id === null) {
            return new ChildrensCampSupplement();
        }

        $supplement = parent::get($id);

        if ($supplement === null) {
            return new ChildrensCampSupplement();
        }

        return $supplement;
    }

    /**
     * @return ChildrensCampSupplement[]
     */
    public function getAll(bool $onlyActive = false) : array
    {
        $criteria = [];

        if ($onlyActive) {
            $criteria['isActive'] = true;
        }

        return $this->em
            ->getRepository($this->table)
            ->findBy($criteria);
    }
}
