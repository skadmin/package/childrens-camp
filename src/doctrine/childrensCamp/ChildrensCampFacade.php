<?php

declare(strict_types=1);

namespace Skadmin\ChildrensCamp\Doctrine\ChildrensCamp;

use SkadminUtils\ChildrensCamp\Factory\ChildrensCampFactory;
use SkadminUtils\DoctrineTraits\Facade;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\QueryBuilder;
use Nette\Utils\DateTime;
use Nettrine\ORM\EntityManagerDecorator;
use DateTimeInterface;
use Skadmin\ChildrensCamp\Doctrine\ChildrensCamp\ChildrensCampSupplement\ChildrensCampSupplement;
use Skadmin\File\Doctrine\File\File;
use Skadmin\Payment\Doctrine\Payment\Payment;
use Skadmin\Transport\Doctrine\Transport\Transport;
use SkadminUtils\DoctrineTraits;

final class ChildrensCampFacade extends Facade
{
    use DoctrineTraits\Facade\Webalize;
    use DoctrineTraits\Facade\Sequence;
    use DoctrineTraits\Facade\Code;

    private ChildrensCampFactory $childrensCampSettings;

    public function __construct(EntityManagerDecorator $em, ChildrensCampFactory $childrensCampSettings)
    {
        parent::__construct($em);
        $this->table = ChildrensCamp::class;

        $this->childrensCampSettings = $childrensCampSettings;
    }

    public function create(string $name, string $description, string $content, string $textPrice, string $textCampAreal, string $textTransport, string $textMenu, string $textQuestBook, DateTimeInterface $termFrom, DateTimeInterface $termTo, string $address, int $capacity, bool $isActive, ?string $imagePreview): ChildrensCamp
    {
        return $this->update(null, $name, $description, $content, $textPrice, $textCampAreal, $textTransport, $textMenu, $textQuestBook, $termFrom, $termTo, $address, $capacity, $isActive, $imagePreview);
    }

    public function update(?int $id, string $name, string $description, string $content, string $textPrice, string $textCampAreal, string $textTransport, string $textMenu, string $textQuestBook, DateTimeInterface $termFrom, DateTimeInterface $termTo, string $address, int $capacity, bool $isActive, ?string $imagePreview, bool $changeWebalize = false): ChildrensCamp
    {
        $childrensCamp = $this->get($id);
        $childrensCamp->update($name, $description, $content, $textPrice, $textCampAreal, $textTransport, $textMenu, $textQuestBook, $termFrom, $termTo, $address, $capacity, $isActive, $imagePreview);

        if (! $childrensCamp->isLoaded()) {
            $childrensCamp->setWebalize($this->getValidWebalize($name));
            $childrensCamp->setCode($this->getValidCode());

            if ($this->childrensCampSettings->isPrependChildrensCamp()) {
                foreach ($this->getAll() as $tChildrensCamp) {
                    $tChildrensCamp->updateSequence($tChildrensCamp->getSequence() + 1);
                }
                $childrensCamp->setSequence(0);
            } else {
                $childrensCamp->setSequence($this->getValidSequence());
            }

        } elseif ($changeWebalize) {
            $childrensCamp->updateWebalize($this->getValidWebalize($name));
        }

        $this->em->persist($childrensCamp);
        $this->em->flush();

        return $childrensCamp;
    }

    /**
     * @param array<Transport> $transports
     * @param array<Payment> $payments
     * @param array<string> $tshirtColorList
     * @param array<string> $tshirtSizeList
     */
    public function updateRegistration(ChildrensCamp $childrensCamp, string $textRegistration, array $transports, array $payments, array $tshirtColorList, array $tshirtSizeList, DateTimeInterface $tshirtDeathLine, string $variableSymbolPrefix, bool $isOpenRegistration, string $registrationExternalLink): ChildrensCamp
    {
        $childrensCamp->updateRegistration($textRegistration, $transports, $payments, $tshirtColorList, $tshirtSizeList, $tshirtDeathLine, $variableSymbolPrefix, $isOpenRegistration, $registrationExternalLink);
        $this->em->persist($childrensCamp);
        $this->em->flush();

        return $childrensCamp;
    }

    /**
     * @param array<ChildrensCampSupplement> $supplements
     */
    public function updatePriceAndSupplement(ChildrensCamp $childrensCamp, float $price, float $tshirtPrice, array $supplements)
    {
        $childrensCamp->updatePriceAndSupplement($price, $tshirtPrice, $supplements);
        $this->em->persist($childrensCamp);
        $this->em->flush();

        return $childrensCamp;
    }

    public function get(?int $id = null): ChildrensCamp
    {
        if ($id === null) {
            return new ChildrensCamp();
        }

        $user = parent::get($id);

        if ($user === null) {
            return new ChildrensCamp();
        }

        return $user;
    }

    public function findByWebalize(string $webalize): ?ChildrensCamp
    {
        $criteria = ['webalize' => $webalize];

        return $this->em
            ->getRepository($this->table)
            ->findOneBy($criteria);
    }

    public function findByCode(string $code): ?ChildrensCamp
    {
        $criteria = ['code' => $code];

        return $this->em
            ->getRepository($this->table)
            ->findOneBy($criteria);
    }

    /**
     * @return ChildrensCamp[]
     */
    public function findForSitemap(): array
    {
        return $this->getAll(true);
    }

    /**
     * @return ChildrensCamp[]
     */
    public function getAll(bool $onlyActive = false): array
    {
        $criteria = [];

        if ($onlyActive) {
            $criteria['isActive'] = true;
        }

        $orderBy = ['sequence' => 'ASC'];

        return $this->em
            ->getRepository($this->table)
            ->findBy($criteria, $orderBy);
    }

    /** @return array<ChildrensCamp> */
    public function findInFuture(?int $limit = null): array
    {
        $currentDateTimeStart = (new DateTime())->setTime(0, 0, 0);

        /** @var QueryBuilder $qb */
        $qb = $this->em
            ->getRepository($this->table)
            ->createQueryBuilder('a');

        $qb->setMaxResults($limit)
            ->orderBy('a.sequence', 'ASC');

        $criteria = Criteria::create();
        $criteria
            ->where(Criteria::expr()->eq('a.isActive', true))
            ->andWhere(Criteria::expr()->gte('a.termTo', $currentDateTimeStart));

        $qb->addCriteria($criteria);

        return $qb->getQuery()->getResult();
    }

    /** @return array<ChildrensCamp> */
    public function findInPast(?int $limit = null): array
    {
        $currentDateTimeStart = (new DateTime())->setTime(0, 0, 0);

        /** @var QueryBuilder $qb */
        $qb = $this->em
            ->getRepository($this->table)
            ->createQueryBuilder('a');

        $qb->setMaxResults($limit)
            ->orderBy('a.sequence', 'DESC');

        $criteria = Criteria::create();
        $criteria
            ->where(Criteria::expr()->eq('a.isActive', true))
            ->andWhere(Criteria::expr()->lte('a.termTo', $currentDateTimeStart));

        $qb->addCriteria($criteria);

        return $qb->getQuery()->getResult();
    }

    public function addFile(ChildrensCamp $childrensCamp, File $file): void
    {
        $childrensCamp->addFile($file);
        $this->em->flush();
    }

    public function getFileByHash(ChildrensCamp $childrensCamp, string $hash): ?File
    {
        return $childrensCamp->getFileByHash($hash);
    }

    public function removeFileByHash(ChildrensCamp $childrensCamp, string $hash): void
    {
        $childrensCamp->removeFileByHash($hash);
        $this->em->flush();
    }

}
