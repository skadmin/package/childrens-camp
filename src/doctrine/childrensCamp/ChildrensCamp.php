<?php

declare(strict_types=1);

namespace Skadmin\ChildrensCamp\Doctrine\ChildrensCamp;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\DBAL\Types\Types;
use Nette\Utils\DateTime;
use Skadmin\ChildrensCamp\Doctrine\ChildrensCamp\ChildrensCampSupplement\ChildrensCampSupplement;
use Skadmin\ChildrensCamp\Doctrine\ChildrensCampMailTemplate\ChildrensCampMailTemplate;
use Skadmin\ChildrensCamp\Doctrine\ChildrensCampRegistration\ChildrensCampRegistration;
use Skadmin\File\Doctrine\File\File;
use Skadmin\File\Traits\TFile;
use Skadmin\Payment\Doctrine\Payment\Payment;
use Skadmin\Transport\Doctrine\Transport\Transport;
use SkadminUtils\DoctrineTraits\Entity;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use function boolval;

#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class ChildrensCamp
{
    use TFile;
    use Entity\BaseEntity;
    use Entity\WebalizeName;
    use Entity\Description;
    use Entity\Content;
    use Entity\IsActive;
    use Entity\Code;
    use Entity\Term;
    use Entity\Address;
    use Entity\ImagePreview;
    use Entity\Sequence;

    #[ORM\Column]
    private string $variableSymbolPrefix = '';

    #[ORM\Column]
    private int $capacity = 0;

    #[ORM\Column(type: Types::TEXT)]
    private string $textPrice = '';

    #[ORM\Column(type: Types::TEXT)]
    private string $textCampAreal = '';

    #[ORM\Column(type: Types::TEXT)]
    private string $textTransport = '';

    #[ORM\Column(type: Types::TEXT)]
    private string $textMenu = '';

    #[ORM\Column(type: Types::TEXT)]
    private string $textQuestBook = '';

    #[ORM\Column(type: Types::TEXT)]
    private string $textRegistration = '';

    #[ORM\Column]
    private float $price = 0;

    #[ORM\Column(type: Types::SIMPLE_ARRAY)]
    private array $tshirtColorList = ['-'];

    #[ORM\Column(type: Types::SIMPLE_ARRAY)]
    private array $tshirtSizeList = ['-'];

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private DateTimeInterface $tshirtDeathLine;

    #[ORM\Column]
    private float $tshirtPrice = 0;

    #[ORM\Column(options: ['default' => true])]
    private bool $isOpenRegistration = true;

    #[ORM\Column]
    private string $registrationExternalLink = '';

    // pošta / návštěvní kniha

    /** @var Collection<int, File> */
    #[ORM\ManyToMany(targetEntity: File::class)]
    #[ORM\JoinTable(name: 'childrens_camp_rel_file')]
    protected Collection $files;

    /** @var Collection<int, Transport> */
    #[ORM\ManyToMany(targetEntity: Transport::class)]
    #[ORM\JoinTable(name: 'childrens_camp_rel_transport')]
    private Collection $transports;

    /** @var Collection<int, Payment> */
    #[ORM\ManyToMany(targetEntity: Payment::class)]
    #[ORM\JoinTable(name: 'childrens_camp_rel_payment')]
    private Collection $payments;

    /** @var Collection<int, ChildrensCampRegistration> */
    #[ORM\OneToMany(targetEntity: ChildrensCampRegistration::class, mappedBy: 'camp')]
    #[ORM\OrderBy(['createdAt' => 'DESC'])]
    private Collection $registrations;

    /** @var Collection<int, ChildrensCampMailTemplate> */
    #[ORM\OneToMany(targetEntity: ChildrensCampMailTemplate::class, mappedBy: 'camp', orphanRemoval: true)]
    private Collection $mailTemplates;

    /** @var Collection<int, ChildrensCampSupplement> */
    #[ORM\ManyToMany(targetEntity: ChildrensCampSupplement::class)]
    #[ORM\JoinTable(name: 'childrens_camp_rel_childrens_camp_supplement')]
    #[ORM\OrderBy(['type' => 'ASC', 'name' => 'ASC'])]
    private Collection $supplements;

    public function __construct()
    {
        $this->files         = new ArrayCollection();
        $this->transports    = new ArrayCollection();
        $this->payments      = new ArrayCollection();
        $this->registrations = new ArrayCollection();
        $this->mailTemplates = new ArrayCollection();
        $this->supplements   = new ArrayCollection();

        $this->description     = '';
        $this->content         = '';
        $this->tshirtDeathLine = new DateTime();
    }

    public function update(string $name, string $description, string $content, string $textPrice, string $textCampAreal, string $textTransport, string $textMenu, string $textQuestBook, DateTimeInterface $termFrom, DateTimeInterface $termTo, string $address, int $capacity, bool $isActive, ?string $imagePreview): void
    {
        $this->name          = $name;
        $this->description   = $description;
        $this->content       = $content;
        $this->textPrice     = $textPrice;
        $this->textCampAreal = $textCampAreal;
        $this->textTransport = $textTransport;
        $this->textMenu      = $textMenu;
        $this->textQuestBook = $textQuestBook;
        $this->termFrom      = $termFrom;
        $this->termTo        = $termTo;
        $this->address       = $address;
        $this->capacity      = $capacity;

        $this->setIsActive($isActive);
        if ($imagePreview !== null && $imagePreview !== '') {
            $this->imagePreview = $imagePreview;
        }
    }

    public function updateRegistration(string $textRegistration, array $transports, array $payments, array $tshirtColorList, array $tshirtSizeList, DateTimeInterface $tshirtDeathLine, string $variableSymbolPrefix, bool $isOpenRegistration, string $registrationExternalLink): void
    {
        $this->textRegistration         = $textRegistration;
        $this->tshirtColorList          = $tshirtColorList;
        $this->tshirtSizeList           = $tshirtSizeList;
        $this->tshirtDeathLine          = $tshirtDeathLine;
        $this->variableSymbolPrefix     = $variableSymbolPrefix;
        $this->isOpenRegistration       = $isOpenRegistration;
        $this->registrationExternalLink = $registrationExternalLink;

        $this->transports->clear();
        foreach ($transports as $transport) {
            if (! $this->transports->contains($transport)) {
                $this->transports->add($transport);
            }
        }

        $this->payments->clear();
        foreach ($payments as $payment) {
            if (! $this->payments->contains($payment)) {
                $this->payments->add($payment);
            }
        }
    }

    /**
     * @param array<ChildrensCampSupplement> $supplements
     */
    public function updatePriceAndSupplement(float $price, float $tshirtPrice, array $supplements): void
    {
        $this->price       = $price;
        $this->tshirtPrice = $tshirtPrice;

        $this->supplements->clear();
        foreach ($supplements as $supplement) {
            $this->supplements->add($supplement);
        }
    }

    public function isOpenRegistration(): bool
    {
        return $this->isOpenRegistration;
    }

    public function getRegistrationExternalLink(): string
    {
        return $this->registrationExternalLink;
    }

    public function getVariableSymbolPrefix(): string
    {
        return $this->variableSymbolPrefix;
    }

    public function getCapacity(): int
    {
        return $this->capacity;
    }

    public function getTextPrice(): string
    {
        return $this->textPrice;
    }

    public function getTextCampAreal(): string
    {
        return $this->textCampAreal;
    }

    public function getTextTransport(): string
    {
        return $this->textTransport;
    }

    public function getTextMenu(): string
    {
        return $this->textMenu;
    }

    public function getTextQuestBook(): string
    {
        return $this->textQuestBook;
    }

    public function getTextRegistration(): string
    {
        return $this->textRegistration;
    }

    public function getPrice(): float
    {
        return $this->price;
    }

    /** @return array<string> */
    public function getTshirtColorList(): array
    {
        return $this->tshirtColorList;
    }

    /** @return array<string> */
    public function getTshirtSizeList(): array
    {
        return $this->tshirtSizeList;
    }

    public function getTshirtDeathLine(): DateTimeInterface
    {
        return $this->tshirtDeathLine;
    }

    public function getTshirtPrice(): float
    {
        return $this->tshirtPrice;
    }

    /** @return ArrayCollection|array<Transport> */
    public function getTransports(bool $onlyActive = false)
    {
        $criteria = Criteria::create();

        if ($onlyActive) {
            $criteria->where(Criteria::expr()->eq('isActive', true));
        }

        return $this->transports->matching($criteria);
    }

    /** @return ArrayCollection|array<Payment> */
    public function getPayments(bool $onlyActive = false)
    {
        $criteria = Criteria::create();

        if ($onlyActive) {
            $criteria->where(Criteria::expr()->eq('isActive', true));
        }

        return $this->payments->matching($criteria);
    }

    /** @return ArrayCollection|array<ChildrensCampRegistration> */
    public function getRegistrations(array $onlyStatuses = [], array $exludeStatuses = [])
    {
        $criteria = Criteria::create();

        if (count($onlyStatuses) > 0) {
            $criteria->andWhere(Criteria::expr()->in('status', $onlyStatuses));
        }

        if (count($exludeStatuses) > 0) {
            $criteria->andWhere(Criteria::expr()->notIn('status', $exludeStatuses));
        }

        return $this->registrations->matching($criteria);
    }

    /** @return ArrayCollection|array<ChildrensCampRegistration> */
    public function getValidRegistration()
    {
        return $this->getRegistrations([], [ChildrensCampRegistration::STATE_CANCELED]);
    }

    public function getCapacityInPercent(): int
    {
        if ($this->getCapacity() === 0) {
            return 100;
        }

        return intval(round(100 / $this->getCapacity() * $this->getValidRegistration()->count()));
    }

    /**
     * @return ArrayCollection|ChildrensCampMailTemplate[]
     */
    public function getMailTemplates()
    {
        return $this->mailTemplates;
    }


    /**
     * @param array<string> $identifiers
     */
    public function removeMailTemplatesExclude(string $class, string $type, array $identifiers): void
    {
        $criteria = Criteria::create();

        $criteria->where(Criteria::expr()->andX(
            Criteria::expr()->eq('class', $class),
            Criteria::expr()->eq('type', $type),
            Criteria::expr()->notIn('identifier', $identifiers),
        ));

        foreach ($this->mailTemplates->matching($criteria) as $mailTemplate) {
            $this->mailTemplates->removeElement($mailTemplate);
        }
    }

    /**
     * @param int|string $identifier
     */
    public function findMailTemplate(string $class, $identifier): ?ChildrensCampMailTemplate
    {
        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->andX(
            Criteria::expr()->eq('class', $class),
            Criteria::expr()->eq('identifier', (string) $identifier)
        ));

        $template = $this->mailTemplates->matching($criteria)->first();
        return is_bool($template) ? null : $template;
    }

    public function isCapacityFull(): bool
    {
        return $this->getValidRegistration()->count() >= $this->getCapacity();
    }

    public function isRegistrationExternalLink(): bool
    {
        return trim($this->getRegistrationExternalLink()) !== '';
    }

    /**
     * @return ArrayCollection|ChildrensCampSupplement[]
     */
    public function getSupplements(bool $onlyActive = false)
    {
        $criteria = Criteria::create();

        if ($onlyActive) {
            $criteria->where(Criteria::expr()->eq('isActive', true));
        }

        return $this->supplements->matching($criteria);
    }

    public function getSupplement(int $supplementId): ?ChildrensCampSupplement
    {
        $supplement = $this->supplements
            ->matching(Criteria::create()->where(Criteria::expr()->eq('id', $supplementId)))
            ->first();
        return $supplement instanceof ChildrensCampSupplement ? $supplement : null;
    }

}