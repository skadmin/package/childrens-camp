<?php

declare(strict_types=1);

namespace Skadmin\ChildrensCamp;

use App\Model\System\ABaseControl;
use Nette\Utils\ArrayHash;
use Nette\Utils\Html;

class BaseControl extends ABaseControl
{
    public const RESOURCE            = 'childrens-camp';
    public const DIR_IMAGE           = 'childrens-camp';
    public const DIR_FILE            = 'childrens-camp';
    public const DIR_FILE_ATTACHMENT = 'childrens-camp-attachment';

    public function getMenu() : ArrayHash
    {
        return ArrayHash::from([
            'control' => $this,
            'icon'    => Html::el('i', ['class' => 'fas fa-fw fa-campground']),
            'items'   => ['overview', 'overview-supplement'],
        ]);
    }
}
