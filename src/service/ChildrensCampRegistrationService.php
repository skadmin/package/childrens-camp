<?php

declare(strict_types=1);

namespace Skadmin\ChildrensCamp\Service;

use App\Model\System\Flash;
use App\Model\System\SystemDir;
use Mpdf\Mpdf;
use Mpdf\Output\Destination;
use Nette\Application\UI\Template;
use Nette\Application\UI\TemplateFactory;
use Nette\Http\FileUpload;
use Nette\Utils\Strings;
use Skadmin\ChildrensCamp\BaseControl;
use Skadmin\ChildrensCamp\Doctrine\ChildrensCampMailTemplate\ChildrensCampMailTemplateFacade;
use Skadmin\ChildrensCamp\Doctrine\ChildrensCampRegistration\ChildrensCampRegistration;
use Skadmin\ChildrensCamp\Mail\CMailChildrensCampExtraordinaryEvent;
use Skadmin\ChildrensCamp\Mail\CMailChildrensCampRegistration;
use Skadmin\ChildrensCamp\Mail\CMailChildrensCampRegistrationInvoice;
use Skadmin\ChildrensCamp\Mail\CMailChildrensCampSatisfactionQuestionnaire;
use Skadmin\FileStorage\FilePreview;
use Skadmin\FileStorage\FileStorage;
use Skadmin\Mailing\Doctrine\Mail\MailQueue;
use Skadmin\Mailing\Model\MailService;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;

class ChildrensCampRegistrationService
{

    private Translator                      $translator;
    private MailService                     $mailService;
    private ChildrensCampMailTemplateFacade $facadeChildrensCampMailTemplate;
    private FileStorage                     $fileStorage;
    private TemplateFactory                 $templateFactory;
    private SystemDir                       $systemDir;

    public function __construct(Translator $translator, MailService $mailService, ChildrensCampMailTemplateFacade $facadeChildrensCampMailTemplate, FileStorage $fileStorage, TemplateFactory $templateFactory, SystemDir $systemDir)
    {
        $this->translator                      = $translator;
        $this->mailService                     = $mailService;
        $this->facadeChildrensCampMailTemplate = $facadeChildrensCampMailTemplate;
        $this->fileStorage                     = $fileStorage;
        $this->templateFactory                 = $templateFactory;
        $this->systemDir                       = $systemDir;
    }

    public function sendByState(ChildrensCampRegistration $registration): ?MailQueue
    {
        $template = $registration->getCamp()->findMailTemplate(CMailChildrensCampRegistration::class, $registration->getStatus());

        if ($template === null) {
            return null;
        }

        return $this->mailService->add(
            $template,
            new CMailChildrensCampRegistration($registration, $this->translator),
            [$registration->getContactEmail()],
            true
        );
    }

    public function sendInvoice(ChildrensCampRegistration $registration): ?MailQueue
    {
        $template = $registration->getCamp()->findMailTemplate(CMailChildrensCampRegistrationInvoice::class, CMailChildrensCampRegistrationInvoice::IDENTIFIER);

        if ($template === null) {
            return null;
        }

        return $this->mailService->add(
            $template,
            new CMailChildrensCampRegistrationInvoice($registration, $this->createPdf($registration, ChildrensCampRegistration::PDF_TYPE_INVOICE)),
            [$registration->getContactEmail()],
            true
        );
    }

    public function sendExtraordinaryEvent(ChildrensCampRegistration $registration): ?MailQueue
    {
        $template = $registration->getCamp()->findMailTemplate(CMailChildrensCampExtraordinaryEvent::class, CMailChildrensCampExtraordinaryEvent::IDENTIFIER);

        if ($template === null) {
            return null;
        }

        return $this->mailService->add(
            $template,
            new CMailChildrensCampExtraordinaryEvent($registration),
            [$registration->getContactEmail()],
            true
        );
    }

    public function sendSatisfactionQuestionnaire(ChildrensCampRegistration $registration): ?MailQueue
    {
        $template = $registration->getCamp()->findMailTemplate(CMailChildrensCampSatisfactionQuestionnaire::class, CMailChildrensCampSatisfactionQuestionnaire::IDENTIFIER);

        if ($template === null) {
            return null;
        }

        return $this->mailService->add(
            $template,
            new CMailChildrensCampSatisfactionQuestionnaire($registration),
            [$registration->getContactEmail()],
            true
        );
    }

    public function createPdf(ChildrensCampRegistration $registration, int $pdfType, bool $download = false): ?string
    {
        $translator = clone $this->translator;
        $translator->setModule('admin');

        $template = null;
        $footer   = null;

        switch ($pdfType) {
            case ChildrensCampRegistration::PDF_TYPE_INVOICE:
                $template = $this->templateFactory->createTemplate();
                $template->setFile($this->systemDir->getPathApp(['templates', 'pdf', 'childrens-camp-registration-pdf-type-invoice.latte']));
                $template->registration = $registration;

                $pdfName = $translator->translate(new SimpleTranslation('childrens-camp-registration.pdf.invoice %s', [$registration->getVariableSymbol()]));
                break;
        }

        if ($template instanceof Template) {
            $mpdfConfig = include_once $this->systemDir->getPathApp(['templates', 'pdf', '_mpdf-config.php']);

            $pdf = new Mpdf($mpdfConfig['config']);
            $pdf->WriteHTML($template);

            if (isset($mpdfConfig['header']) && $mpdfConfig['header']) {
                $pdf->SetHeader($mpdfConfig['header']);
            }

            if (isset($mpdfConfig['footer']) && $mpdfConfig['footer']) {
                $pdf->SetFooter($mpdfConfig['footer']);
            }

            $fileIdentifier = $this->fileStorage->saveContent(
                $pdf->Output('', Destination::STRING_RETURN),
                sprintf('invoice-%s.pdf', $registration->getVariableSymbol()),
                BaseControl::DIR_FILE,
                true
            );

            if ($download) {
                $this->fileStorage->download($fileIdentifier, sprintf('%s.pdf', $pdfName));
            }

            return $fileIdentifier;
        }
    }

}
