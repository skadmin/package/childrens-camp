<?php

declare(strict_types=1);

namespace Skadmin\ChildrensCamp\Mail;

use App\Model\System\Strings as SystemStrings;
use Haltuf\Genderer\Genderer;
use ReflectionProperty;
use Skadmin\ChildrensCamp\Doctrine\ChildrensCampRegistration\ChildrensCampRegistration;
use Skadmin\Mailing\Model\CMail;
use Skadmin\Mailing\Model\MailParameterValue;
use Skadmin\Mailing\Model\MailTemplateParameter;

class CMailChildrensCampSatisfactionQuestionnaire extends CMail
{
    public const TYPE       = 'childrens-camp-satisfaction-questionnaire';
    public const IDENTIFIER = 'satisfactionQuestionnaire';

    private string $childName;
    private string $childNameReversed;
    private string $childNameInflected;
    private string $campName;
    private string $variableSymbol;
    private string $priceTotal;

    public function __construct(ChildrensCampRegistration $registration)
    {
        $this->childName          = $registration->getFullName();
        $this->childNameReversed  = $registration->getFullName(true);
        $this->childNameInflected = (new Genderer())->getVocative($registration->getFullName());
        $this->campName           = $registration->getCamp()->getName();
        $this->variableSymbol     = $registration->getVariableSymbol();
        $this->priceTotal         = sprintf('%s Kč', number_format($registration->getTotalPrice(), 2, ',', ' '));
    }

    /**
     * @return mixed[]
     */
    public static function getModelForSerialize(): array
    {
        $model = [];

        /** @var ReflectionProperty $property */
        foreach (self::getProperties(self::class) as $property) {
            $description = sprintf('mail.%s.parameter.%s.description', self::TYPE, $property);
            $example     = sprintf('mail.%s.parameter.%s.example', self::TYPE, $property);

            $model[] = (new MailTemplateParameter($property, $description, $example))->getDataForSerialize();
        }

        return $model;
    }

    /**
     * @return MailParameterValue[]
     */
    public function getParameterValues(): array
    {
        $mailParameterValue = [];

        /** @var ReflectionProperty $property */
        foreach (self::getProperties(self::class) as $property) {
            $method = sprintf('get%s', SystemStrings::camelize($property));

            if (method_exists($this, $method)) {
                $mailParameterValue[] = new MailParameterValue($property, call_user_func([$this, $method]));
            }
        }

        return $mailParameterValue;
    }

    public function getChildName(): string
    {
        return $this->childName;
    }

    public function setChildName(string $childName): CMailChildrensCampRegistrationInvoice
    {
        $this->childName = $childName;
        return $this;
    }

    public function getChildNameReversed(): string
    {
        return $this->childNameReversed;
    }

    public function setChildNameReversed(string $childNameReversed): CMailChildrensCampRegistrationInvoice
    {
        $this->childNameReversed = $childNameReversed;
        return $this;
    }

    public function getChildNameInflected(): string
    {
        return $this->childNameInflected;
    }

    public function setChildNameInflected(string $childNameInflected): CMailChildrensCampRegistrationInvoice
    {
        $this->childNameInflected = $childNameInflected;
        return $this;
    }

    public function getCampName(): string
    {
        return $this->campName;
    }

    public function setCampName(string $campName): CMailChildrensCampRegistrationInvoice
    {
        $this->campName = $campName;
        return $this;
    }

    public function getVariableSymbol(): string
    {
        return $this->variableSymbol;
    }

    public function setVariableSymbol(string $variableSymbol): CMailChildrensCampRegistrationInvoice
    {
        $this->variableSymbol = $variableSymbol;
        return $this;
    }

    public function getPriceTotal(): string
    {
        return $this->priceTotal;
    }

    public function setPriceTotal(string $priceTotal): CMailChildrensCampRegistrationInvoice
    {
        $this->priceTotal = $priceTotal;
        return $this;
    }

}
