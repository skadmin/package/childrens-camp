<?php

declare(strict_types=1);

namespace Skadmin\ChildrensCamp\Mail;

use App\Model\System\Constant;
use Haltuf\Genderer\Genderer;
use Nette\Utils\Strings;
use App\Model\System\Strings as SystemStrings;
use DateTimeInterface;
use Nette\Utils\Html;
use ReflectionProperty;
use Skadmin\ChildrensCamp\Doctrine\ChildrensCampRegistration\ChildrensCampRegistration;
use Skadmin\Mailing\Model\CMail;
use Skadmin\Mailing\Model\MailParameterValue;
use Skadmin\Mailing\Model\MailTemplateParameter;
use Skadmin\Translator\Translator;
use function call_user_func;
use function method_exists;
use function sprintf;

class CMailChildrensCampRegistration extends CMail
{
    public const TYPE = 'childrens-camp-registration';

    // 1. Část = Základní údaje
    private string $childName;
    private string $childNameReversed;
    private string $childNameInflected;
    private string $personalIdentificationNumber;
    private string $birthdate;

    // 2. Část = Bydliště
    private string $address;

    // 3. Část = Tábor
    private string $campName;
    private string $campTerm;
    private string $campAddress;
    private string $campAddressLink;

    // 4. Část = Kontaktní údaje (Rodiče / Zákonný zástupce)
    private string $contactEmail;
    private string $legalRepresentativePersons;

    // 5. Část = Doprava na tábor
    private string $transportName;
    private string $transportContent;

    // 6. Část = Způsob platby
    private string $paymentName;
    private string $paymentContent;

    // 7. Část = Informace o dítěti
    private string $swimmingSkills;
    private string $insuranceCompany;
    private string $confirmationForTheInsuranceCompany;
    private string $healthCondition;
    private string $washingFirst;
    private string $washingSecond;
    private string $washingThird;

    // 8. Část = Fakturace
    private string $invoice;

    // 10. Část = Souhlasy
    private string $permissionToSeparateAChildOnTrip;

    // 11. Část = Táborové tričko
    private string $tshirt;

    // 12. Část = Závěr
    private string $note;

    // 13. Část = Datumy
    private string $createdAt;

    // 14. Část = Platba
    private string $variableSymbol;
    private string $price;
    private string $priceTotal;

    public function __construct(ChildrensCampRegistration $registration, Translator $origTranslator)
    {
        $translator = clone $origTranslator;
        $translator->setModule('front');

        $that          = $this;
        $functionMatch = static function (array $m) use ($that) : string {
            $match  = trim($m[0], '[]');
            $method = sprintf('get%s', SystemStrings::camelize($match));

            if (method_exists($that, $method)) {
                $value = $that->$method();

                if ($value instanceof DateTimeInterface) {
                    return $value->format('d.m.Y H:i');
                }

                return (string) $value;
            }

            return $match;
        };

        // 1. Část = Základní údaje
        $this->childName                    = $registration->getFullName();
        $this->childNameReversed            = $registration->getFullName(true);
        $this->childNameInflected           = (new Genderer())->getVocative($registration->getFullName());
        $this->personalIdentificationNumber = $registration->getPersonalIdentificationNumber();
        $this->birthdate                    = $registration->getBirthdate()->format('d.m.Y');

        // 2. Část = Bydliště
        $this->address = sprintf('%s, %s %s', $registration->getStreet(), $registration->getCity(), $registration->getZipCode());

        // 3. Část = Tábor
        $this->campName        = $registration->getCamp()->getName();
        $this->campTerm        = $registration->getCamp()->getTermClever();
        $this->campAddress     = $registration->getCamp()->getAddress();
        $this->campAddressLink = sprintf('<a href="%s" target="_blank">%s</a>', $registration->getCamp()->getAddressLinkToMap(), $registration->getCamp()->getAddress());

        // 4. Část = Kontaktní údaje (Rodiče / Zákonný zástupce)
        $this->contactEmail               = $registration->getContactEmail();
        $this->legalRepresentativePersons = trim(
            trim(sprintf('%s - %s', $registration->getMotherName(), $registration->getMotherPhone()), '- ') .
            trim(sprintf('%s - %s', $registration->getFatherName(), $registration->getFatherPhone()), '- ') .
            trim(sprintf('%s - %s', $registration->getLegalRepresentativeName(), $registration->getLegalRepresentativePhone()), '- ')
        );

        // 5. Část = Doprava na tábor
        $this->transportName    = $registration->getTransport()->getName();
        $this->transportContent = $registration->getTransport()->getContent();

        // 6. Část = Způsob platby
        $this->paymentName    = $registration->getPayment()->getName();
        $this->paymentContent = $registration->getPayment()->getContent();

        // 7. Část = Informace o dítěti
        $this->swimmingSkills                     = $translator->translate(ChildrensCampRegistration::SWIMMINGS_SKILLS[$registration->getSwimmingSkills()]);
        $this->insuranceCompany                   = trim(sprintf('%s - %s', $registration->getInsuranceCompany()->getCode(), $registration->getInsuranceCompany()->getName()), '- ');
        $this->confirmationForTheInsuranceCompany = $translator->translate(Constant::DIAL_YES_NO[intval($registration->isConfirmationForTheInsuranceCompany())]);
        $this->healthCondition                    = $registration->getHealthCondition();
        $this->washingFirst                       = $registration->getWashingFirst();
        $this->washingSecond                      = $registration->getWashingSecond();
        $this->washingThird                       = $registration->getWashingThird();

        // 8. Část = Fakturace
        $this->invoice = trim(
            trim($registration->getInvoiceFirm()) .
            trim(sprintf('%s, %s', $registration->getInvoiceIN(), $registration->getInvoiceTIN()), ', ') .
            trim(sprintf('%s, %s %s', $registration->getInvoiceStreet(), $registration->getInvoiceCity(), $registration->getInvoiceZipCode()), ', ') .
            trim($registration->getInvoiceNote())
        );
        if ($this->invoice === '') {
            $this->invoice = '-';
        }

        // 10. Část = Souhlasy
        $this->permissionToSeparateAChildOnTrip = $translator->translate(Constant::DIAL_YES_NO[intval($registration->isPermissionToSeparateAChildOnTrip())]);

        // 11. Část = Táborové tričko
        if ($registration->getTshirtColor() !== '') {
            $this->tshirt = sprintf('%sx %s [%s]<br>%s', $registration->getTshirtCount(), $registration->getTshirtColor(), $registration->getTshirtSize(), $registration->getTshirtNote());
        } else {
            $this->tshirt = '-';
        }

        // 12. Část = Závěr
        $this->note = $registration->getNote();

        // 13. Část = Datumy
        $this->createdAt = $registration->getCreatedAt()->format('d.m.Y H:i:s');

        // 14. Část = Systém
        $this->variableSymbol = $registration->getVariableSymbol();

        $this->priceTotal = sprintf('%s Kč', number_format($registration->getTotalPrice(), 2, ',', ' '));

        $priceTableRow = static function (string $name, string $value, array $attr = []) : Html {
            $tr = Html::el('tr', $attr['tr'] ?? []);
            $tr->addHtml(Html::el('td', $attr['td1'] ?? $attr['td'] ?? [])->setHtml($name));
            $tr->addHtml(Html::el('td', $attr['td2'] ?? $attr['td'] ?? [])->setHtml($value));
            return $tr;
        };
        $price         = Html::el('table', ['style' => 'width: 100%; border-collapse: collapse;']);
        $price->addHtml($priceTableRow(
            $translator->translate('childrens-camp-registration.price.item'),
            $translator->translate('childrens-camp-registration.price.price'),
            [
                'tr'  => ['style' => 'font-weight: bold;'],
                'td1' => ['style' => 'border-bottom: 1px solid black;'],
                'td2' => ['style' => 'text-align: right; border-bottom: 1px solid black;'],
            ]
        ));
        $price->addHtml($priceTableRow(
            $translator->translate('childrens-camp-registration.camp-price'),
            sprintf('%s Kč', number_format($registration->getCampPrice(), 2, ',', ' ')),
            [
                'td2' => ['style' => 'text-align: right;'],
            ]
        ));
        if ($registration->getTshirtColor() !== '') {
            $tshirtName = sprintf('%s - %sx %s [%s]<br><span style="font-size: 80%%; opacity: 75%%;">%s</span>',
                $translator->translate('childrens-camp-registration.tshirt-phrase'),
                $registration->getTshirtCount(),
                $registration->getTshirtColor(),
                $registration->getTshirtSize(),
                $registration->getTshirtNote()
            );
            $price->addHtml($priceTableRow(
                $tshirtName,
                sprintf('%s Kč', number_format($registration->getTshirtCount() * $registration->getCamp()->getTshirtPrice(), 2, ',', ' ')),
                [
                    'td2' => ['style' => 'text-align: right;'],
                ]
            ));
        }
        foreach ($registration->getSupplements() as $relSupplement) {
            $supplement = $relSupplement->getSupplement();
            $price->addHtml($priceTableRow(
                $supplement->getName(),
                sprintf('%s Kč', number_format($supplement->getPrice(true), 2, ',', ' ')),
                [
                    'td2' => ['style' => 'text-align: right;'],
                ]
            ));
        }

        $price->addHtml($priceTableRow(
            $translator->translate('childrens-camp-registration.camp-price-total'),
            sprintf('%s Kč', number_format($registration->getTotalPrice(), 2, ',', ' ')),
            [
                'tr'  => ['style' => 'font-weight: bold;'],
                'td1' => ['style' => 'border-top: 1px solid black; font-size: 120%;'],
                'td2' => ['style' => 'text-align: right; border-top: 1px solid black; font-size: 120%;'],
            ]
        ));
        $this->price = (string) $price;

        // Specific e-mail
        if (trim($registration->getTransport()->getContentEmail()) !== '') {
            $this->transportContent = Strings::replace($registration->getTransport()->getContentEmail(), '~\[[a-z\-]+\]~i', $functionMatch);
        }
        if (trim($registration->getPayment()->getContentEmail()) !== '') {
            $this->paymentContent = Strings::replace($registration->getPayment()->getContentEmail(), '~\[[a-z\-]+\]~i', $functionMatch);
        }
    }

    /**
     * @return mixed[]
     */
    public static function getModelForSerialize() : array
    {
        $model = [];

        /** @var ReflectionProperty $property */
        foreach (self::getProperties(self::class) as $property) {
            $description = sprintf('mail.%s.parameter.%s.description', self::TYPE, $property);
            $example     = sprintf('mail.%s.parameter.%s.example', self::TYPE, $property);

            $model[] = (new MailTemplateParameter($property, $description, $example))->getDataForSerialize();
        }

        return $model;
    }

    /**
     * @return MailParameterValue[]
     */
    public function getParameterValues() : array
    {
        $mailParameterValue = [];

        /** @var ReflectionProperty $property */
        foreach (self::getProperties(self::class) as $property) {
            $method = sprintf('get%s', SystemStrings::camelize($property));

            if (method_exists($this, $method)) {
                $mailParameterValue[] = new MailParameterValue($property, call_user_func([$this, $method]));
            }
        }

        return $mailParameterValue;
    }

    public function getChildName() : string
    {
        return $this->childName;
    }

    public function setChildName(string $childName) : CMailChildrensCampRegistration
    {
        $this->childName = $childName;
        return $this;
    }

    public function getChildNameReversed() : string
    {
        return $this->childNameReversed;
    }

    public function setChildNameReversed(string $childNameReversed) : CMailChildrensCampRegistration
    {
        $this->childNameReversed = $childNameReversed;
        return $this;
    }

    public function getChildNameInflected() : string
    {
        return $this->childNameInflected;
    }

    public function setChildNameInflected(string $childNameInflected) : CMailChildrensCampRegistration
    {
        $this->childNameInflected = $childNameInflected;
        return $this;
    }

    public function getPersonalIdentificationNumber() : string
    {
        return $this->personalIdentificationNumber;
    }

    public function setPersonalIdentificationNumber(string $personalIdentificationNumber) : CMailChildrensCampRegistration
    {
        $this->personalIdentificationNumber = $personalIdentificationNumber;
        return $this;
    }

    public function getBirthdate() : string
    {
        return $this->birthdate;
    }

    public function setBirthdate(string $birthdate) : CMailChildrensCampRegistration
    {
        $this->birthdate = $birthdate;
        return $this;
    }

    public function getAddress() : string
    {
        return $this->address;
    }

    public function setAddress(string $address) : CMailChildrensCampRegistration
    {
        $this->address = $address;
        return $this;
    }

    public function getCampName() : string
    {
        return $this->campName;
    }

    public function setCampName(string $campName) : CMailChildrensCampRegistration
    {
        $this->campName = $campName;
        return $this;
    }

    public function getCampTerm() : string
    {
        return $this->campTerm;
    }

    public function setCampTerm(string $campTerm) : CMailChildrensCampRegistration
    {
        $this->campTerm = $campTerm;
        return $this;
    }

    public function getCampAddress() : string
    {
        return $this->campAddress;
    }

    public function setCampAddress(string $campAddress) : CMailChildrensCampRegistration
    {
        $this->campAddress = $campAddress;
        return $this;
    }

    public function getCampAddressLink() : string
    {
        return $this->campAddressLink;
    }

    public function setCampAddressLink(string $campAddressLink) : CMailChildrensCampRegistration
    {
        $this->campAddressLink = $campAddressLink;
        return $this;
    }

    public function getContactEmail() : string
    {
        return $this->contactEmail;
    }

    public function setContactEmail(string $contactEmail) : CMailChildrensCampRegistration
    {
        $this->contactEmail = $contactEmail;
        return $this;
    }

    public function getLegalRepresentativePersons() : string
    {
        return $this->legalRepresentativePersons;
    }

    public function setLegalRepresentativePersons(string $legalRepresentativePersons) : CMailChildrensCampRegistration
    {
        $this->legalRepresentativePersons = $legalRepresentativePersons;
        return $this;
    }

    public function getTransportName() : string
    {
        return $this->transportName;
    }

    public function setTransportName(string $transportName) : CMailChildrensCampRegistration
    {
        $this->transportName = $transportName;
        return $this;
    }

    public function getTransportContent() : string
    {
        return $this->transportContent;
    }

    public function setTransportContent(string $transportContent) : CMailChildrensCampRegistration
    {
        $this->transportContent = $transportContent;
        return $this;
    }

    public function getPaymentName() : string
    {
        return $this->paymentName;
    }

    public function setPaymentName(string $paymentName) : CMailChildrensCampRegistration
    {
        $this->paymentName = $paymentName;
        return $this;
    }

    public function getPaymentContent() : string
    {
        return $this->paymentContent;
    }

    public function setPaymentContent(string $paymentContent) : CMailChildrensCampRegistration
    {
        $this->paymentContent = $paymentContent;
        return $this;
    }

    public function getSwimmingSkills() : string
    {
        return $this->swimmingSkills;
    }

    public function setSwimmingSkills(string $swimmingSkills) : CMailChildrensCampRegistration
    {
        $this->swimmingSkills = $swimmingSkills;
        return $this;
    }

    public function getInsuranceCompany() : string
    {
        return $this->insuranceCompany;
    }

    public function setInsuranceCompany(string $insuranceCompany) : CMailChildrensCampRegistration
    {
        $this->insuranceCompany = $insuranceCompany;
        return $this;
    }

    public function getConfirmationForTheInsuranceCompany() : string
    {
        return $this->confirmationForTheInsuranceCompany;
    }

    public function setConfirmationForTheInsuranceCompany(string $confirmationForTheInsuranceCompany) : CMailChildrensCampRegistration
    {
        $this->confirmationForTheInsuranceCompany = $confirmationForTheInsuranceCompany;
        return $this;
    }

    public function getHealthCondition() : string
    {
        return $this->healthCondition;
    }

    public function setHealthCondition(string $healthCondition) : CMailChildrensCampRegistration
    {
        $this->healthCondition = $healthCondition;
        return $this;
    }

    public function getWashingFirst() : string
    {
        return $this->washingFirst;
    }

    public function setWashingFirst(string $washingFirst) : CMailChildrensCampRegistration
    {
        $this->washingFirst = $washingFirst;
        return $this;
    }

    public function getWashingSecond() : string
    {
        return $this->washingSecond;
    }

    public function setWashingSecond(string $washingSecond) : CMailChildrensCampRegistration
    {
        $this->washingSecond = $washingSecond;
        return $this;
    }

    public function getWashingThird() : string
    {
        return $this->washingThird;
    }

    public function setWashingThird(string $washingThird) : CMailChildrensCampRegistration
    {
        $this->washingThird = $washingThird;
        return $this;
    }

    public function getInvoice() : string
    {
        return $this->invoice;
    }

    public function setInvoice(string $invoice) : CMailChildrensCampRegistration
    {
        $this->invoice = $invoice;
        return $this;
    }

    public function getPermissionToSeparateAChildOnTrip() : string
    {
        return $this->permissionToSeparateAChildOnTrip;
    }

    public function setPermissionToSeparateAChildOnTrip(string $permissionToSeparateAChildOnTrip) : CMailChildrensCampRegistration
    {
        $this->permissionToSeparateAChildOnTrip = $permissionToSeparateAChildOnTrip;
        return $this;
    }

    public function getTshirt() : string
    {
        return $this->tshirt;
    }

    public function setTshirt(string $tshirt) : CMailChildrensCampRegistration
    {
        $this->tshirt = $tshirt;
        return $this;
    }

    public function getNote() : string
    {
        return $this->note;
    }

    public function setNote(string $note) : CMailChildrensCampRegistration
    {
        $this->note = $note;
        return $this;
    }

    public function getCreatedAt() : string
    {
        return $this->createdAt;
    }

    public function setCreatedAt(string $createdAt) : CMailChildrensCampRegistration
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    public function getVariableSymbol() : string
    {
        return $this->variableSymbol;
    }

    public function setVariableSymbol(string $variableSymbol) : CMailChildrensCampRegistration
    {
        $this->variableSymbol = $variableSymbol;
        return $this;
    }

    public function getPrice() : string
    {
        return $this->price;
    }

    public function setPrice(string $price) : CMailChildrensCampRegistration
    {
        $this->price = $price;
        return $this;
    }

    public function getPriceTotal() : string
    {
        return $this->priceTotal;
    }

    public function setPriceTotal(string $priceTotal) : CMailChildrensCampRegistration
    {
        $this->priceTotal = $priceTotal;
        return $this;
    }

}
